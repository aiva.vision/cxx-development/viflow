#pragma once

#include <unordered_map>

#include "viflow/bus_watch.hxx"

namespace viflow {

/**
 * @brief Useful when dealing with low level GstBus instances.
 */
using unique_bus = unique_ptr_fn<GstBus, gst_object_unref>;

/**
 * @brief Adapter for GstBus.
 */
class Bus {
public:
  /**
   * @brief Increase the reference count and take ownership.
   *
   * @param b The bus to reference.
   *
   * @return Bus
   */
  static Bus ref(GstBus *b);

  Bus(const Bus &b) = delete;
  Bus(Bus &&b) = default;
  Bus &operator=(const Bus &b) = delete;
  Bus &operator=(Bus &&b) = default;

  /**
   * @brief Construct a new Bus object and assume ownership.
   *
   * @param bus The bus to assume ownership for.
   */
  explicit Bus(GstBus *bus);

  virtual ~Bus();

  /**
   * @brief Checks whether the bus has a registered watch.
   *
   * @return true if the bus has a registered watch.
   */
  bool has_watch();

  /**
   * @brief Returns the watch id.
   *
   * @return unsigned The watch id.
   */
  unsigned watch_id() const noexcept { return bus_watch_id; }

  /**
   * @brief Adds a watch to the bus.
   *
   * @param watch The watch to add.
   *
   * @return The event source id.
   *
   * @throw Exception If the watch cannot be added.
   */
  unsigned int add_watch(std::unique_ptr<Bus_Watch> &&watch);

  /**
   * @brief Removes the watch from the bus.
   *
   * @return true if the watch was removed.
   */
  bool remove_watch() noexcept;

  /**
   * @brief Checks whether the bus has a registered signal watch.
   *
   * @param signal The signal to check.
   *
   * @return true if the bus has a registered signal watch.
   */
  bool has_signal_watch(const std::string &signal) {
    return bus_signal_watches.contains(signal);
  }

  /**
   * @brief Adds a signal watch to the bus.
   *
   * @param watch The watch to add.
   * @param signal The signal to watch.
   */
  void add_signal_watch(std::unique_ptr<Bus_Signal_Watch> &&watch,
                        const std::string &signal) noexcept;
  /**
   * @brief Removes the signal watch from the bus.
   *
   * @param signal The signal to remove.
   */
  void remove_signal_watch(const std::string &signal) noexcept;

  /**
   * @brief Pops a message from the bus.
   *
   * @return Message
   */
  Message pop() noexcept;

  /**
   * @brief Pops a message from the bus.
   *
   * If timeout is GST_CLOCK_TIME_NONE, this function will block forever until a
   * matching message was posted on the bus.
   *
   * @param timeout The timeout to wait for.
   * @param type The type of the message.
   *
   * @return Message
   */
  Message timed_pop_filtered(GstClockTime timeout,
                             GstMessageType type) noexcept;

private:
  /*
   * Low level representation of the bus.
   */
  unique_bus core;
  /*
   * ID returned by gst_bus_add_watch.
   */
  unsigned bus_watch_id{0};
  /*
   * This will receive asynchronous messages in the main loop.
   */
  std::unique_ptr<Bus_Watch> bus_watch;
  /*
   * These will receive asynchronous messages in the main loop for a certain
   * signal.
   */
  std::unordered_map<std::string, std::unique_ptr<Bus_Signal_Watch>>
      bus_signal_watches;
};

} // namespace viflow
