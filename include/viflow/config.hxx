#pragma once

#define VIFLOW_CONFIG

#include <filesystem>
#include <libconfig.h++>
#include <viflow/element.hxx>

namespace viflow {

/**
 * @brief Tries to return the value of the field as having the specified type.
 *
 * @tparam T The type of the value.
 *
 * @param setting The setting from which to get the value.
 * @param field The field for which to get the value.
 *
 * @return T The value of the field.
 */
template <typename T>
T get_value(const libconfig::Setting &setting, const std::string &field);

/**
 * @brief Sets the property from the config file on the element.
 *
 * @tparam T The type of the property.
 *
 * @param elem The element on which to set the property.
 * @param config The config from which to get the property.
 * @param prop The property to set.
 *
 * @throw Property_Type_Error If the property is of the wrong type.
 * @throw Missing_Property If the property is missing.
 */
template <typename T>
void set_prop(const Shared_Element<> &elem, const libconfig::Setting &config,
              const std::string &prop);

/**
 * @brief Sets the property from the config file on the element.
 *
 * @tparam T The type of the property.
 *
 * @param elem The element on which to set the property.
 * @param config The config from which to get the property.
 * @param prop The property to set.
 *
 * @throw Property_Type_Error If the property is of the wrong type.
 * @throw Missing_Property If the property is missing.
 */
template <typename T>
void set_prop(viflow::Element *elem, const libconfig::Setting &config,
              const std::string &prop);

/**
 * @brief Sets the property from the config file on the element.
 *
 * @tparam T The type of the property.
 *
 * @param elem The element on which to set the property.
 * @param config The config from which to get the property.
 * @param prop The property to set.
 *
 * @throw Property_Type_Error If the property is of the wrong type.
 */
template <typename T>
void try_set_prop(const Shared_Element<> &elem,
                  const libconfig::Setting &config, const std::string &prop);

/**
 * @brief Sets the property from the config file on the element.
 *
 * @tparam T The type of the property.
 *
 * @param elem The element on which to set the property.
 * @param config The config from which to get the property.
 * @param prop The property to set.
 *
 * @throw Property_Type_Error If the property is of the wrong type.
 */
template <typename T>
void try_set_prop(viflow::Element *elem, const libconfig::Setting &config,
                  const std::string &prop);

/**
 * @brief If the field contains an already absolute path, it just returns the
 * path. Otherwise, it concatenates the parent path of the config to the
 * relative path found in the field.
 *
 * @param setting The setting from which to get the path.
 * @param field The field for which to get the path.
 *
 * @return std::filesystem::path The absolute path.
 */
std::filesystem::path get_abs_path(const libconfig::Setting &setting,
                                   const std::string &field);

} // namespace viflow

#include "viflow/config.txx"
