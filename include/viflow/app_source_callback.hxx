#pragma once

#include "viflow/shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {

/**
 * @brief Interface for enough-data GCallback adapters.
 */
class Enough_Data_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the Enough_Data_Callback object
   */
  virtual ~Enough_Data_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the enough-data signal is
   *        emitted.
   *
   * @param app_source The app_source element that emitted the signal.
   */
  virtual void process(const Shared_Element<> &app_source) = 0;
};

/**
 * @brief Interface for need-data GCallback adapters.
 */
class Need_Data_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the Need_Data_Callback object
   */
  virtual ~Need_Data_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the need-data signal is
   *        emitted.
   *
   * @param app_source The app_source element that emitted the signal.
   * @param length The length of data that is needed.
   */
  virtual void process(const Shared_Element<> &app_source,
                       unsigned int length) = 0;
};

/**
 * @brief Interface for seek-data GCallback adapters.
 */
class Seek_Data_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the Seek_Data_Callback object
   */
  virtual ~Seek_Data_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the seek-data signal is
   *        emitted.
   *
   * @param app_source The app_source element that emitted the signal.
   * @param offset The offset to seek to.
   */
  virtual void process(const Shared_Element<> &app_source,
                       unsigned long offset) = 0;
};

} // namespace viflow
