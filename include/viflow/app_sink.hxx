#pragma once

#include "viflow/app_sink_callback.hxx"
#include "viflow/element.hxx"
#include "viflow/sample.hxx"

namespace viflow {

/**
 * @brief Namespace for App Sink signal names
 */
namespace App_Sink_Signals {

constexpr const char *eos = "eos";
constexpr const char *new_preroll = "new-preroll";
constexpr const char *new_sample = "new-sample";
constexpr const char *new_serialized_event = "new-serialized-event";
constexpr const char *propose_allocation = "propose-allocation";

} // namespace App_Sink_Signals

/**
 * @brief Class representing an App Sink element
 */
class App_Sink : public Element {
public:
  /**
   * @brief Destroy the App_Sink object
   */
  virtual ~App_Sink() {}

  /**
   * @brief Blocks until a sample or EOS becomes available or the appsink
   *        element is set to the READY/NULL state.
   *
   * @return Sample The available sample
   */
  Sample get_sample() const noexcept;

  /*
   * Expose the existing callback methods.
   */
  using Element::connect_callback;

  /**
   * @brief Connects a callback for the eos signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<EoS_Callback> &&callback);

  /**
   * @brief Connects a callback for the new-preroll signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<New_Preroll_Callback> &&callback);

  /**
   * @brief Connects a callback for the new-sample signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<New_Sample_Callback> &&callback);

  /**
   * @brief Connects a callback for the new-serialized-event signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<New_Serialized_Event_Callback> &&callback);

  protected:
  /**
   * @brief Creates a new App_Sink element.
   *
   * @param name The name of the element.
   */
  App_Sink(const std::string &name);

  friend class Element;
};

} // namespace viflow
