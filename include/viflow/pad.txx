#pragma once

#ifndef VIFLOW_PAD
#include "viflow/pad.hxx"
#endif // VIFLOW_PAD

template <typename T>
void viflow::Pad::set_property(const std::string &name,
                               const T &value) const noexcept {
  g_object_set(get_core(), name.c_str(), value, nullptr);
}
