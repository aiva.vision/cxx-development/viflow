#pragma once

#include <glib-object.h>

#include "viflow/shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {

/**
 * @brief Interface for child-added GCallback adapters.
 */
class Child_Added_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the Child_Added_Callback object
   */
  virtual ~Child_Added_Callback() {}

  /**
   * @brief Called by the dummy adapter function when a new child is added.
   *
   * @param proxy The proxy element that emitted the signal.
   * @param child The child that was added.
   * @param name The name of the child.
   */
  virtual void process(const Shared_Element<> &proxy, GObject *child,
                       char *name) = 0;
};

/**
 * @brief Interface for child-removed GCallback adapters.
 */
class Child_Removed_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the Child_Removed_Callback object
   */
  virtual ~Child_Removed_Callback() {}

  /**
   * @brief Called by the dummy adapter function when a child is removed.
   *
   * @param proxy The proxy element that emitted the signal.
   * @param child The child that was removed.
   * @param name The name of the child.
   */
  virtual void process(const Shared_Element<> &proxy, GObject *child,
                       char *name) = 0;
};

} // namespace viflow
