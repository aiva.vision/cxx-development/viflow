#pragma once

#include <memory>

namespace viflow {

class Element;

/**
 * @brief A template alias for a weak pointer to an Element.
 *
 * This template is enabled only if E is a base of Element.
 *
 * @tparam E The type of the Element. Defaults to Element.
 */
template <class E = Element,
          typename = std::enable_if_t<std::is_base_of_v<Element, E>>>
using Weak_Element = std::weak_ptr<E>;

} // namespace viflow
