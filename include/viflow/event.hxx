#pragma once

#include <gst/gstevent.h>
#include <memory>

#include "viflow/unique_ptr.hxx"

namespace viflow {
/**
 * @brief Decreases the reference count of the event. If the reference count
 * drops to 0, the event will be freed.
 *
 * @param event The GstEvent to unreference.
 */
inline void gst_event_unref(GstEvent *event) {
  gst_mini_object_unref(GST_MINI_OBJECT_CAST(event));
}

/**
 * @brief Unique pointer for GstEvent with a custom deleter. Useful when dealing
 * with low level GstElement instances.
 */
using unique_event = unique_ptr_fn<GstEvent, viflow::gst_event_unref>;

/**
 * @brief Adapter for GstEvent.
 */
class Event {
public:
  /**
   * @brief Increases the reference count of the event and returns a new Event
   * instance that wraps it.
   *
   * @param e The GstEvent to reference.
   *
   * @return Event The new Event instance.
   */
  static Event ref(GstEvent *e);

  /**
   * @brief Increases the reference count of the event and returns a new Event
   * instance that wraps it.
   *
   * @param e The Event to reference.
   *
   * @return Event The new Event instance.
   */
  static Event ref(const Event &e);
  Event(const Event &event) = delete;
  Event(Event &&event) = default;
  virtual ~Event() {}
  Event &operator=(const Event &event) = delete;
  Event &operator=(Event &&event) = default;
  /**
   * @brief Creates a seek event using gst_event_new_seek().
   *
   * @param rate The rate of the seek.
   * @param format The format of the seek.
   * @param flags The flags for the seek.
   * @param start_type The type of the start of the seek.
   * @param start The start of the seek.
   * @param stop_type The type of the stop of the seek.
   * @param stop The stop of the seek.
   */
  explicit Event(double rate, GstFormat format, GstSeekFlags flags,
                 GstSeekType start_type, long start, GstSeekType stop_type,
                 long stop);

  /**
   * @brief Creates a step event using gst_event_new_step().
   *
   * @param format The format of the step.
   * @param amount The amount of the step.
   * @param rate The rate of the step.
   * @param flush Whether to flush the step.
   * @param intermediate Whether the step is intermediate.
   */
  explicit Event(GstFormat format, unsigned long amount, double rate,
                 bool flush, bool intermediate);

  /**
   * @brief Assumes ownership of an existing low level event.
   *
   * @param event The GstEvent to take ownership of.
   */
  explicit Event(GstEvent *event);

  /**
   * @brief Returns the type of the event.
   *
   * @return GstEventType The type of the event.
   */
  GstEventType type() const noexcept { return core->type; }

  /**
   * @brief Returns the low level event.
   *
   * @return GstEvent* The underlying GstEvent object.
   */
  GstEvent *get_core() const noexcept;

  /**
   * @brief Releases ownership of the low level event.
   *
   * @return GstEvent* The GstEvent object that was released.
   */
  GstEvent *release() noexcept;

private:
  /**
   * @brief Unique pointer for GstEvent with a custom deleter.
   */
  unique_event core;
};
} // namespace viflow
