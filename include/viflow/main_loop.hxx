#pragma once

#include <glib.h>
#include <memory>

#include "viflow/unique_ptr.hxx"

namespace viflow {

/**
 * @brief Useful when dealing with low level GMainLoop instances.
 */
using unique_main_loop = unique_ptr_fn<GMainLoop, g_main_loop_unref>;

/**
 * @brief Uses the singleton pattern to ensure unicity and to provide a global
 * point of access.
 *
 * This is very important for graciously exiting with signal handlers.
 */
class Main_Loop {
public:
  /**
   * @brief Checks if an instance is already registered.
   *
   * @return bool True if an instance is already registered, false otherwise.
   */
  static bool is_registered() noexcept;

  /**
   * @brief Registers a main loop instance.
   *
   * @param ctx The GMainContext instance.
   * @param is_running The running status of the main loop.
   *
   * @return std::unique_ptr<Main_Loop>& The registered main loop instance.
   *
   * @throw Exception If an instance has already been registered.
   */
  static std::unique_ptr<Main_Loop> &
  register_instance(GMainContext *ctx = nullptr, bool is_running = false);

  /**
   * @brief Returns the current registered main loop.
   *
   * @return std::unique_ptr<Main_Loop>& The current registered main loop.
   *
   * @throw Exception If no main loop is currently registered.
   */
  static std::unique_ptr<Main_Loop> &instance();

  Main_Loop(const Main_Loop &loop) = delete;
  Main_Loop(Main_Loop &&loop) = default;
  Main_Loop &operator=(const Main_Loop &loop) = delete;
  Main_Loop &operator=(Main_Loop &&loop) = default;
  ~Main_Loop() = default;

  /**
   * @brief Checks if the main loop is running.
   *
   * @return bool True if the main loop is running, false otherwise.
   */
  bool is_running() const noexcept;

  /**
   * @brief Returns the low level loop.
   *
   * @return GMainLoop* The low level loop.
   */
  GMainLoop *get_core() const noexcept;

  /**
   * @brief Starts the loop.
   */
  void run() const noexcept;

  /**
   * @brief Quits the loop.
   */
  void quit() const noexcept;

private:
  /**
   * @brief The currently registered instance.
   */
  static std::unique_ptr<Main_Loop> main_loop;

  /**
   * @brief Creates a new main loop.
   *
   * @param ctx The GMainContext instance.
   * @param is_running The running status of the main loop.
   */
  explicit Main_Loop(GMainContext *ctx = nullptr, bool is_running = false);

  /**
   * @brief Low level loop.
   */
  unique_main_loop core;
};

} // namespace viflow
