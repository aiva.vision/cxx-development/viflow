#pragma once

#include "viflow/shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {
/**
 * @brief Interface for the about-to-finish signal callback.
 */
class About_To_Finish_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~About_To_Finish_Callback() {}

  /**
   * @brief Called when the about-to-finish signal is emitted.
   *
   * @param playbin The playbin element.
   */
  virtual void process(const Shared_Element<> &playbin) = 0;
};

/**
 * @brief Interface for the audio-changed signal callback.
 */
class Audio_Changed_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Audio_Changed_Callback() {}

  /**
   * @brief Called when the audio-changed signal is emitted.
   *
   * @param playbin The playbin element.
   */
  virtual void process(const Shared_Element<> &playbin) = 0;
};

/**
 * @brief Interface for the audio-tags-changed signal callback.
 */
class Audio_Tags_Changed_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Audio_Tags_Changed_Callback() {}

  /**
   * @brief Called when the audio-tags-changed signal is emitted.
   *
   * @param playbin The playbin element.
   * @param stream The stream index.
   */
  virtual void process(const Shared_Element<> &playbin, int stream) = 0;
};

/**
 * @brief Interface for the element-setup signal callback.
 */
class Element_Setup_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Element_Setup_Callback() {}

  /**
   * @brief Called when the element-setup signal is emitted.
   *
   * @param playbin The playbin element.
   * @param element The element that was set up.
   */
  virtual void process(const Shared_Element<> &playbin,
                       const Shared_Element<> &element) = 0;
};

/**
 * @brief Interface for the source-setup signal callback.
 */
class Source_Setup_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Source_Setup_Callback() {}

  /**
   * @brief Called when the source-setup signal is emitted.
   *
   * @param playbin The playbin element.
   * @param source The source that was set up.
   */
  virtual void process(const Shared_Element<> &playbin,
                       const Shared_Element<> &source) = 0;
};

/**
 * @brief Interface for the text-changed signal callback.
 */
class Text_Changed_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Text_Changed_Callback() {}

  /**
   * @brief Called when the text-changed signal is emitted.
   *
   * @param playbin The playbin element.
   */
  virtual void process(const Shared_Element<> &playbin) = 0;
};

/**
 * @brief Interface for the text-tags-changed signal callback.
 */
class Text_Tags_Changed_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Text_Tags_Changed_Callback() {}

  /**
   * @brief Called when the text-tags-changed signal is emitted.
   *
   * @param playbin The playbin element.
   * @param stream The stream index.
   */
  virtual void process(const Shared_Element<> &playbin, int stream) = 0;
};

/**
 * @brief Interface for the video-changed signal callback.
 */
class Video_Changed_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Video_Changed_Callback() {}

  /**
   * @brief Called when the video-changed signal is emitted.
   *
   * @param playbin The playbin element.
   */
  virtual void process(const Shared_Element<> &playbin) = 0;
};

/**
 * @brief Interface for the video-tags-changed signal callback.
 */
class Video_Tags_Changed_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Video_Tags_Changed_Callback() {}

  /**
   * @brief Called when the video-tags-changed signal is emitted.
   *
   * @param playbin The playbin element.
   * @param stream The stream index.
   */
  virtual void process(const Shared_Element<> &playbin, int stream) = 0;
};

} // namespace viflow
