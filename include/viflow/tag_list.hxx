#pragma once

#include <gst/gsttaglist.h>
#include <string>

#include "viflow/unique_ptr.hxx"

namespace viflow {
/**
 * @brief Unreferences a GstTagList instance.
 * @param tag_list The GstTagList instance.
 */
inline void gst_tag_list_unref(GstTagList *tag_list) {
  gst_mini_object_unref(GST_MINI_OBJECT_CAST(tag_list));
}

/**
 * @brief Useful when dealing with low level GstTagList instances.
 */
using unique_tag_list = unique_ptr_fn<GstTagList, viflow::gst_tag_list_unref>;

/**
 * @brief Adapter for GstTagList.
 */
class Tag_List {
public:
  /**
   * @brief Increases the reference count and takes ownership of the tag list.
   *
   * @param t The GstTagList instance.
   *
   * @return Tag_List The Tag_List instance.
   */
  static Tag_List ref(GstTagList *t);

  /**
   * @brief Increases the reference count and takes ownership of the tag list.
   *
   * @param t The Tag_List instance.
   *
   * @return Tag_List The Tag_List instance.
   */
  static Tag_List ref(const Tag_List &t);
  ;

  Tag_List() = default;
  Tag_List(const Tag_List &t) = delete;
  Tag_List(Tag_List &&t) = default;
  virtual ~Tag_List() {}
  Tag_List &operator=(const Tag_List &t) = delete;
  Tag_List &operator=(Tag_List &&t) = default;

  /**
   * @brief Takes ownership of the tag list.
   *
   * @param taglist The GstTagList instance.
   */
  explicit Tag_List(GstTagList *taglist);

  /**
   * @brief Checks whether the low level tag list is null.
   *
   * @return bool True if the tag list is null, false otherwise.
   */
  bool is_empty();

  /**
   * @brief Retrieves a tag value.
   *
   * @tparam T The type of the tag value.
   *
   * @param tag The tag name.
   *
   * @return T The tag value.
   *
   * @throw This function may throw exceptions.
   */
  template <typename T> T get(const std::string &tag) const;

  /**
   * @brief Returns the low level tag list.
   *
   * @return GstTagList* Pointer to the low level tag list.
   */
  GstTagList *get_core() const noexcept { return core.get(); }

  /**
   * @brief Releases the ownership of the low level tag list.
   *
   * @return GstTagList* Pointer to the low level tag list.
   */
  GstTagList *release() noexcept { return core.release(); }

private:
  /**
   * @brief Low level representation of the tag list.
   */
  unique_tag_list core;
};

} // namespace viflow

#include "viflow/tag_list.txx"
