#pragma once

#include "viflow/app_source_callback.hxx"
#include "viflow/buffer.hxx"
#include "viflow/element.hxx"

namespace viflow {

/**
 * @brief Namespace for App Source signal names
 */
namespace App_Source_Signals {

constexpr char const *need_data = "need-data";
constexpr char const *enough_data = "enough-data";
constexpr char const *seek_data = "seek-data";

} // namespace App_Source_Signals
/**
 * @brief Adapter for GstAppSrc.
 */
class App_Source : public Element {
public:
  /**
   * @brief Destroy the App_Source object
   */
  virtual ~App_Source() {}

  /**
   * @brief Push a buffer.
   *
   * @param buffer The buffer to push.
   *
   * @return GST_FLOW_OK if the buffer was pushed successfully.
   */
  GstFlowReturn push_buffer(Buffer &&buffer);

  /*
   * Expose the existing callback methods.
   */
  using Element::connect_callback;

  /**
   * @brief Connects a callback for the enough-data signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<Enough_Data_Callback> &&callback);

  /**
   * @brief Connects a callback for the need-data signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<Need_Data_Callback> &&callback);

  /**
   * @brief Connects a callback for the seek-data signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<Seek_Data_Callback> &&callback);

protected:
  /**
   * @brief Construct a new App_Source element.
   *
   * @param name The name of the element.
   */
  App_Source(const std::string &name);

  friend class Element;
};

} // namespace viflow
