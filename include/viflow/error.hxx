#pragma once

#include <glib.h>

#include "viflow/unique_ptr.hxx"

namespace viflow {

/**
 * @brief Unique pointer for GError with a custom deleter.
 */
using unique_error = unique_ptr_fn<GError, g_error_free>;

/**
 * @brief Adapter for GError.
 */
class Error {
public:
  Error() = default;
  Error(Error &&) = default;
  Error(const Error &) = delete;
  Error &operator=(Error &&) = default;
  Error &operator=(const Error &) = delete;
  ~Error() = default;

  /**
   * @brief Assumes ownership of error.
   *
   * @param error The GError to take ownership of.
   */
  explicit Error(GError *error);

  /**
   * @brief Returns the low level glib error.
   *
   * @return GError* The underlying GError object.
   */
  GError *get_core() const noexcept { return core.get(); }

  /**
   * @brief Returns the address of the low level glib error.
   *
   * @return GError** The address of the underlying GError object.
   */
  GError **get_core_address() noexcept { return core.get_address_of(); }

  /**
   * @brief Returns the error's message.
   *
   * @return char* The error message.
   */
  char *message() const noexcept { return core->message; }

  /**
   * @brief Returns the error's domain.
   *
   * @return GQuark The error domain.
   */
  GQuark domain() const noexcept { return core->domain; }

  /**
   * @brief Returns the error's code.
   *
   * @return gint The error code.
   */
  gint code() const noexcept { return core->code; }

private:
  unique_error core;
};

} // namespace viflow
