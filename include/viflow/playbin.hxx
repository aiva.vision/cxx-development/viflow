#pragma once

#include <gst/gst.h>
#include <string>

#include "viflow/pipeline.hxx"
#include "viflow/playbin_callback.hxx"
#include "viflow/sample.hxx"
#include "viflow/tag_list.hxx"

namespace viflow {

/**
 * @brief Namespace for Playbin signals.
 */
namespace Playbin_Signals {

constexpr const char *about_to_finish = "about-to-finish";
constexpr const char *audio_changed = "audio-changed";
constexpr const char *audio_tags_changed = "audio-tags-changed";
constexpr const char *element_setup = "element-setup";
constexpr const char *source_setup = "source-setup";
constexpr const char *text_changed = "text-changed";
constexpr const char *text_tags_changed = "text-tags-changed";
constexpr const char *video_changed = "video-changed";
constexpr const char *video_tags_changed = "video-tags-changed";

} // namespace Playbin_Signals

/**
 * @brief Namespace for Playbin action signals.
 */
namespace Playbin_Action_Signals {

constexpr const char *convert_sample = "convert-sample";
constexpr const char *get_audio_pad = "get-audio-pad";
constexpr const char *get_audio_tags = "get-audio-tags";
constexpr const char *get_text_pad = "get-text-pad";
constexpr const char *get_text_tags = "get-text-tags";
constexpr const char *get_video_pad = "get-video-pad";
constexpr const char *get_video_tags = "get-video-tags";

} // namespace Playbin_Action_Signals

/**
 * @brief Namespace for Playbin properties.
 */
namespace Playbin_Properties {

constexpr const char *audio_filter = "audio-filter";
constexpr const char *audio_sink = "audio-sink";
constexpr const char *audio_stream_combiner = "audio-stream-combiner";
constexpr const char *av_offset = "av-offset";
constexpr const char *buffer_duration = "buffer-duration";
constexpr const char *buffer_size = "buffer-size";
constexpr const char *connection_speed = "connection-speed";
constexpr const char *current_audio = "current-audio";
constexpr const char *current_suburi = "current-suburi";
constexpr const char *current_text = "current-text";
constexpr const char *current_uri = "current-uri";
constexpr const char *current_video = "current-video";
constexpr const char *flags = "flags";
constexpr const char *force_aspect_ratio = "force-aspect-ratio";
constexpr const char *mute = "mute";
constexpr const char *n_audio = "n-audio";
constexpr const char *n_text = "n-text";
constexpr const char *n_video = "n-video";
constexpr const char *ring_buffer_max_size = "ring-buffer-max-size";
constexpr const char *sample = "sample";
constexpr const char *source = "source";
constexpr const char *subtitle_encoding = "subtitle-encoding";
constexpr const char *subtitle_font_desc = "subtitle-font-desc";
constexpr const char *suburi = "suburi";
constexpr const char *text_offset = "text-offset";
constexpr const char *text_sink = "text-sink";
constexpr const char *text_stream_combiner = "text-stream-combiner";
constexpr const char *uri = "uri";
constexpr const char *video_filter = "video-filter";
constexpr const char *video_multiview_flags = "video-multiview-flags";
constexpr const char *video_multiview_mode = "video-multiview-mode";
constexpr const char *video_sink = "video-sink";
constexpr const char *video_stream_combiner = "video-stream-combiner";
constexpr const char *vis_plugin = "vis-plugin";
constexpr const char *volume = "volume";

} // namespace Playbin_Properties

/**
 * @brief Class for handling playbin operations.
 */
class Playbin : public Pipeline {
public:
  virtual ~Playbin() {}

  /**
   * @brief Retrieves the currently playing video frame in the format specified
   * by caps.
   *
   * @param caps The GstCaps instance.
   *
   * @return Sample The retrieved sample.
   *
   * @throw Exception If the sample cannot be retrieved.
   */
  Sample convert_sample(GstCaps *caps);

  /**
   * @brief Retrieves the currently playing video frame in the format specified
   * by caps.
   *
   * @param caps The Capabilities instance.
   *
   * @return Sample The retrieved sample.
   *
   * @throw Exception If the sample cannot be retrieved.
   */
  Sample convert_sample(const Capabilities &caps);

  /**
   * @brief Retrieves the stream-combiner sinkpad for a specific audio stream.
   *
   * @param stream The stream index.
   *
   * @return Pad& The retrieved pad.
   *
   * @throw Exception If the pad cannot be retrieved.
   */
  Pad &get_audio_pad(int stream);

  /**
   * @brief Retrieves the tags of a specific audio stream number.
   *
   * @param stream The stream index.
   *
   * @return Tag_List The retrieved tags.
   *
   * @throw Exception If the tags cannot be retrieved.
   */
  Tag_List get_audio_tags(int stream);

  /**
   * @brief Retrieves the stream-combiner sinkpad for a specific text stream.
   *
   * @param stream The stream index.
   *
   * @return Pad& The retrieved pad.
   *
   * @throw Exception If the pad cannot be retrieved.
   */
  Pad &get_text_pad(int stream);

  /**
   * @brief Retrieves the tags of a specific text stream number.
   *
   * @param stream The stream index.
   *
   * @return Tag_List The retrieved tags.
   *
   * @throw Exception If the tags cannot be retrieved.
   */
  Tag_List get_text_tags(int stream);

  /**
   * @brief Retrieves the stream-combiner sinkpad for a specific video stream.
   *
   * @param stream The stream index.
   *
   * @return Pad& The retrieved pad.
   *
   * @throw Exception If the pad cannot be retrieved.
   */
  Pad &get_video_pad(int stream);

  /**
   * @brief Retrieves the tags of a specific video stream number.
   *
   * @param stream The stream index.
   *
   * @return Tag_List The retrieved tags.
   *
   * @throw Exception If the tags cannot be retrieved.
   */
  Tag_List get_video_tags(int stream);

  /**
   * @brief Exposes the existing callback methods from the Pipeline class.
   */
  using Pipeline::connect_callback;

  /**
   * @brief Connects a callback to the 'about-to-finish' signal.
   *
   * @param callback The About_To_Finish_Callback instance.
   */
  void connect_callback(std::unique_ptr<About_To_Finish_Callback> &&callback);

  /**
   * @brief Connects a callback to the 'audio-changed' signal.
   *
   * @param callback The Audio_Changed_Callback instance.
   */
  void connect_callback(std::unique_ptr<Audio_Changed_Callback> &&callback);

  /**
   * @brief Connects a callback to the 'audio-tags-changed' signal.
   *
   * @param callback The Audio_Tags_Changed_Callback instance.
   */
  void
  connect_callback(std::unique_ptr<Audio_Tags_Changed_Callback> &&callback);

  /**
   * @brief Connects a callback to the 'element-setup' signal.
   *
   * @param callback The Element_Setup_Callback instance.
   */
  void connect_callback(std::unique_ptr<Element_Setup_Callback> &&callback);

  /**
   * @brief Connects a callback to the 'source-setup' signal.
   *
   * @param callback The Source_Setup_Callback instance.
   */
  void connect_callback(std::unique_ptr<Source_Setup_Callback> &&callback);

  /**
   * @brief Connects a callback to the 'text-changed' signal.
   *
   * @param callback The Text_Changed_Callback instance.
   */
  void connect_callback(std::unique_ptr<Text_Changed_Callback> &&callback);

  /**
   * @brief Connects a callback to the 'text-tags-changed' signal.
   *
   * @param callback The Text_Tags_Changed_Callback instance.
   */
  void connect_callback(std::unique_ptr<Text_Tags_Changed_Callback> &&callback);

  /**
   * @brief Connects a callback to the 'video-changed' signal.
   *
   * @param callback The Video_Changed_Callback instance.
   */
  void connect_callback(std::unique_ptr<Video_Changed_Callback> &&callback);

  /**
   * @brief Connects a callback to the 'video-tags-changed' signal.
   *
   * @param callback The Video_Tags_Changed_Callback instance.
   */
  void
  connect_callback(std::unique_ptr<Video_Tags_Changed_Callback> &&callback);

protected:
  /**
   * @brief Constructor for Playbin.
   *
   * @param name The name of the Playbin.
   */
  Playbin(const std::string &name);

  /**
   * @brief Constructor for Playbin with a URI.
   *
   * @param name The name of the Playbin.
   * @param uri The URI for the Playbin.
   *
   * @throw Exception If the Playbin cannot be created with the given URI.
   */
  Playbin(const std::string &name, const std::string &uri);

  friend class Element;
};

} // namespace viflow
