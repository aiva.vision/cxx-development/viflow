#pragma once

#include "viflow/bin.hxx"
#include "viflow/buffer.hxx"
#include "viflow/queue.hxx"

namespace viflow {

/**
 * @brief The inside-pipeline equivalent of appsrc and appsink.
 */
class App_Transform : public Bin {
public:
  /**
   * @brief Fast access to the internal queue.
   *
   * @return Shared_Element<Queue> The internal queue
   */
  Shared_Element<Queue> queue();

  /**
   * @brief Set caps on the sink caps filter element.
   *
   * @param caps The caps to set.
   */
  void set_sink_caps(const Capabilities &caps);

  /**
   * @brief Set caps on the src caps filter element.
   *
   * @param caps The caps to set.
   */
  void set_src_caps(const Capabilities &caps);

  /**
   * @brief Called for every event. The callback is on the internal queue's sink
   * pad.
   *
   * @param event The event to process
   */
  virtual void process(GstEvent *event) {}

  /**
   * @brief Called for every buffer. The callback is on the internal queue's src
   * pad.
   *
   * @param buf The buffer to process
   */
  virtual void process(Buffer &buf) = 0;

protected:
  /**
   * @brief Construct a new App_Transform element.
   *
   * @param name The name of the element.
   */
  App_Transform(const std::string &name);

  friend class Element;
};

} // namespace viflow
