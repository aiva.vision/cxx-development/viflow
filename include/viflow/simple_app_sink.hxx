#pragma once

#include "viflow/element.hxx"
#include "viflow/sample.hxx"

namespace viflow {

/**
 * @brief Class for handling simple app sink operations.
 */
class Simple_App_Sink : public Element {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Simple_App_Sink() {}

  /**
   * @brief Processes a sample.
   *
   * @param sample The Sample instance.
   *
   * @return GstFlowReturn The result of the processing.
   *
   * @throw This function may throw exceptions.
   */
  virtual GstFlowReturn process(Sample &sample) = 0;

protected:
  /**
   * @brief Creates an element with the appsink factory.
   *
   * @param name The name of the element.
   */
  Simple_App_Sink(const std::string &name);
};

} // namespace viflow
