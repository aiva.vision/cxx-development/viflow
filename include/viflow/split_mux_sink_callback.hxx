#pragma once

#include <gst/gstsample.h>

#include "viflow/capabilities.hxx"
#include "viflow/shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {

/**
 * @brief Interface for format-location GCallback adapters.
 */
class Format_Location_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Format_Location_Callback() {}

  /**
   * @brief Called by the dummy adapter function to get the location for the
   * next file.
   *
   * @param splitmux The Shared_Element instance.
   * @param fragment_id The fragment id.
   *
   * @return std::string The location for the next file.
   */
  virtual std::string process(const Shared_Element<> &splitmux,
                              unsigned fragment_id) = 0;
};

/**
 * @brief Interface for format-location-full GCallback adapters.
 */
class Format_Location_Full_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Format_Location_Full_Callback() {}

  /**
   * @brief Called by the dummy adapter function to get the location for the
   * next file.
   *
   * @param splitmux The Shared_Element instance.
   * @param fragment_id The fragment id.
   * @param first_sample The GstSample instance.
   *
   * @return std::string The location for the next file.
   */
  virtual std::string process(const Shared_Element<> &splitmux,
                              unsigned fragment_id,
                              GstSample *first_sample) = 0;
};

/**
 * @brief Interface for muxer-added GCallback adapters.
 */
class Muxer_Added_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Muxer_Added_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the muxer has been added.
   *
   * @param splitmux The Shared_Element instance.
   * @param muxer The Shared_Element instance for the muxer.
   */
  virtual void process(const Shared_Element<> &splitmux,
                       const Shared_Element<> &muxer) = 0;
};

/**
 * @brief Interface for sink-added GCallback adapters.
 */
class Sink_Added_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Sink_Added_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the sink has been added.
   *
   * @param splitmux The Shared_Element instance.
   * @param sink The Shared_Element instance for the sink.
   */
  virtual void process(const Shared_Element<> &splitmux,
                       const Shared_Element<> &sink) = 0;
};

} // namespace viflow
