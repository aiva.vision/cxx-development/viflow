#pragma once

#include <glib.h>
#include <iterator>

namespace viflow {
/**
 * @brief Iterator for GList.
 */
class GList_Iterator {
public:
  using iterator_category = std::bidirectional_iterator_tag;
  using difference_type = std::ptrdiff_t;
  using value_type = void;
  using pointer = value_type *;

  GList_Iterator(const GList_Iterator &iterator) : glist{iterator.glist} {}

  GList_Iterator(GList_Iterator &&iterator) : glist{iterator.glist} {
    iterator.glist = nullptr;
  }

  GList_Iterator &operator=(const GList_Iterator &iterator) {
    glist = iterator.glist;
    return *this;
  }

  GList_Iterator &operator=(GList_Iterator &&iterator) {
    glist = iterator.glist;
    iterator.glist = nullptr;
    return *this;
  }

  ~GList_Iterator() = default;

  /**
   * @brief Constructor that initializes the iterator from a GList.
   *
   * @param list The GList to initialize the iterator from.
   */
  explicit GList_Iterator(const GList *list) : glist{list} {}

  /**
   * @brief Dereference operator.
   *
   * @return pointer The pointer to the current element.
   */
  pointer operator*() const { return glist ? glist->data : nullptr; }

  /**
   * @brief Access operator.
   *
   * @return const GList* The pointer to the current GList.
   */
  const GList *operator->() const { return glist; }

  /**
   * @brief Prefix increment operator.
   *
   * @return GList_Iterator& A reference to this iterator.
   */
  GList_Iterator &operator++() {
    glist = g_list_next(glist);
    return *this;
  }

  /**
   * @brief Postfix increment operator.
   *
   * @return GList_Iterator The iterator before incrementing.
   */
  GList_Iterator operator++(int) {
    GList_Iterator tmp = *this;
    ++(*this);
    return tmp;
  }

  /**
   * @brief Prefix decrement operator.
   *
   * @return GList_Iterator& A reference to this iterator.
   */
  GList_Iterator &operator--() {
    glist = g_list_previous(glist);
    return *this;
  }

  /**
   * @brief Postfix decrement operator.
   *
   * @return GList_Iterator The iterator before decrementing.
   */
  GList_Iterator operator--(int) {
    GList_Iterator tmp = *this;
    --(*this);
    return tmp;
  }

  /**
   * @brief Equality comparison operator.
   *
   * @param iter The iterator to compare with.
   *
   * @return bool True if the iterators are equal, false otherwise.
   */
  bool operator==(const GList_Iterator &iter) { return glist == iter.glist; };

  /**
   * @brief Inequality comparison operator.
   *
   * @param iter The iterator to compare with.
   *
   * @return bool True if the iterators are not equal, false otherwise.
   */
  bool operator!=(const GList_Iterator &iter) { return glist != iter.glist; };

private:
  const GList *glist;
};

} // namespace viflow
