#pragma once

#ifndef VIFLOW_ELEMENT
#include "viflow/element.hxx"
#endif // VIFLOW_ELEMENT

#include "viflow/exceptions.hxx"

template <class E, typename... Args>
viflow::Shared_Element<E> viflow::Element::register_instance(Args &&...args) {

  Shared_Element<E> elem{new E{std::forward<Args>(args)...}};
  std::unique_lock<std::mutex> register_lock{register_mutex};

  auto name = elem->get_name();
  if (!instances_register.contains(name)) {

    instances_register.insert_or_assign(name, Shared_Element<E>{elem});
    return elem;

  } else {
    throw Element_Already_Registered(name);
  }
}

template <class E>
viflow::Shared_Element<E> viflow::Element::instance(const std::string &name) {

  auto base_instance = instance<Element>(name);
  auto derived_instance = std::dynamic_pointer_cast<E>(base_instance);

  if (!derived_instance) {
    throw Bad_Element_Cast(base_instance->get_name());
  }
  return derived_instance;
}

template <>
inline viflow::Shared_Element<viflow::Element>
viflow::Element::instance<viflow::Element>(const std::string &name) {

  try {
    return instances_register.at(name);

  } catch (const std::out_of_range &e) {
    throw Element_Not_Registered(name);
  }
}

template <class E>
viflow::Shared_Element<E> viflow::Element::convert(GstElement *elem) {

  if (!elem) {
    throw Exception{"Can not convert null element"};
  }
  if (is_registered(elem)) {
    return instance<E>(GST_ELEMENT_NAME(elem));
  }
  return register_instance<E>(elem);
}

template <typename T>
T viflow::Element::get_property(const std::string &name) const noexcept {
  T value;
  g_object_get(core.get(), name.c_str(), &value, nullptr);
  return value;
}

template <typename E>
viflow::Shared_Element<E> viflow::Element::get_parent() const {
  auto parent = GST_ELEMENT_PARENT(core.get());
  if (!parent) {
    throw Exception{
        std::string{"The element '" + get_name() + "' has no parent"}};
  }
  return viflow::Element::convert<E>(parent);
}

template <typename T>
void viflow::Element::set_property(const std::string &name,
                                   const T value) noexcept {
  g_object_set(core.get(), name.c_str(), value, nullptr);
}

template <class T> bool viflow::Element::can_cast() {
  return dynamic_cast<T *>(this);
}

template <class T> T *viflow::Element::cast() {
  T *elem = dynamic_cast<T *>(this);
  if (!elem) {
    throw Bad_Element_Cast(get_name());
  }
  return elem;
}

template <typename T, typename>
void viflow::Element::connect_callback(
    const std::string &signal, T adapter,
    std::unique_ptr<Signal_Callback> &&callback) {

  auto id = g_signal_connect(G_OBJECT(core.get()), signal.c_str(),
                             G_CALLBACK(adapter), callback.get());

  registered_callbacks.insert_or_assign(
      signal, std::make_pair(id, std::move(callback)));
}
