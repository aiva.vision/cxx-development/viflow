#pragma once

#include "viflow/bin.hxx"
#include "viflow/buffer.hxx"
#include "viflow/child_proxy_callback.hxx"

namespace viflow {

/**
 * @brief Namespace for Child Proxy signal names
 */
namespace Child_Proxy_Signals {

constexpr char const *child_added = "child-added";
constexpr char const *child_removed = "child-removed";

} // namespace Child_Proxy_Signals
/**
 * @brief Class representing a Child Proxy element.
 */
class Child_Proxy : public Bin {
public:
  /**
   * @brief Destroy the Child_Proxy object
   */
  virtual ~Child_Proxy() {}

  /**
   * @brief Expose the existing callback methods.
   */
  using Element::connect_callback;

  /**
   * @brief Connects a callback for the child-added signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<Child_Added_Callback> &&callback);

  /**
   * @brief Connects a callback for the child-removed signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<Child_Removed_Callback> &&callback);

protected:
  /**
   * @brief Construct a new Child_Proxy element.
   */
  using Bin::Bin;

  friend class Element;
};

} // namespace viflow
