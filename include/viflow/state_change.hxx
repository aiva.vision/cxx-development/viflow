#pragma once

#include <gst/gstelement.h>

namespace viflow {

/**
 * @struct State_Change
 * @brief Structure for handling state changes.
 */
struct State_Change {
  GstStateChangeReturn ret;
  GstState current;
  GstState pending;
};

} // namespace viflow
