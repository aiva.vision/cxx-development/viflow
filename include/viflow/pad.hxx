#pragma once

#define VIFLOW_PAD

#include "viflow/capabilities.hxx"
#include "viflow/element.hxx"
#include "viflow/event.hxx"
#include "viflow/pad_probe.hxx"
#include "viflow/shared_element.hxx"
#include "viflow/unique_ptr.hxx"

namespace viflow {

class Buffer;
/**
 * @brief Useful when dealing with low level GstPad instances.
 */
using unique_pad = unique_ptr_fn<GstPad, gst_object_unref>;

/**
 * @brief Adapter for GstPad. The class doesn't manage its own instances, but
 * the Element objects do. Therefore, they are unique per element.
 */
class Pad {
public:
  /**
   * @brief Converts a low level pad to a viflow::Pad.
   *
   * This does NOT assume ownership of the original pad.
   *
   * @param pad The GstPad instance.
   *
   * @return Pad& The converted pad.
   */
  static Pad &convert(GstPad *pad);

  Pad() = delete;
  Pad(Pad &&pad) = default;
  Pad(const Pad &) = delete;
  Pad &operator=(Pad &&pad) = default;
  Pad &operator=(const Pad &) = delete;
  ~Pad();

  /**
   * @brief Assumes ownership of pad.
   *
   * @param pad The GstPad instance.
   * @param dynamic Whether the pad is dynamic.
   */
  explicit Pad(GstPad *pad, bool dynamic = false);

  /**
   * @brief Checks whether the pad is dynamic.
   *
   * @return bool True if the pad is dynamic, false otherwise.
   */
  bool is_dynamic() const noexcept { return dynamic; }

  /**
   * @brief Checks whether the pad has already been linked.
   *
   * @return bool True if the pad is linked, false otherwise.
   */
  bool is_linked() const noexcept;

  /**
   * @brief Returns the name of the pad.
   *
   * @return std::string The name of the pad.
   */
  std::string get_name() const noexcept;

  /**
   * @brief Returns a pointer to the low level core.
   *
   * @return GstPad* The low level core.
   */
  GstPad *get_core() const noexcept;

  /**
   * @brief Set a property of the pad.
   *
   * @tparam T The type of the property.
   * @param name The name of the property.
   * @param value The value of the property.
   */
  template <typename T>
  void set_property(const std::string &name, const T &value) const noexcept;

  /**
   * @brief Returns the peer pad.
   *
   * @return Pad& The peer pad.
   *
   * @throw Pad_Not_Found If the peer pad is not found.
   */
  Pad &get_peer_pad() const;

  /**
   * @brief Returns the parent element.
   *
   * @return Shared_Element<> The parent element.
   */
  Shared_Element<> get_parent() const;

  /**
   * @brief Retrieve the current capabilities.
   *
   * If it fails, all possible caps are returned.
   *
   * @return Capabilities The current capabilities.
   *
   * @throw Unavailable_Caps If the capabilities are unavailable.
   */
  Capabilities get_caps() const;

  /**
   * @brief Verifies if the pad has current caps.
   *
   * @return bool True if the pad has current caps, false otherwise.
   */
  bool has_current_caps() const noexcept;

  /**
   * @brief Returns the pad's current capabilities.
   *
   * @return Capabilities The current capabilities.
   *
   * @throw Unavailable_Caps If the capabilities are unavailable.
   */
  Capabilities get_current_caps() const;

  /**
   * @brief Returns the pad's allowed capabilities.
   *
   * @return Capabilities The allowed capabilities.
   */
  Capabilities get_allowed_caps() const;

  /**
   * @brief Returns the pad's all possible capabilities.
   *
   * @return Capabilities All possible capabilities.
   *
   * @throw Unavailable_Caps If the capabilities are unavailable.
   */
  Capabilities query_caps() const;
  /**
   * @brief Set capabilities for the pad.
   *
   * @param caps The capabilities to set.
   *
   * @return bool True if the capabilities were set successfully, false
   * otherwise.
   */
  bool set_caps(const Capabilities &caps) noexcept;

  /**
   * @brief Use fixed capabilities for this pad.
   */
  void use_fixed_caps();

  /**
   * @brief Returns a pointer to a ghost pad with the core as target.
   *
   * @param name The name of the ghost pad.
   *
   * @return Pad The created ghost pad.
   */
  Pad create_ghost_pad(const std::string &name) const noexcept;

  /**
   * @brief Links this pad to the argument pad.
   *
   * @param pad The GstPad to link to.
   *
   * @throw Pad_Link_Error If the pads cannot be linked.
   */
  void link(GstPad *pad);

  /**
   * @brief Links this pad to the argument pad.
   *
   * @param pad The Pad to link to.
   *
   * @throw Pad_Link_Error If the pads cannot be linked.
   */
  void link(Pad &pad);

  /**
   * @brief Unlinks this pad from its peer.
   *
   * @throw Pad_Unlink_Error If the pad cannot be unlinked.
   */
  void unlink();

  /**
   * @brief Checks if a probe is registered.
   *
   * @return bool True if a probe is registered, false otherwise.
   */
  bool has_probe() const noexcept;

  /**
   * @brief Adds a probe to the pad.
   *
   * @param probe The Pad_Probe to add.
   *
   * @return unsigned long The ID of the added probe.
   *
   * @throw Exception If the probe cannot be added.
   */
  unsigned long add_probe(std::unique_ptr<Pad_Probe> &&probe);

  /**
   * @brief Removes the last added probe.
   *
   * @throw Exception If the probe cannot be removed.
   */
  void remove_probe();
  /**
   * @brief Sends an event to this pad.
   *
   * @param event The GstEvent instance.
   *
   * @return bool True if the event was sent successfully, false otherwise.
   */
  bool send_event(GstEvent *event) noexcept;

  /**
   * @brief Sends an event to this pad.
   *
   * @param event The Event instance.
   *
   * @return bool True if the event was sent successfully, false otherwise.
   */
  bool send_event(Event &&event) noexcept;

  /**
   * @brief Pushes an event to this pad's peer.
   *
   * @param event The GstEvent instance.
   *
   * @return bool True if the event was pushed successfully, false otherwise.
   */
  bool push_event(GstEvent *event) noexcept;

  /**
   * @brief Pushes an event to this pad's peer.
   *
   * @param event The Event instance.
   *
   * @return bool True if the event was pushed successfully, false otherwise.
   */
  bool push_event(Event &&event) noexcept;

  /**
   * @brief Pushes a buffer to this pad's peer.
   *
   * @param buf The Buffer instance.
   *
   * @return GstFlowReturn The result of the operation.
   */
  GstFlowReturn push_buffer(Buffer &&buf) noexcept;

private:
  /**
   * @brief Low level pad.
   */
  unique_pad core;

  /**
   * @brief Set in the constructor depending on how it was built.
   */
  bool dynamic{false};

  /**
   * @brief Probe for processing events.
   */
  std::unique_ptr<Pad_Probe> pad_probe;

  /**
   * @brief The registered probe's id.
   */
  unsigned long probe_id{0};
};

} // namespace viflow

#include "viflow/pad.txx"
