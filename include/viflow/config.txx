#pragma once

#include "viflow/exceptions.hxx"

#ifndef VIFLOW_CONFIG
#include "viflow/config.hxx"
#endif // VIFLOW_CONFIG

template <typename T>
T viflow::get_value(const libconfig::Setting &setting,
                    const std::string &field) {
  try {
    T value = setting[field];
    return value;
  } catch (const libconfig::SettingNotFoundException &e) {
    throw Misssing_Property(e.getPath());
  } catch (const libconfig::SettingTypeException &e) {
    throw Property_Type_Error(e.getPath());
  }
}

template <typename T>
void viflow::set_prop(const Shared_Element<> &elem,
                      const libconfig::Setting &config,
                      const std::string &prop) {

  return set_prop<T>(elem.get(), config, prop);
}

template <typename T>
void viflow::set_prop(viflow::Element *elem, const libconfig::Setting &config,
                      const std::string &prop) {
  try {
    T value = config[prop];
    elem->set_property(prop, value);
  } catch (const libconfig::SettingNotFoundException &e) {
    throw Misssing_Property(e.getPath(), elem->get_name());
  } catch (const libconfig::SettingTypeException &e) {
    throw Property_Type_Error(e.getPath(), elem->get_name());
  }
}

template <typename T>
void viflow::try_set_prop(const Shared_Element<> &elem,
                          const libconfig::Setting &config,
                          const std::string &prop) {
  try_set_prop<T>(elem.get(), config, prop);
}

template <typename T>
void viflow::try_set_prop(viflow::Element *elem,
                          const libconfig::Setting &config,
                          const std::string &prop) {
  if (config.exists(prop)) {
    try {
      T value = config[prop];
      elem->set_property(prop, value);
    } catch (const libconfig::SettingTypeException &e) {
      throw Property_Type_Error(e.getPath(), elem->get_name());
    }
  }
}
