#pragma once

#include "viflow/element.hxx"

namespace viflow {

/**
 * @brief Namespace for Tee properties.
 */
namespace Tee_Props {

constexpr const char *allow_not_linked = "allow-not-linked";
constexpr const char *has_chain = "has-chain";
constexpr const char *last_message = "last-message";
constexpr const char *name = "name";
constexpr const char *num_src_pads = "num-src-pads";
constexpr const char *parent = "parent";
constexpr const char *pull_mode = "pull-mode";
constexpr const char *silent = "silent";

} // namespace Tee_Props

/**
 * @brief Class for handling tee operations.
 */
class Tee : public Element {
public:
  /**
   * @brief Destructor. This class is very likely to be inherited from.
   */
  virtual ~Tee() {}

  /**
   * @brief Exposes the existing link methods.
   */
  using Element::link;

  /**
   * @brief Requests a pad from the tee using this template: "src_%u".
   *
   * The pad is then linked to the provided element's sink pad.
   *
   * @param elem The Shared_Element instance.
   *
   * @return Shared_Element<> The linked element.
   */
  Shared_Element<> link(const Shared_Element<> &elem);

protected:
  /**
   * @brief Creates a named element.
   *
   * @param name The name of the element.
   */
  Tee(const std::string &name);

  friend class Element;
};
;

} // namespace viflow
