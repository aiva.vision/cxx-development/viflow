#pragma once

#include <glib.h>
#include <memory>
#include <unordered_map>

#include "viflow/io_channel_watch.hxx"
#include "viflow/unique_ptr.hxx"

namespace viflow {
/**
 * @brief Useful when dealing with low level GIOChannel instances.
 */
using unique_io_channel = unique_ptr_fn<GIOChannel, g_io_channel_unref>;

/**
 * @brief Adapter for GIOChannel. Uses the singleton pattern to avoid releasing
 *        the callback when going out of scope.
 */
class IO_Channel {
public:
  /**
   * @brief Returns a pointer to the channel instance.
   *
   * @param fd The file descriptor for the IO channel.
   *
   * @return std::shared_ptr<IO_Channel> The shared pointer to the IO channel
   * instance.
   */
  static std::shared_ptr<IO_Channel> instance(int fd);

  /**
   * @brief Returns a pointer to the channel instance.
   *
   * @param channel The GIOChannel instance.
   *
   * @return std::shared_ptr<IO_Channel> The shared pointer to the IO channel
   * instance.
   */
  static std::shared_ptr<IO_Channel> instance(GIOChannel *channel);

  IO_Channel(const IO_Channel &channel) = delete;
  IO_Channel(IO_Channel &&channel) = default;
  virtual ~IO_Channel() {}
  IO_Channel &operator=(const IO_Channel &channel) = delete;
  IO_Channel &operator=(IO_Channel &&channel) = default;

  /**
   * @brief Returns the low level channel.
   *
   * @return GIOChannel* The low level channel.
   */
  GIOChannel *get_core() const noexcept;

  /**
   * @brief Reads a line from the registered file descriptor.
   *
   * @return std::string The read line.
   */
  std::string read_line() const;

  /**
   * @brief Registers a watch to process input.
   *
   * @param watch The watch to register.
   *
   * @return unsigned int The ID of the watch.
   *
   * @throw Exception If the watch cannot be registered.
   */
  unsigned int add_watch(std::unique_ptr<IO_Channel_Watch> &&watch);

private:
  /**
   * @brief Register of channels.
   */
  static std::unordered_map<int, std::shared_ptr<IO_Channel>> channels;

  /**
   * @brief Creates the channel depending on the platform.
   * @param file_number The file number for the channel.
   */
  explicit IO_Channel(int file_number);

  /**
   * @brief Assume ownership.
   * @param channel The channel to assume ownership of.
   */
  explicit IO_Channel(GIOChannel *channel);

  /**
   * @brief Low level channel.
   */
  unique_io_channel core;

  /**
   * @brief Callback that processes input.
   */
  std::unique_ptr<IO_Channel_Watch> channel_watch;
};

} // namespace viflow
