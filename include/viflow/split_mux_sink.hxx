#pragma once

#include "viflow/element.hxx"
#include "viflow/split_mux_sink_callback.hxx"

namespace viflow {

/**
 * @brief Namespace for Split_Mux_Sink signals.
 */
namespace Split_Mux_Sink_Signals {

constexpr const char *format_location = "format-location";
constexpr const char *format_location_full = "format-location-full";
constexpr const char *muxer_added = "muxer-added";
constexpr const char *sink_added = "sink-added";

} // namespace Split_Mux_Sink_Signals

/**
 * @brief Namespace for Split_Mux_Sink action signals.
 */
namespace Split_Mux_Sink_Action_Signals {

constexpr const char *split_after = "split-after";
constexpr const char *split_at_running_time = "split-at-running-time";
constexpr const char *split_now = "split-now";

} // namespace Split_Mux_Sink_Action_Signals

/**
 * @brief Namespace for Split_Mux_Sink properties.
 */
namespace Split_Mux_Sink_Props {

constexpr const char *alignment_threshold = "alignment-threshold";
constexpr const char *async_finalize = "async-finalize";
constexpr const char *async_handling = "async-handling";
constexpr const char *location = "location";
constexpr const char *max_files = "max-files";
constexpr const char *max_size_bytes = "max-size-bytes";
constexpr const char *max_size_time = "max-size-time";
constexpr const char *max_size_timecode = "max-size-timecode";
constexpr const char *message_forward = "message-forward";
constexpr const char *mux_overhead = "mux-overhead";
constexpr const char *muxer = "muxer";
constexpr const char *muxer_factory = "muxer-factory";
constexpr const char *muxer_properties = "muxer-properties";
constexpr const char *name = "name";
constexpr const char *parent = "parent";
constexpr const char *reset_muxer = "reset-muxer";
constexpr const char *send_keyframe_requests = "send-keyframe-requests";
constexpr const char *sink = "sink";
constexpr const char *sink_factory = "sink-factory";
constexpr const char *sink_properties = "sink-properties";
constexpr const char *use_robust_muxing = "use-robust-muxing";

} // namespace Split_Mux_Sink_Props

/**
 * @brief Class for handling split mux sink operations.
 */
class Split_Mux_Sink : public Element {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Split_Mux_Sink() {}

  /**
   * @brief Exposes the existing callback methods.
   */
  using Element::connect_callback;

  /**
   * @brief Connects a callback for the format-location signal.
   *
   * @param callback The Format_Location_Callback instance.
   */
  void connect_callback(std::unique_ptr<Format_Location_Callback> &&callback);

  /**
   * @brief Connects a callback for the format-location-full signal.
   *
   * @param callback The Format_Location_Full_Callback instance.
   */
  void
  connect_callback(std::unique_ptr<Format_Location_Full_Callback> &&callback);

  /**
   * @brief Connects a callback for the muxer-added signal.
   *
   * @param callback The Muxer_Added_Callback instance.
   */
  void connect_callback(std::unique_ptr<Muxer_Added_Callback> &&callback);

  /**
   * @brief Connects a callback for the sink-added signal.
   *
   * @param callback The Sink_Added_Callback instance.
   */
  void connect_callback(std::unique_ptr<Sink_Added_Callback> &&callback);

  /**
   * @brief Splits the file now. The current GOP will go to the new file.
   */
  void split_now() const noexcept;

  /**
   * @brief Splits the file now. The current GOP will go to the old file.
   */
  void split_after() const noexcept;

protected:
  /**
   * @brief Creates a named element.
   *
   * @param name The name of the element.
   */
  Split_Mux_Sink(const std::string &name);

  friend class Element;
};

} // namespace viflow
