#pragma once

#include <gio/gio.h>
#include <gst/rtsp/rtsp.h>
#include <gst/sdp/gstsdpmessage.h>

#include "viflow/capabilities.hxx"
#include "viflow/shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {
/**
 * @brief Interface for on-sdp GCallback adapters.
 */
class On_SDP_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~On_SDP_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the client has retrieved
   * the SDP and before it configures the streams in the SDP.
   *
   * @param src The Shared_Element instance.
   * @param message The GstSDPMessage instance.
   */
  virtual void process(const Shared_Element<> &src, GstSDPMessage *message) = 0;
};

/**
 * @brief Interface for select-stream GCallback adapters.
 */
class Select_Stream_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Select_Stream_Callback() {}

  /**
   * @brief Called by the dummy adapter function.
   *
   * @param src The Shared_Element instance.
   * @param stream_num The stream number.
   * @param caps The GstCaps instance.
   *
   * @return bool True if the process is successful, false otherwise.
   */
  virtual bool process(const Shared_Element<> &src, unsigned stream_num,
                       GstCaps *caps) = 0;
};

/**
 * @brief Interface for new-manager GCallback adapters.
 */
class New_Manager_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~New_Manager_Callback() {}

  /**
   * @brief Called by the dummy adapter function after a new manager (like
   * rtpbin) was created and the default properties were configured.
   *
   * @param src The Shared_Element instance.
   * @param manager The Shared_Element instance for the manager.
   */
  virtual void process(const Shared_Element<> &src,
                       const Shared_Element<> &manager) = 0;
};

/**
 * @brief Interface for request-rtcp-key GCallback adapters.
 */
class Request_RTCP_Key_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Request_RTCP_Key_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the crypto parameters
   * relevant to the RTCP stream are needed.
   *
   * @param src The Shared_Element instance.
   * @param stream_num The stream number.
   *
   * @return GstCaps* The GstCaps instance.
   */
  virtual GstCaps *process(const Shared_Element<> &src,
                           unsigned stream_num) = 0;
};

/**
 * @brief Interface for accept-certificate GCallback adapters.
 */
class Accept_Certificate_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Accept_Certificate_Callback() {}

  /**
   * @brief Called by the dummy adapter function during the TLS handshake after
   * the peer certificate has been received.
   *
   * @param src The Shared_Element instance.
   * @param peer_cert The GTlsConnection instance for the peer certificate.
   * @param errors The GTlsCertificate instance for the errors.
   * @param user_data The GTlsCertificateFlags instance for the user data.
   *
   * @return bool True if the process is successful, false otherwise.
   */
  virtual bool process(const Shared_Element<> &src, GTlsConnection *peer_cert,
                       GTlsCertificate *errors,
                       GTlsCertificateFlags *user_data) = 0;
};

/**
 * @brief Interface for before-send GCallback adapters.
 */
class Before_Send_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Before_Send_Callback() {}

  /**
   * @brief Called by the dummy adapter function before each RTSP request is
   * sent, in order to allow the application to modify send parameters or to
   * skip the message entirely.
   *
   * @param src The Shared_Element instance.
   * @param message The GstRTSPMessage instance.
   *
   * @return bool True if the process is successful, false otherwise.
   */
  virtual bool process(const Shared_Element<> &src,
                       GstRTSPMessage *message) = 0;
};

} // namespace viflow
