#pragma once

#include <gst/gstpad.h>

#include "viflow/shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {

/**
 * @brief Interface for eos GCallback adapters.
 */
class EoS_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the EoS_Callback object
   */
  virtual ~EoS_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the eos signal is emitted.
   *
   * @param app_sink The app_sink element that emitted the signal.
   */
  virtual void process(const Shared_Element<> &app_sink) = 0;
};

/**
 * @brief Interface for new-preroll GCallback adapters.
 */
class New_Preroll_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the New_Preroll_Callback object
   */
  virtual ~New_Preroll_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the new-preroll signal is
   *        emitted.
   *
   * @param app_sink The app_sink element that emitted the signal.
   *
   * @return GST_FLOW_OK if the preroll was handled successfully.
   */
  virtual GstFlowReturn process(const Shared_Element<> &app_sink) = 0;
};

/**
 * @brief Interface for new-sample GCallback adapters.
 */
class New_Sample_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the New_Sample_Callback object
   */
  virtual ~New_Sample_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the new-sample signal is
   *        emitted.
   *
   * @param app_sink The app_sink element that emitted the signal.
   *
   * @return GST_FLOW_OK if the sample was handled successfully.
   */
  virtual GstFlowReturn process(const Shared_Element<> &app_sink) = 0;
};

/**
 * @brief Interface for new-serialized-event GCallback adapters.
 */
class New_Serialized_Event_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the New_Serialized_Event_Callback object
   */
  virtual ~New_Serialized_Event_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the new-serialized-event
   *        signal is emitted.
   *
   * @param app_sink The app_sink element that emitted the signal.
   *
   * @return TRUE if the event was handled successfully.
   */
  virtual gboolean process(const Shared_Element<> &app_sink) = 0;
};

} // namespace viflow
