#pragma once

#include <gst/gstpad.h>

#include "viflow/shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {

/**
 * @brief Interface for source-setup GCallback adapters.
 */
class Source_Setup_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Source_Setup_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the source element has
   * been created.
   *
   * @param parent The Shared_Element instance for the parent.
   * @param source The Shared_Element instance for the source.
   */
  virtual void process(const Shared_Element<> &parent,
                       const Shared_Element<> &source) = 0;
};

} // namespace viflow
