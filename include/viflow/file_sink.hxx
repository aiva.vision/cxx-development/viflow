#pragma once

#include "viflow/element.hxx"
#include "viflow/split_mux_sink_callback.hxx"

namespace viflow {

/**
 * @brief This namespace contains properties related to the File Sink.
 */
namespace File_Sink_Props {

constexpr const char *append = "append";
constexpr const char *async = "async";
constexpr const char *blocksize = "blocksize";
constexpr const char *buffer_mode = "buffer-mode";
constexpr const char *buffer_size = "buffer-size";
constexpr const char *enable_last_sample = "enable-last-sample";
constexpr const char *last_sample = "last-sample";
constexpr const char *location = "location";
constexpr const char *max_bitrate = "max-bitrate";
constexpr const char *max_lateness = "max-lateness";
constexpr const char *max_transient_error_timeout =
    "max-transient-error-timeout";
constexpr const char *name = "name";
constexpr const char *o_sync = "o-sync";
constexpr const char *parent = "parent";
constexpr const char *processing_deadline = "processing-deadline";
constexpr const char *qos = "qos";
constexpr const char *render_delay = "render-delay";
constexpr const char *stats = "stats";
constexpr const char *sync = "sync";
constexpr const char *throttle_time = "throttle-time";
constexpr const char *ts_offset = "ts-offset";

} // namespace File_Sink_Props

/**
 * @brief This class represents a file sink element.
 * @details It inherits from the Element class.
 */
class File_Sink : public Element {
public:
  /**
   * @brief Virtual destructor.
   */
  virtual ~File_Sink() {}

protected:
  /**
   * @brief Constructor that creates a named element.
   *
   * @param name The name of the element.
   *
   * @throw std::runtime_error If the element cannot be created.
   */
  File_Sink(const std::string &name);

  /**
   * @brief Friend class declaration for Element.
   */
  friend class Element;
};

} // namespace viflow
