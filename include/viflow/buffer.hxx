#pragma once

#include <gst/gstbuffer.h>

#include "viflow/map_info.hxx"
#include "viflow/unique_ptr.hxx"

namespace viflow {

inline void gst_buffer_unref(GstBuffer *buf) {
  gst_mini_object_unref(GST_MINI_OBJECT_CAST(buf));
}

/**
 * @brief Useful when dealing with low level GstBuffer instances.
 */
using unique_buffer = unique_ptr_fn<GstBuffer, gst_buffer_unref>;

/**
 * @brief Adapter for GstBuffer.
 */
class Buffer {
public:
  /**
   * @brief Increase the reference count and take ownership.
   *
   * @param b The buffer to reference.
   *
   * @return Buffer
   */
  static Buffer ref(GstBuffer *b);

  /**
   * @brief Increase the reference count of a Buffer
   *
   * @param b The Buffer to increase the reference count of
   *
   * @return Buffer The referenced Buffer
   */
  static Buffer ref(const Buffer &b);

  /**
   * @brief Create a Buffer from GstPadProbeInfo.
   *
   * The buffer reference can be increased by passing ref as true.
   *
   * @param info The GstPadProbeInfo to create the Buffer from
   * @param ref Whether to increase the buffer reference. Default is true.
   *
   * @return Buffer The created Buffer
   */
  static Buffer from(GstPadProbeInfo *info, bool ref = true);

  Buffer() = default;
  Buffer(const Buffer &b) = delete;
  Buffer(Buffer &&b) = default;
  virtual ~Buffer() {}
  Buffer &operator=(const Buffer &b) = delete;
  Buffer &operator=(Buffer &&b) = default;

  /**
   * @brief Check if the Buffer is valid
   *
   * @return true if the Buffer is valid, false otherwise
   */
  explicit operator bool() const { return core.get() != nullptr; }

  /**
   * @brief Construct a Buffer and take ownership of a GstBuffer
   *
   * @param buffer GstBuffer to take ownership of
   */
  explicit Buffer(GstBuffer *buffer);

  /**
   * @brief Allocate a Buffer of a certain size
   *
   * @param size Size of the Buffer to allocate
   */
  explicit Buffer(unsigned long size);

  /**
   * @brief Get the reference count of the Buffer
   *
   * @return size_t Reference count
   */
  size_t ref_count() const noexcept {
    return GST_OBJECT_REFCOUNT_VALUE(core.get());
  }

  /**
   * @brief Get the underlying GstBuffer
   *
   * @return GstBuffer* Underlying GstBuffer
   */
  GstBuffer *get_core() const noexcept;

  /**
   * @brief Check if the Buffer is writable
   *
   * @return true if the Buffer is writable, false otherwise
   */
  bool is_writable() const noexcept;

  /**
   * @brief Make the Buffer writable
   */
  void make_writable();

  /**
   * @brief Set the timestamp of the Buffer
   *
   * @param timestamp The timestamp to set
   */
  void set_timestamp(unsigned long timestamp);

  /**
   * @brief Set the duration of the Buffer
   *
   * @param duration The duration to set
   */
  void set_duration(unsigned long duration);

  /**
   * @brief Map the Buffer with specified flags
   *
   * @param flags The flags to use for mapping. Default is GST_MAP_READ.
   *
   * @return Map_Info The mapped information
   */
  Map_Info map(GstMapFlags flags = GST_MAP_READ);

  /**
   * @brief Release the Buffer
   *
   * @return GstBuffer* The released Buffer
   */
  GstBuffer *release() noexcept;

private:
  unique_buffer core;
};

} // namespace viflow
