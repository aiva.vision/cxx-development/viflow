#pragma once

#include <gst/gstpad.h>

namespace viflow {

/**
 * @brief Interface for GstPadProbeCallback adapters.
 */
class Pad_Probe {
public:
  virtual ~Pad_Probe() {}

  /**
   * @brief Called when events in the data flow happen.
   *
   * The low level pad can be converted to a proper viflow::Pad handle with
   * viflow::Pad::convert(). Do NOT release or assume ownership of the
   * arguments.
   *
   * @param pad The GstPad instance.
   * @param info The GstPadProbeInfo instance.
   *
   * @return GstPadProbeReturn The result of the probe.
   */
  virtual GstPadProbeReturn process(GstPad *pad, GstPadProbeInfo *info) = 0;

  /**
   * @brief Returns the pad's type.
   *
   * @return GstPadProbeType The type of the pad.
   */
  virtual GstPadProbeType get_type() const noexcept = 0;
};
} // namespace viflow
