#pragma once

#define VIFLOW_ELEMENT

#include <gst/gstelement.h>
#include <mutex>
#include <string>
#include <unordered_map>

#include "viflow/bus.hxx"
#include "viflow/element_callback.hxx"
#include "viflow/event.hxx"
#include "viflow/pad.hxx"
#include "viflow/state_change.hxx"
#include "viflow/unique_ptr.hxx"
#include "viflow/weak_element.hxx"

namespace viflow {

/**
 * @brief Namespace for Element signal names
 */
namespace Element_Signals {

constexpr const char *no_more_pads = "no-more-pads";
constexpr const char *pad_added = "pad-added";
constexpr const char *pad_removed = "pad-removed";

} // namespace Element_Signals
/**
 * @brief Useful when dealing with low level GstElement instances.
 */
using unique_element = unique_ptr_fn<GstElement, gst_object_unref>;

/**
 * @brief Adapter for GstElement. Uses the singleton pattern to guarantee that
 * instances are unique based on their name. Without keeping track of the
 * created elements, all the data being kept by the Element wrapper could be
 * lost.
 */
class Element {
public:
  /**
   * @brief Checks whether the element has been registered.
   *
   * @param name The name of the element to check.
   *
   * @return bool True if the element is registered, false otherwise.
   */
  static bool is_registered(const std::string &name);

  /**
   * @brief Checks whether the element has been registered.
   *
   * @param elem The element to check.
   *
   * @return bool True if the element is registered, false otherwise.
   */
  static bool is_registered(GstElement *elem);

  /**
   * @brief Creates an instance using the given factory and registers it.
   *
   * @param factory The factory to use for creating the instance.
   * @param name The name of the instance.
   * @return Shared_Element<> A pointer to the registered element.
   *
   * @throws Element_Already_Registered
   */
  static Shared_Element<> register_instance(const std::string &factory,
                                            const std::string &name);

  /**
   * @brief Creates an instance using the given type and registers it.
   *
   * @tparam E The type of the element.
   * @tparam Args The types of the arguments.
   *
   * @param args The arguments to pass to the constructor.
   *
   * @return Shared_Element<E> A pointer to the registered element.
   *
   * @throws Element_Already_Registered
   */
  template <class E = Element, typename... Args>
  static Shared_Element<E> register_instance(Args &&...args);

  /**
   * @brief Unregisters the given element.
   * If the element is already added to a bin, it has to be manually removed.
   *
   * @param name The name of the element to unregister.
   */
  static void unregister_instance(const std::string &name);

  /**
   * @brief Retrieves the named instance from the registry and casts it.
   *
   * @tparam E The type of the element.
   *
   * @param name The name of the instance.
   *
   * @return Shared_Element<E> The instance.
   */
  template <class E = Element>
  static Shared_Element<E> instance(const std::string &name);

  /**
   * @brief Retrieves the instance corresponding to the object from the
   * registry.
   *
   * @tparam E The type of the element.
   * @param elem The element to convert.
   *
   * @return Shared_Element<E> The instance.
   */
  template <class E = Element>
  static Shared_Element<E> convert(GstElement *elem);

  Element(const Element &element) = delete;
  Element(Element &&element) = default;
  Element &operator=(const Element &element) = delete;
  Element &operator=(Element &&element) = default;
  /**
   * @brief The registered pads are manually released before the core element
   * itself. This is needed by the dynamic pads.
   */
  virtual ~Element();

  /**
   * @brief Retrieves the element's property.
   *
   * @tparam T The type of the property.
   *
   * @param name The name of the property.
   *
   * @return T The property value.
   */
  template <typename T> T get_property(const std::string &name) const noexcept;

  /**
   * @brief Set property on element.
   *
   * @tparam T The type of the property.
   *
   * @param name The name of the property.
   * @param value The value to set.
   */
  template <typename T>
  void set_property(const std::string &name, const T value) noexcept;

  /**
   * @brief Check if the element can be casted.
   *
   * @tparam T The type to cast to.
   *
   * @return bool True if the element can be casted, false otherwise.
   */
  template <class T> bool can_cast();

  /**
   * @brief Casts the element to the requested type.
   *
   * @tparam T The type to cast to.
   *
   * @return T* The casted element.
   *
   * @throws Bad_Element_Cast if the element cannot be casted.
   */
  template <class T> T *cast();

  /**
   * @brief Retrieves the element's state.
   *
   * @return State_Change The current state of the element.
   */
  State_Change get_state() const noexcept;

  /**
   * @brief Changes the state of the element.
   *
   * @param state The state to set.
   *
   * @return GstStateChangeReturn The result of the state change.
   */
  GstStateChangeReturn set_state(GstState state) noexcept;

  /**
   * @brief Tries to change the state of the element to the same as its parent.
   *
   * @return bool True if the state change was successful, false otherwise.
   */
  bool sync_state_with_parent() noexcept;

  /**
   * @brief Set the state to GST_STATE_PLAYING.
   *
   * @return GstStateChangeReturn The result of the state change.
   */
  GstStateChangeReturn play();

  /**
   * @brief Set the state to GST_STATE_PAUSED.
   *
   * @return GstStateChangeReturn The result of the state change.
   */
  GstStateChangeReturn pause();

  /**
   * @brief Set the state to GST_STATE_NULL.
   *
   * @return GstStateChangeReturn The result of the state change.
   */
  virtual GstStateChangeReturn stop();

  /**
   * @brief Retrieve pad from the element.
   *
   * First it checks whether the pad's name contains a %. If it does,
   * then a new dynamic pad is requested. If it doesn't, the internal register
   * is queried. If no match is found, a static pad is requested. If that
   * fails too, a dynamic pad is requested. Finally, the new pad is added to the
   * internal register or the method throws an exception.
   *
   * @param name The name of the pad.
   *
   * @return Pad& The requested pad.
   *
   * @throws Pad_Request_Error
   */
  Pad &get_pad(const std::string &name);

  /**
   * @brief Adds the pad to the internal register.
   *
   * @param pad The pad to register.
   * @param dynamic Whether the pad is dynamic.
   *
   * @return Pad& The registered pad.
   *
   * @throws Pad_Already_Registered
   */
  Pad &register_pad(GstPad *pad, bool dynamic = false);

  /**
   * @brief Adds the pad to the internal register.
   *
   * @param pad The pad to register.
   *
   * @return Pad& The registered pad.
   *
   * @throws Pad_Already_Registered
   */
  Pad &register_pad(Pad &&pad);

  /**
   * @brief Unregisters a pad from the element.
   *
   * @param name The name of the pad to unregister.
   */
  void unregister_pad(const std::string &name);

  /**
   * @brief Adds a pad to the element and if successful registers it.
   *
   * @param pad The pad to add.
   *
   * @return bool True if the pad was added successfully, false otherwise.
   */
  bool add_pad(Pad &&pad);

  /**
   * @brief Returns a pointer to the lower level element.
   *
   * @return GstElement* The lower level element.
   */
  GstElement *get_core() const noexcept;

  /**
   * @brief Returns the name used when creating the element.
   *
   * @return std::string The name of the element.
   */
  std::string get_name() const noexcept;

  /**
   * @brief Checks if the element has a parent.
   *
   * @return bool True if the element has a parent, false otherwise.
   */
  bool has_parent() const;

  /**
   * @brief Returns the parent of the element.
   *
   * @tparam E The type of the parent element.
   *
   * @return Shared_Element<E> The parent element.
   *
   * @throws viflow::Exception
   */
  template <class E = Element> Shared_Element<E> get_parent() const;

  /**
   * @brief Returns the element's bus.
   *
   * @return std::unique_ptr<Bus>& The bus of the element.
   */
  std::unique_ptr<Bus> &get_bus() noexcept;

  /**
   * @brief Returns a value between 0 and the stream duration (if the stream
   *        duration is known).
   *
   * @param format The format of the position.
   *
   * @return long The position of the stream.
   */
  long query_position(GstFormat format);
  /**
   * @brief Returns the total duration of the stream in nanoseconds.
   *
   * @param format The format of the duration.
   *
   * @return long The duration of the stream.
   */
  long query_duration(GstFormat format);

  /**
   * @brief Performs a seek on the element.
   *
   * @param rate The rate of the seek.
   * @param format The format of the seek.
   * @param flags The flags for the seek.
   * @param start_type The type of the start of the seek.
   * @param start The start of the seek.
   * @param stop_type The type of the stop of the seek.
   * @param stop The stop of the seek.
   *
   * @return bool True if the seek was successful, false otherwise.
   */
  bool seek(double rate, GstFormat format, GstSeekFlags flags,
            GstSeekType start_type, long start, GstSeekType stop_type,
            long stop);

  /**
   * @brief Simple API to perform a seek on the element.
   *
   * @param format The format of the seek.
   * @param flags The flags for the seek.
   * @param position The position of the seek.
   *
   * @return bool True if the seek was successful, false otherwise.
   */
  bool seek_simple(GstFormat format, GstSeekFlags flags, long position);

  /**
   * @brief Send an event to this element.
   *
   * @param event The event to send.
   *
   * @return bool True if the event was sent successfully, false otherwise.
   */
  bool send_event(GstEvent *event) noexcept;
  bool send_event(Event &&event) noexcept;

  /**
   * @brief Post message on this element's bus.
   *
   * @param message The message to post.
   *
   * @return bool True if the message was posted successfully, false otherwise.
   */
  bool post_message(GstMessage *message) noexcept;
  bool post_message(Message &&message) noexcept;

  /**
   * @brief Link this element to the parameter.
   *
   * @param elem The element to link to.
   *
   * @return Shared_Element<> The linked element.
   *
   * @throws Element_Link_Error
   */
  virtual Shared_Element<> link(const Shared_Element<> &elem);

  /**
   * @brief Link this element's source pad to the parameter's sink pad.
   *
   * This may create request pads, but they will be managed by the element.
   *
   * @param elem The element to link to.
   * @param src_pad_name The name of the source pad.
   * @param sink_pad_name The name of the sink pad.
   *
   * @return Shared_Element<> The linked element.
   *
   * @throws Pad_Link_Error
   */
  virtual Shared_Element<> link(const Shared_Element<> &elem,
                                const std::string &src_pad_name,
                                const std::string &sink_pad_name);

  /**
   * @brief Unlink this element from the parameter.
   *
   * @param elem The element to unlink from.
   */
  virtual void unlink(GstElement *elem);

  /**
   * @brief Unlink this element from the parameter.
   *
   * @param elem The element to unlink from.
   */
  virtual void unlink(const Shared_Element<> &elem);

  /**
   * @brief Checks if the element is connected.
   *
   * @param name The name of the element to check.
   *
   * @return bool True if the element is connected, false otherwise.
   */
  bool is_connected(const std::string &name) const noexcept;

  /**
   * @brief Connects a callback for the no-more-pads signal.
   *
   * @param callback The callback to connect.
   */
  void connect_callback(std::unique_ptr<No_More_Pads_Callback> &&callback);

  /**
   * @brief Connects a callback for the pad-added signal.
   *
   * @param callback The callback to connect.
   */
  void connect_callback(std::unique_ptr<Pad_Added_Callback> &&callback);

  /**
   * @brief Connects a callback for the pad-removed signal.
   *
   * @param callback The callback to connect.
   */
  void connect_callback(std::unique_ptr<Pad_Removed_Callback> &&callback);

  /**
   * @brief Disconnects a callback.
   *
   * @param name The name of the callback to disconnect.
   */
  void disconnect_callback(const std::string &name);

  /**
   * @brief Unregisters this element from the registry.
   *
   * After this call, the element is not safe to use anymore. After this call,
   * the caller should hold the unique reference for this instance.
   */
  virtual void unregister();

protected:
  /**
   * @brief Low level representation of the element.
   */
  unique_element core;

  /**
   * @brief Useful for elements that involve a complex construction process.
   */
  Element() = default;

  /**
   * @brief Assumes ownership for the element.
   *
   * @param elem The element to assume ownership of.
   */
  explicit Element(GstElement *elem);

  /**
   * @brief Creates the low level element with gst_element_factory_make() and
   * increases it's reference count.
   *
   * @param factory The factory to use for creating the element.
   * @param name The name of the element.
   *
   * @throws Factory_Error
   */
  explicit Element(const std::string &factory, const std::string &name);

  /**
   * @brief Connects a callback for a signal.
   *
   * @tparam F The type of the function.
   *
   * @param signal The signal to connect the callback to.
   * @param adapter The adapter for the callback.
   * @param callback The callback to connect.
   */
  template <typename F, typename = std::enable_if_t<
                            std::is_function_v<std::remove_pointer_t<F>>>>
  void connect_callback(const std::string &signal, F adapter,
                        std::unique_ptr<Signal_Callback> &&callback);
  /**
   * @brief Register of Element instances.
   */
  static std::unordered_map<std::string, Shared_Element<>> instances_register;

  /**
   * @brief Mutex for writing to the registry.
   */
  static std::mutex register_mutex;

  /**
   * @brief Map of signal callbacks that keeps track of both the handler and the
   * source id from g_signal_connect.
   */
  using Callback_Map =
      std::unordered_map<std::string,
                         std::pair<gulong, std::unique_ptr<Signal_Callback>>>;

  /**
   * @brief Callbacks registered for this element.
   */
  Callback_Map registered_callbacks;

  /**
   * @brief Register of pads.
   */
  std::unordered_map<std::string, Pad> pads;

  /**
   * @brief The element's bus.
   */
  std::unique_ptr<Bus> bus;
};

} // namespace viflow

#include "viflow/element.txx"
