#pragma once

#include "viflow/features.hxx"

namespace viflow {

/**
 * @brief Unreference a GstCaps instance.
 *
 * @param caps The GstCaps instance to unreference.
 */
inline void gst_caps_unref(GstCaps *caps) {
  gst_mini_object_unref(GST_MINI_OBJECT_CAST(caps));
}

/**
 * @brief Useful when dealing with low level GstCaps instances.
 */
using unique_caps = unique_ptr_fn<GstCaps, gst_caps_unref>;

/**
 * @brief Namespace for Caps Fields
 */
namespace Caps_Fields {

constexpr const char *format = "format";
constexpr const char *width = "width";
constexpr const char *height = "height";
constexpr const char *framerate = "framerate";
constexpr const char *fps_nominator = "fps-nominator";
constexpr const char *fps_denominator = "fps-denominator";

} // namespace Caps_Fields

/**
 * @brief Adapter for GstCaps.
 */
class Capabilities {
public:
  /**
   * @brief Create Capabilities from a string.
   *
   * @param caps The string representation of the capabilities.
   *
   * @return Capabilities
   */
  static Capabilities from_string(const std::string &caps);

  /**
   * @brief Increase the reference count and take ownership.
   *
   * @param c The GstCaps to reference.
   *
   * @return Capabilities
   */
  static Capabilities ref(const GstCaps *c);

  /**
   * @brief Increase the reference count and take ownership.
   *
   * @param c The Capabilities to reference.
   *
   * @return Capabilities
   */
  static Capabilities ref(const Capabilities &c);

  /**
   * @brief Copy capabilities and take ownership.
   *
   * @param c The GstCaps to copy.
   *
   * @return Capabilities
   */
  static Capabilities copy(const GstCaps *c);

  /**
   * @brief Copy capabilities and take ownership.
   *
   * @param c The Capabilities to copy.
   *
   * @return Capabilities
   */
  static Capabilities copy(const Capabilities &c);

  /**
   * @brief Intersect two GstCaps.
   *
   * @param caps_1 The first GstCaps.
   * @param caps_2 The second GstCaps.
   * @param mode The intersection mode.
   *
   * @return Capabilities
   */
  static Capabilities
  intersect(GstCaps *caps_1, GstCaps *caps_2,
            GstCapsIntersectMode mode = GST_CAPS_INTERSECT_ZIG_ZAG);

  /**
   * @brief Intersect two Capabilities.
   *
   * @param caps_1 The first Capabilities.
   * @param caps_2 The second Capabilities.
   * @param mode The intersection mode.
   */
  static Capabilities
  intersect(const Capabilities &caps_1, const Capabilities &caps_2,
            GstCapsIntersectMode mode = GST_CAPS_INTERSECT_ZIG_ZAG);

  /**
   * @brief Check if two GstCaps can intersect.
   *
   * @param caps_1 The first GstCaps.
   * @param caps_2 The second GstCaps.
   *
   * @return bool True if the GstCaps can intersect, false otherwise.
   */
  static bool can_intersect(GstCaps *caps_1, GstCaps *caps_2);

  /**
   * @brief Check if two Capabilities can intersect.
   *
   * @param caps_1 The first Capabilities.
   * @param caps_2 The second Capabilities.
   *
   * @return bool True if the Capabilities can intersect, false otherwise.
   */
  static bool can_intersect(const Capabilities &caps_1,
                            const Capabilities &caps_2);

  /**
   * @brief Convert GstCaps to a string.
   *
   * @param caps The GstCaps to convert.
   *
   * @return std::string The string representation of the GstCaps.
   */
  static std::string to_string(const GstCaps *caps);

  Capabilities(const Capabilities &c);
  Capabilities(Capabilities &&c) = default;
  virtual ~Capabilities() {}
  Capabilities &operator=(const Capabilities &c);
  Capabilities &operator=(Capabilities &&c) = default;
  /**
   * @brief Create ANY caps.
   */
  Capabilities();

  /**
   * @brief Assumes ownership of the capabilities.
   *
   * @param caps The capabilities to take ownership of.
   */
  explicit Capabilities(GstCaps *caps);

  /**
   * @brief Create simple empty caps.
   *
   * @param media_type The media type for the caps.
   */
  explicit Capabilities(const std::string &media_type);

  /**
   * @brief Build capabilities for media type and format.
   *
   * @param media_type The media type for the caps.
   * @param format The format for the caps.
   * @param features The features for the caps.
   */
  explicit Capabilities(const std::string &media_type,
                        const std::string &format,
                        const std::string &features = {});

  /**
   * @brief Determines if caps represents any media format.
   *
   * @return bool True if the caps represents any media format, false otherwise.
   */
  bool is_any() const noexcept;

  /**
   * @brief Determines if caps are fixed.
   *
   * @return bool True if the caps are fixed, false otherwise.
   */
  bool is_fixed() const noexcept;

  /**
   * @brief Determines if caps represents no media format.
   *
   * @return bool True if the caps represents no media format, false otherwise.
   */
  bool is_empty() const noexcept;

  /**
   * @brief Returns the count of structures contained.
   *
   * @return unsigned int The count of structures contained in the caps.
   */
  unsigned int get_size() const noexcept;

  /**
   * @brief Return the low level caps.
   *
   * @return GstCaps* The low level caps.
   */
  GstCaps *get_core() const noexcept;

  /**
   * @brief Release the ownership of the caps.
   *
   * @return GstCaps* The caps that were released.
   */
  GstCaps *release() noexcept;
  /**
   * @brief Returns the caps's GstStructure.
   *
   * @param index The index of the structure to get.
   *
   * @return GstStructure* The GstStructure at the given index.
   */
  GstStructure *get_structure(unsigned int index) const noexcept;

  /**
   * @brief Returns the features corresponding to this index.
   *
   * @param index The index of the features to get.
   *
   * @return GstCapsFeatures* The GstCapsFeatures at the given index.
   */
  GstCapsFeatures *get_features(unsigned int index) const noexcept;

  /**
   * @brief Set the features for these capabilities. Takes ownership of the
   * features.
   *
   * @param index The index at which to set the features.
   * @param features The features to set.
   */
  void set_features(unsigned int index, Features &&features);

  /**
   * @brief Set a field in the capabilities.
   *
   * @tparam Args The types of the arguments.
   * @param field The field to set.
   * @param args The values to set the field to.
   */
  template <typename... Args>
  void set(const std::string &field, Args &&...args) {
    gst_caps_set_simple(core.get(), field.c_str(), std::forward<Args>(args)...,
                        nullptr);
  }

  /**
   * @brief Converts the Capabilities to a string.
   *
   * @return std::string The string representation of the Capabilities.
   */
  std::string to_string() const;

  /**
   * @brief Simplify the Capabilities.
   */
  void simplify();

  /**
   * @brief Make the Capabilities writable.
   */
  void make_writable();

private:
  unique_caps core;
};

}; // namespace viflow
