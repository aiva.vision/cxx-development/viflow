#pragma once

#include <gst/gstcaps.h>

#include "viflow/unique_ptr.hxx"

namespace viflow {
/**
 * @brief Unique pointer for GstCapsFeatures with a custom deleter.
 */
using unique_caps_features =
    unique_ptr_fn<GstCapsFeatures, gst_caps_features_free>;

/**
 * @brief Adapter for GstCapsFeatures.
 */
class Features {
public:
  Features(const Features &features) = delete;
  Features(Features &&features) = default;
  virtual ~Features() {}
  Features &operator=(const Features &features) = delete;
  Features &operator=(Features &&features) = default;

  /**
   * @brief Constructor that initializes the features from a text value.
   *
   * @param feature The text value to initialize the features from.
   */
  explicit Features(const std::string &feature);

  /**
   * @brief Constructor that assumes ownership of the capabilities.
   *
   * @param features The capabilities to assume ownership of.
   */
  explicit Features(GstCapsFeatures *features);

  /**
   * @brief Checks if it contains the feature.
   *
   * @param feature The feature to check for.
   *
   * @return bool True if it contains the feature, false otherwise.
   */
  bool contains(const std::string &feature) const noexcept;

  /**
   * @brief Returns the low level caps.
   *
   * @return GstCapsFeatures* The low level caps.
   */
  GstCapsFeatures *get_core() const noexcept;

  /**
   * @brief Releases the ownership of the low level features.
   *
   * @return GstCapsFeatures* The low level features.
   */
  GstCapsFeatures *release() noexcept;

private:
  unique_caps_features core;
};

}; // namespace viflow
