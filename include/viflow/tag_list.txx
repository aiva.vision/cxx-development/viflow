#pragma once

#include <gst/gsttaglist.h>
#include <vector>

#ifndef VIFLOW_TAG_LIST
#include "viflow/tag_list.hxx"
#endif // VIFLOW_TAG_LIST

#include "viflow/exceptions.hxx"
#include "viflow/unique_resources.hxx"

template <typename T> T viflow::Tag_List::get(const std::string &tag) const {
  throw Exception("Tag_List::get<T>(): unsupported type");
}

template <>
inline std::string
viflow::Tag_List::get<std::string>(const std::string &tag) const {

  unique_g_str value;
  if (gst_tag_list_get_string(core.get(), tag.c_str(),
                              value.get_address_of())) {
    return value.get();
  }
  throw Exception("Tag_List::get<std::string>(): tag not found: " + tag);
}

template <>
inline uint viflow::Tag_List::get<uint>(const std::string &tag) const {

  uint value = 0;
  if (gst_tag_list_get_uint(core.get(), tag.c_str(), &value)) {
    return value;
  }
  throw Exception("Tag_List::get<uint>(): tag not found: " + tag);
}

template <>
inline uint64_t viflow::Tag_List::get<uint64_t>(const std::string &tag) const {

  uint64_t value = 0;
  if (gst_tag_list_get_uint64(core.get(), tag.c_str(), &value)) {
    return value;
  }
  throw Exception("Tag_List::get<uint64_t>(): tag not found: " + tag);
}

template <>
inline int viflow::Tag_List::get<int>(const std::string &tag) const {

  int value = 0;
  if (gst_tag_list_get_int(core.get(), tag.c_str(), &value)) {
    return value;
  }
  throw Exception("Tag_List::get<int>(): tag not found: " + tag);
}

template <>
inline int64_t viflow::Tag_List::get<int64_t>(const std::string &tag) const {

  int64_t value = 0;
  if (gst_tag_list_get_int64(core.get(), tag.c_str(), &value)) {
    return value;
  }
  throw Exception("Tag_List::get<int64_t>(): tag not found: " + tag);
}

template <>
inline float viflow::Tag_List::get<float>(const std::string &tag) const {

  float value = 0;
  if (gst_tag_list_get_float(core.get(), tag.c_str(), &value)) {
    return value;
  }
  throw Exception("Tag_List::get<float>(): tag not found: " + tag);
}

template <>
inline double viflow::Tag_List::get<double>(const std::string &tag) const {

  double value = 0;
  if (gst_tag_list_get_double(core.get(), tag.c_str(), &value)) {
    return value;
  }
  throw Exception("Tag_List::get<double>(): tag not found: " + tag);
}

template <>
inline bool viflow::Tag_List::get<bool>(const std::string &tag) const {

  gboolean value = false;
  if (gst_tag_list_get_boolean(core.get(), tag.c_str(), &value)) {
    return value;
  }
  throw Exception("Tag_List::get<bool>(): tag not found: " + tag);
}
