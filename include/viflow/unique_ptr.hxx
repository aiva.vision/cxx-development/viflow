#pragma once

#include <gst/gstobject.h>
#include <memory>

namespace viflow {

/**
 * @brief A unique pointer class.
 *
 * This class replaces the std::unique_ptr class.
 * It gives dorect access to the managed pointer.
 *
 * @tparam T The type of the managed pointer.
 * @tparam D The type of the deleter.
 */
template <typename T, typename D> class unique_ptr {
public:
  using pointer = T *;
  using element_type = T;
  using deleter_type = D;

  unique_ptr() = default;

  /**
   * @brief Constructor for a unique pointer.
   *
   * @param p The pointer to be managed.
   */
  explicit unique_ptr(pointer p) : ptr{p}, deleter{} {}

  /**
   * @brief Constructor for a unique pointer.
   *
   * @param p The pointer to be managed.
   * @param d The deleter to be used.
   */
  explicit unique_ptr(pointer p, const deleter_type &d) : ptr{p}, deleter{d} {}

  /**
   * @brief Constructor for a unique pointer.
   *
   * @param p The pointer to be managed.
   * @param d The deleter to be used.
   */
  explicit unique_ptr(pointer p, deleter_type &&d)
      : ptr{p}, deleter{std::move(d)} {}

  ~unique_ptr() {
    if (ptr) {
      deleter(ptr);
    }
  }

  unique_ptr(const unique_ptr &) = delete;
  unique_ptr &operator=(const unique_ptr &) = delete;

  unique_ptr(unique_ptr &&other)
      : ptr{other.ptr}, deleter{std::move(other.deleter)} {
    other.ptr = nullptr;
  }

  unique_ptr &operator=(unique_ptr &&other) {

    if (this != &other) {
      if (ptr) {
        deleter(ptr);
        ptr = nullptr;
      }
      using std::swap;
      swap(ptr, other.ptr);
      deleter = std::move(other.deleter);
    }
    return *this;
  }

  /**
   * @brief Get the managed pointer.
   *
   * @return pointer The managed pointer.
   */
  pointer get() const { return ptr; }

  /**
   * @brief Get the address of the managed pointer.
   *
   * @return pointer* The address of the managed pointer.
   */
  pointer *get_address_of() { return &ptr; }

  /**
   * @brief Release the managed pointer.
   *
   * @return pointer The managed pointer.
   */
  pointer release() {
    pointer p = ptr;
    ptr = nullptr;
    return p;
  }

  /**
   * @brief Reset the managed pointer.
   *
   * @param p The new pointer to be managed.
   */
  void reset(pointer p = pointer{}) {
    if (ptr != p) {
      if (ptr) {
        deleter(ptr);
      }
      ptr = p;
    }
  }

  /**
   * @brief Swap the unique pointer with another unique pointer.
   *
   * @param other The other unique pointer.
   */
  void swap(unique_ptr &other) {
    using std::swap;
    swap(ptr, other.ptr);
    swap(deleter, other.deleter);
  }

  /**
   * @brief Check if the unique pointer is empty.
   *
   * @return true If the unique pointer is empty.
   */
  bool empty() const { return ptr == nullptr; }

  /**
   * @brief Check if the unique pointer is empty.
   *
   * @return true If the unique pointer is empty.
   */
  explicit operator bool() const { return ptr != nullptr; }

  T &operator*() const { return *ptr; }

  pointer operator->() const { return ptr; }

  friend void swap(unique_ptr &lhs, unique_ptr &rhs) { lhs.swap(rhs); }

private:
  pointer ptr{nullptr};
  deleter_type deleter;
};

/**
 * @struct deleter_from_fn
 * @brief Structure for creating a deleter from a function.
 *
 * @tparam fn The function to be used for deletion.
 */
template <auto fn> struct deleter_from_fn {
  /**
   * @brief Operator overload to delete a given object.
   *
   * @tparam T The type of the object.
   *
   * @param arg The object to be deleted.
   */
  template <typename T> constexpr void operator()(T *arg) const { fn(arg); }
};

/**
 * @brief Create a unique pointer managed by a given function.
 *
 * Useful for pointers allocated by C libraries.
 *
 * @tparam T The type of the managed pointer.
 * @tparam fn The function to be used for deletion.
 */
template <typename T, auto fn>
using unique_ptr_fn = unique_ptr<T, deleter_from_fn<fn>>;

/**
 * @brief Unique pointer for resources allocated with malloc, calloc, realloc.
 *
 * @tparam T Type of the resource.
 */
template <typename T> using unique_c_ptr = unique_ptr_fn<T, free>;

/**
 * @brief Unique pointer for resources allocated with
 *        g_malloc, g_malloc0, g_realloc etc.
 *
 * @tparam T Type of the resource.
 */
template <typename T> using unique_g_ptr = unique_ptr_fn<T, g_free>;

/**
 * @brief Unique pointer for GstObject resources.
 *
 * @tparam T Type of the resource.
 */
template <typename T> using unique_gst_ptr = unique_ptr_fn<T, gst_object_unref>;

} // namespace viflow
