#pragma once

#include <gst/gstbin.h>
#ifndef VIFLOW_BIN
#include "viflow/bin.hxx"
#endif // VIFLOW_BIN

#include "viflow/exceptions.hxx"

template <class E>
viflow::Shared_Element<E> viflow::Bin::child(const std::string &name) {

  auto base_instance = child<Element>(name);
  auto derived_instance = std::dynamic_pointer_cast<E>(base_instance);

  if (!derived_instance) {
    throw Bad_Element_Cast(base_instance->get_name());
  }
  return derived_instance;
}

template <>
inline viflow::Shared_Element<viflow::Element>
viflow::Bin::child<viflow::Element>(const std::string &name) {

  std::unique_lock<std::mutex> lck{children_mtx};

  if (!contains(name)) {
    /*
       Maybe the pipeline has been created with parse_launch().
       */
    unique_element element{
        gst_bin_get_by_name(GST_BIN(core.get()), name.c_str())};
    /*
       If so, register the element for future use.
       */
    auto registered_elem = Element::register_instance(element.get());
    children.insert_or_assign(name, registered_elem);
  }
  if (auto elem = children.at(name).lock()) {
    return elem;
  }
  throw Exception{"Failed to retrieve child " + name + " for bin " +
                  get_name()};
}

template <class E>
viflow::Shared_Element<E> viflow::Bin::add(const Shared_Element<E> &elem) {

  std::unique_lock<std::mutex> lck{children_mtx};
  /*
     Add the element to the low level bin.
     */
  bool ret = gst_bin_add(GST_BIN(core.get()), elem->get_core());

  if (ret) {
    /*
       Register the element in the internal map.
       */
    children.insert_or_assign(elem->get_name(), elem);
    /*
       Update the last element.
       */
    last_added_child = elem;

    return elem;
  }
  throw Element_Add_Error{get_name(), elem->get_name()};
}

template <class E, typename... Args>
viflow::Shared_Element<E> viflow::Bin::add(Args &&...args) {

  return add(Element::register_instance<E>(std::forward<Args>(args)...));
}

template <class E>
viflow::Shared_Element<E> viflow::Bin::append(const Shared_Element<E> &elem) {

  auto last = last_added();
  try {
    add(elem);

  } catch (const Element_Add_Error &e) {
    remove(elem->get_name());
    throw e;
  }
  last->link(elem);
  return elem;
}

template <class E, typename... Args>
viflow::Shared_Element<E> viflow::Bin::append(Args &&...args) {

  return append(Element::register_instance<E>(std::forward<Args>(args)...));
}
