#pragma once

#include <exception>
#include <gst/gstmessage.h>
#include <gst/gstpad.h>
#include <string>

namespace viflow {

/**
 * @brief Base class for all exceptions in this library.
 */
class Exception : public std::exception {
public:
  /**
   * @brief Constructor that initializes the exception with a message.
   *
   * @param m The message for the exception.
   */
  Exception(const std::string &m);

  /**
   * @brief Destructor.
   */
  virtual ~Exception() {}

  /**
   * @brief Returns the message of the exception.
   *
   * @return const char* The message of the exception.
   */
  const char *what() const noexcept;

private:
  /**
   * @brief The message of the exception.
   */
  std::string message;
};

/**
 * @brief Exception thrown when there is an error with a factory.
 */
class Factory_Error : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a factory and an
   * element.
   *
   * @param factory The factory that caused the error.
   * @param element The element that caused the error.
   */
  Factory_Error(const std::string &factory, const std::string &element);

  /**
   * @brief Constructor that initializes the exception with a factory, an
   * element, and a message.
   *
   * @param factory The factory that caused the error.
   * @param element The element that caused the error.
   * @param message The message for the exception.
   */
  Factory_Error(const std::string &factory, const std::string &element,
                const std::string &message);
};

/**
 * @brief Exception thrown when a factory is not found.
 */
class Factory_Not_Found : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a factory.
   *
   * @param factory The factory that was not found.
   */
  Factory_Not_Found(const std::string &factory);
};

/**
 * @brief Exception thrown when there is an error parsing a launch command.
 */
class Parse_Launch_Error : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a message.
   *
   * @param message The message for the exception.
   */
  Parse_Launch_Error(const std::string &message);
};

/**
 * @brief Exception thrown when an element is not registered.
 */
class Element_Not_Registered : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with an element.
   *
   * @param element The element that is not registered.
   */
  Element_Not_Registered(const std::string &element);
};

/**
 * @brief Exception thrown when an element is already registered.
 */
class Element_Already_Registered : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with an element.
   *
   * @param element The element that is already registered.
   */
  Element_Already_Registered(const std::string &element);
};

/**
 * @brief Exception thrown when an element is not added to a bin.
 */
class Element_Not_Added : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a bin and an
   * element.
   *
   * @param bin The bin that the element was not added to.
   * @param element The element that was not added.
   */
  Element_Not_Added(const std::string &bin, const std::string &element);
};

/**
 * @brief Exception thrown when there is an error adding an element to a bin.
 */
class Element_Add_Error : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a bin and an
   * element.
   *
   * @param bin The bin that the element was not added to.
   * @param element The element that was not added.
   */
  Element_Add_Error(const std::string &bin, const std::string &element);
};

/**
 * @brief Exception thrown when there is an error linking two elements.
 */
class Element_Link_Error : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with two elements.
   *
   * @param elem1 The first element that could not be linked.
   * @param elem2 The second element that could not be linked.
   */
  Element_Link_Error(const std::string &elem1, const std::string &elem2);
};

/**
 * @brief Exception thrown when there is an error casting an element.
 */
class Bad_Element_Cast : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with an element.
   *
   * @param elem The element that could not be cast.
   */
  Bad_Element_Cast(const std::string &elem);
};

/**
 * @brief Exception thrown when there is an error changing the state of an
 * element.
 */
class State_Change_Error : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with an element and a
   * state.
   *
   * @param elem The element that could not change state.
   * @param state The state that the element could not change to.
   */
  State_Change_Error(const std::string &elem, const std::string &state);
};

/**
 * @brief Exception thrown when a pad is already registered to an element.
 */
class Pad_Already_Registered : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a pad and an
   * element.
   *
   * @param pad The pad that is already registered.
   * @param element The element that the pad is registered to.
   */
  Pad_Already_Registered(const std::string &pad, const std::string &element);
};

/**
 * @brief Exception thrown when there is an error requesting a pad.
 */
class Pad_Request_Error : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a pad.
   *
   * @param pad The pad that could not be requested.
   */
  Pad_Request_Error(const std::string &pad);
};

/**
 * @brief Exception thrown when there is an error linking two pads.
 */
class Pad_Link_Error : public Exception {
public:
  /**
   * @brief Converts a GstPadLinkReturn to a string.
   *
   * @param ret The GstPadLinkReturn to convert.
   *
   * @return std::string The converted string.
   */
  static std::string to_string(GstPadLinkReturn ret) noexcept;

  /**
   * @brief Constructor that initializes the exception with two pads and a
   * GstPadLinkReturn.
   *
   * @param pad1 The first pad that could not be linked.
   * @param pad2 The second pad that could not be linked.
   * @param ret The GstPadLinkReturn for the failed link.
   */
  Pad_Link_Error(const std::string &pad1, const std::string &pad2,
                 GstPadLinkReturn ret);

  /**
   * @brief Constructor that initializes the exception with two pads, their
   * caps, and a GstPadLinkReturn.
   *
   * @param pad1 The first pad that could not be linked.
   * @param caps1 The caps of the first pad.
   * @param pad2 The second pad that could not be linked.
   * @param caps2 The caps of the second pad.
   * @param ret The GstPadLinkReturn for the failed link.
   */
  Pad_Link_Error(const std::string &pad1, const std::string &caps1,
                 const std::string &pad2, const std::string &caps2,
                 GstPadLinkReturn ret);

  /**
   * @brief Returns the GstPadLinkReturn for the failed link.
   *
   * @return GstPadLinkReturn The GstPadLinkReturn for the failed link.
   */
  GstPadLinkReturn get_link_return() const noexcept;

private:
  /**
   * @brief The GstPadLinkReturn for the failed link.
   */
  GstPadLinkReturn link_return;
};

/**
 * @brief Exception thrown when there is an error unlinking two pads.
 */
class Pad_Unlink_Error : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with two pads.
   *
   * @param pad1 The first pad that could not be unlinked.
   * @param pad2 The second pad that could not be unlinked.
   */
  Pad_Unlink_Error(const std::string &pad1, const std::string &pad2);
};

/**
 * @brief Exception thrown when there is an error with caps.
 */
class Bad_Caps : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with caps.
   *
   * @param caps The caps that caused the error.
   */
  Bad_Caps(const std::string &caps);
};

/**
 * @brief Exception thrown when caps are unavailable.
 */
class Unavailable_Caps : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a pad.
   *
   * @param pad The pad that has unavailable caps.
   */
  Unavailable_Caps(const std::string &pad);
};

/**
 * @brief Exception thrown when a wrong message type is encountered.
 */
class Wrong_Message_Type : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with an expected and an
   * actual message type.
   *
   * @param expected The expected message type.
   * @param actual The actual message type.
   */
  Wrong_Message_Type(GstMessageType expected, GstMessageType actual);

  /**
   * @brief Constructor that initializes the exception with the unsupported
   * type.
   *
   * @param t The unsupported message type.
   */
  Wrong_Message_Type(GstMessageType t);
};

/**
 * @brief Exception thrown when there is an IO error status.
 */
class IO_Error_Status : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a status.
   *
   * @param status The IO error status.
   */
  IO_Error_Status(GIOStatus status);

  /**
   * @brief Destructor.
   */
  ~IO_Error_Status() {}

  /**
   * @brief Returns the IO error status.
   *
   * @return GIOStatus The IO error status.
   */
  GIOStatus get_status() const noexcept;

private:
  /**
   * @brief The IO error status.
   */
  GIOStatus error_status;
};

/**
 * @brief Exception thrown when a property is missing.
 */
class Misssing_Property : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a property.
   *
   * @param property The missing property.
   */
  Misssing_Property(const std::string &property);

  /**
   * @brief Constructor that initializes the exception with a property and an
   * element.
   *
   * @param property The missing property.
   * @param element The element that the property is missing from.
   */
  Misssing_Property(const std::string &property, const std::string &element);

  /**
   * @brief Destructor.
   */
  ~Misssing_Property() {}
};

/**
 * @brief Exception thrown when there is a property type error.
 */
class Property_Type_Error : public Exception {
public:
  /**
   * @brief Constructor that initializes the exception with a property.
   *
   * @param property The property with the type error.
   */
  Property_Type_Error(const std::string &property);

  /**
   * @brief Constructor that initializes the exception with a property and an
   * element.
   *
   * @param property The property with the type error.
   * @param element The element that the property is associated with.
   */
  Property_Type_Error(const std::string &property, const std::string &element);

  /**
   * @brief Destructor.
   */
  ~Property_Type_Error() {}
};

} // namespace viflow
