#pragma once

#include <gst/gstformat.h>

namespace viflow {

/**
 * @struct Seeking_Context
 * @brief Structure to hold context for seeking operations.
 */
struct Seeking_Context {

  GstFormat format;   /**< The format of the seek operation. */
  int seekable;       /**< Flag indicating if the operation is seekable. */
  long start_segment; /**< The start segment of the seek operation. */
  long end_segment;   /**< The end segment of the seek operation. */
};
;

} // namespace viflow
