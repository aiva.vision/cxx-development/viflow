#pragma once

#include <gst/gstquery.h>

#include "viflow/query_context.hxx"
#include "viflow/shared_element.hxx"
#include "viflow/unique_ptr.hxx"

namespace viflow {
/**
 * @brief Unreferences a GstQuery instance.
 * @param q The GstQuery instance.
 */
inline void gst_query_unref(GstQuery *q) {
  gst_mini_object_unref(GST_MINI_OBJECT_CAST(q));
}

/**
 * @brief Useful when dealing with low level GstQuery instances.
 */
using unique_query = unique_ptr_fn<GstQuery, gst_query_unref>;

// Forward declarations
class Capabilities;
class Element;
class Pad;

/**
 * @brief Adapter for GstQuery.
 */
class Query {
public:
  Query() = delete;
  Query(Query &&) = default;
  Query(const Query &) = delete;
  Query &operator=(Query &&) = default;
  Query &operator=(const Query &) = delete;
  virtual ~Query() {}

  /**
   * @brief Assumes ownership of the query.
   *
   * @param query The GstQuery instance.
   */
  explicit Query(GstQuery *query);

  /**
   * @brief Returns a pointer to the low level query.
   *
   * @return GstQuery* Pointer to the low level query.
   */
  GstQuery *get_core() const noexcept { return core.get(); }

  /**
   * @brief Performs the query on the given element.
   *
   * @param elem The Shared_Element instance.
   *
   * @return bool True if the query was performed successfully, false otherwise.
   */
  bool query_element(const Shared_Element<> &elem);

  /**
   * @brief Performs the query on the given pad.
   *
   * @param pad The Pad instance.
   *
   * @return bool True if the query was performed successfully, false otherwise.
   */
  bool query_pad(Pad &pad);

private:
  unique_query core;
};

/**
 * @brief Class for handling seeking queries.
 */
class Seeking_Query : public Query {
public:
  /**
   * @brief Constructor for Seeking_Query.
   * @param format The GstFormat instance.
   */
  Seeking_Query(GstFormat format);

  /**
   * @brief Parses the seeking query.
   * @return Seeking_Context The parsed seeking context.
   */
  Seeking_Context parse_seeking();
};

} // namespace viflow
