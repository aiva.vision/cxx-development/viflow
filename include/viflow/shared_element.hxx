#pragma once

#include <memory>

namespace viflow {

class Element;

/**
 * @brief A template alias for a shared pointer to an Element or its derived
 * class.
 *
 * This template is enabled only if E is a base of Element.
 *
 * @tparam E The Element or its derived class.
 */
template <class E = Element,
          typename = std::enable_if_t<std::is_base_of_v<Element, E>>>
using Shared_Element = std::shared_ptr<E>;

} // namespace viflow
