#pragma once

#include "viflow/unique_ptr.hxx"

namespace viflow {

/**
 * @brief A unique pointer to a C-style string.
 */
using unique_c_str = unique_c_ptr<char>;

/**
 * @brief A unique pointer to a C-style wide string.
 */
using unique_c_wstr = unique_c_ptr<wchar_t>;

/**
 * @brief A unique pointer to a C-style string array.
 */
using unique_c_str_array = unique_c_ptr<char *>;

/**
 * @brief A unique pointer to a C-style wide string array.
 */
using unique_c_wstr_array = unique_c_ptr<wchar_t *>;

/**
 * @brief A unique pointer to a gchar.
 */
using unique_g_str = unique_g_ptr<gchar>;

} // namespace viflow
