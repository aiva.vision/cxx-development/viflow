#pragma once

#include <gst/gstpad.h>

#include "shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {

class Element;
/**
 * @brief Interface for drained GCallback adapters.
 */
class Drained_Callback : public Signal_Callback {
public:
   /**
    * @brief Destroy the Drained_Callback object
    */
   virtual ~Drained_Callback() {}

   /**
    * @brief Called by the dummy adapter function when the data for the current element is played.
    *
    * @param elem The element that was played.
    */
   virtual void process(const Shared_Element<> &elem) = 0;
};

/**
 * @brief Interface for unknown-type GCallback adapters.
 */
class Unknown_Type_Callback : public Signal_Callback {
public:
   /**
    * @brief Destroy the Unknown_Type_Callback object
    */
   virtual ~Unknown_Type_Callback() {}

   /**
    * @brief Called by the dummy adapter function when a pad for which there is no further possible decoding is added to the uridecodebin.
    *
    * @param parent The parent element of the pad.
    * @param pad The pad that was added.
    * @param caps The capabilities of the pad.
    */
   virtual void process(const Shared_Element<> &parent, GstPad *pad,
                                  GstCaps *caps) = 0;
};

} // namespace viflow
