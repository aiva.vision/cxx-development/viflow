#pragma once

#include <gio/gio.h>
#include <gst/rtsp/rtsp.h>
#include <gst/sdp/gstsdpmessage.h>

#include "viflow/capabilities.hxx"
#include "viflow/shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {

/**
 * @brief Interface for underrun GCallback adapters.
 */
class Underrun_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Underrun_Callback() {}

  /**
   * @brief Called by the dummy adapter function when there is less data in the
   * buffer than the minimum amounts set.
   *
   * @param q The Shared_Element instance.
   */
  virtual void process(const Shared_Element<> &q) = 0;
};

/**
 * @brief Interface for running GCallback adapters.
 */
class Running_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Running_Callback() {}

  /**
   * @brief Called by the dummy adapter function when there is more data in the
   * buffer than the minimum amounts set.
   *
   * @param q The Shared_Element instance.
   */
  virtual void process(const Shared_Element<> &q) = 0;
};

/**
 * @brief Interface for overrun GCallback adapters.
 */
class Overrun_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Overrun_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the buffer is full.
   *
   * @param q The Shared_Element instance.
   */
  virtual void process(const Shared_Element<> &q) = 0;
};

/**
 * @brief Interface for pushing GCallback adapters.
 */
class Pushing_Callback : public Signal_Callback {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Pushing_Callback() {}

  /**
   * @brief Called by the dummy adapter function when the buffer has enough data
   * again.
   *
   * @param q The Shared_Element instance.
   */
  virtual void process(const Shared_Element<> &q) = 0;
};

} // namespace viflow
