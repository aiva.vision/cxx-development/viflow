#pragma once

namespace viflow {

/**
 * @brief Base class for signal callback handlers.
 */
class Signal_Callback {
public:
  /**
   * @brief Virtual destructor.
   */
  virtual ~Signal_Callback() {}
};

} // namespace viflow
