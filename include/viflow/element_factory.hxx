#pragma once

#include <gst/gstelementfactory.h>
#include <string>

#include "viflow/element.hxx"
#include "viflow/glist_iterator.hxx"

namespace viflow {
/**
 * @brief Useful when dealing with low level GstElementFactory instances.
 */
using unique_element_factory = unique_ptr_fn<GstElementFactory, gst_object_unref>;

/**
 * @brief Adapter for GstElementFactory.
 */
class Element_Factory {
public:
  Element_Factory() = delete;
  Element_Factory(const Element_Factory &factory) = delete;
  Element_Factory(Element_Factory &&factory) = default;
  Element_Factory &operator=(const Element_Factory &factory) = delete;
  Element_Factory &operator=(Element_Factory &&factory) = default;
  ~Element_Factory() = default;

  /**
   * @brief Creates a factory using gst_element_factory_find().
   *
   * @param name The name of the factory to find.
   */
  explicit Element_Factory(const std::string &name);

  /**
   * @brief Returns the name of the factory.
   *
   * @return const char* The name of the factory.
   */
  const char *get_name() const noexcept;

  /**
   * @brief Returns the number of pad templates in this factory.
   *
   * @return unsigned int The number of pad templates.
   */
  unsigned int get_pad_templates_count() const noexcept;

  /**
   * @brief Return an iterator for the static pad templates of this factory.
   *
   * @return GList_Iterator The iterator for the static pad templates.
   */
  GList_Iterator get_static_pad_templates() const noexcept;

  /**
   * @brief Returns the low level factory.
   *
   * @return GstElementFactory* The low level factory.
   */
  GstElementFactory *get_core() const noexcept;

  /**
   * @brief Creates an element and registers it.
   *
   * @param name The name of the element to create.
   *
   * @return Shared_Element<> The created element.
   */
  Shared_Element<> create_element(const std::string &name) const;

private:
  unique_element_factory core;
};

} // namespace viflow
