#pragma once

#define VIFLOW_BIN

#include <memory>
#include <mutex>

#include "viflow/element.hxx"

namespace viflow {

/**
 * @brief Adapter for GstBin. Uses the singleton pattern, just like Element.
 */
class Bin : public Element {
public:
  /**
   * @brief Destroy the Bin object
   */
  virtual ~Bin() {}

  /**
   * @brief Checks whether this element has been added to this bin.
   *
   * @param name The name of the element.
   *
   * @return true if the element has been added to this bin.
   */
  bool contains(const std::string &name);

  /**
   * @brief Retrieves the element from the bin.
   *
   * @tparam E The type of the element.
   * @param name The name of the element.
   *
   * @return A shared pointer to the element.
   */
  template <class E = Element> Shared_Element<E> child(const std::string &name);

  /**
   * @brief Checks whether the bin has a last added element.
   *
   * @return true if the bin has a last added element.
   */
  bool has_last_added() const noexcept;

  /**
   * @brief Retrieves the last added element.
   *
   * @return A shared pointer to the element.
   */
  Shared_Element<> last_added();

  /**
   * @brief Adds the element to the bin and registers it.
   *
   * @tparam E The type of the element.
   * @param elem The element to add.
   *
   * @return A shared pointer to the element.
   */
  template <class E> Shared_Element<E> add(const Shared_Element<E> &elem);

  /**
   * @brief Creates the element with the given factory before adding.
   *
   * @param factory The factory to use.
   * @param name The name of the element.
   *
   * @return A shared pointer to the element.
   */
  Shared_Element<> add(const std::string &factory, const std::string &name);

  /**
   * @brief Creates the element with the given type before adding.
   *
   * @tparam E The type of the element.
   * @tparam Args The types of the arguments to pass to the constructor.
   * @param args The arguments to pass to the constructor.
   *
   * @return A shared pointer to the element.
   */
  template <class E, typename... Args> Shared_Element<E> add(Args &&...args);

  /**
   * @brief Adds the element to the bin and links to the last element.
   *
   * @tparam E The type of the element.
   * @param elem The element to add.
   *
   * @return A shared pointer to the element.
   *
   * @throw Element_Link_Error If the elements cannot be linked.
   */
  template <class E> Shared_Element<E> append(const Shared_Element<E> &elem);

  /**
   * @brief Creates with the given factory, adds and links to the last element.
   *
   * @param factory The factory to use.
   * @param name The name of the element.
   *
   * @return A shared pointer to the element.
   */
  Shared_Element<> append(const std::string &factory, const std::string &name);

  /**
   * @brief Creates, adds and links to the last element.
   *
   * @tparam E The type of the element.
   * @param args The arguments to pass to the constructor.
   *
   * @return A shared pointer to the element.
   */
  template <typename E, typename... Args>
  Shared_Element<E> append(Args &&...args);

  /**
   * @brief Removes the element from the bin.
   *
   * Unregister it from the viflow registry using
   * viflow::Element::unregister_instance() if it's not going to be used after
   * the call.
   *
   * @param name The name of the element.
   *
   * @return A shared pointer to the element.
   */
  Shared_Element<> remove(const std::string &name);

  /**
   * @brief Links the two elements.
   *
   * @param elem1 The name of the first element.
   * @param elem2 The name of the second element.
   */
  void link_elements(const std::string &elem1, const std::string &elem2);

  /**
   * @brief Links the two elements.
   *
   * @param elem1 The name of the first element.
   * @param src_pad_name The name of the source pad of the first element.
   * @param elem2 The name of the second element.
   * @param sink_pad_name The name of the sink pad of the second element.
   */
  void link_elements(const std::string &elem1, const std::string &src_pad_name,
                     const std::string &elem2,
                     const std::string &sink_pad_name);

  /**
   * @brief Adds a ghost pad from one of the member's pads.
   *
   * @param elem The name of the element.
   * @param pad_name The name of the pad.
   */
  void add_ghost_pad(const std::string &elem, const std::string &pad_name);

  /**
   * @brief Adds a ghost pad named "src" from the element's "src" pad.
   *
   * @param elem The name of the element.
   */
  void set_src_elem(const std::string &elem);

  /**
   * @brief Adds a ghost pad named "sink" from the element's "sink" pad.
   *
   * @param elem The name of the element.
   */
  void set_sink_elem(const std::string &elem);

  /**
   * @brief Unregisters this element and its children.
   */
  virtual void unregister();

protected:
  /**
   * @brief Assumes ownership for the bin.
   *
   * @param bin The bin to assume ownership for.
   */
  Bin(GstElement *bin);

  /**
   * @brief Construct a new Bin element.
   *
   * @param name
   */
  Bin(const std::string &name);

private:
  /*
   * Acquire to modify children.
   */
  std::mutex children_mtx;
  /*
   * Last added element.
   */
  Weak_Element<> last_added_child;
  /*
   * Elements added to the bin.
   */
  std::unordered_map<std::string, Weak_Element<>> children;

  friend class Element;
};

} // namespace viflow

#include "viflow/bin.txx"
