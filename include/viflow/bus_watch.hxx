#pragma once

#include <gst/gstbus.h>

#include "viflow/message.hxx"

namespace viflow {

class Bus;

/**
 * @brief Interface for bus GCallback adapters.
 */
class Bus_Watch {
public:
  /**
   * @brief Destroy the Bus_Watch object
   */
  virtual ~Bus_Watch() {}

  /**
   * @brief Process a message from the bus
   *
   * @param bus The bus from which the message originates
   * @param message The message to process
   *
   * @return bool Return false to remove the watch from the bus
   */
  virtual bool process_message(const Bus &bus, const Message &message) = 0;
};

/**
 * @brief Interface for bus signal GCallback adapters.
 */
class Bus_Signal_Watch {
public:
  /**
   * @brief Destroy the Bus_Signal_Watch object
   */
  virtual ~Bus_Signal_Watch() {}

  /**
   * @brief Process a message from the bus
   *
   * @param bus The bus from which the message originates
   * @param message The message to process
   */
  virtual void process_message(const Bus &bus, const Message &message) = 0;
};

} // namespace viflow
