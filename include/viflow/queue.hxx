#pragma once

#include "viflow/element.hxx"
#include "viflow/queue_callback.hxx"

namespace viflow {

/**
 * @brief Namespace for Queue signals.
 */
namespace Queue_Signals {

constexpr const char *underrun = "underrun";
constexpr const char *running = "running";
constexpr const char *overrun = "overrun";
constexpr const char *pushing = "pushing";

} // namespace Queue_Signals

/**
 * @brief Namespace for Queue properties.
 */
namespace Queue_Props {

constexpr const char *current_level_buffers = "current-level-buffers";
constexpr const char *current_level_bytes = "current-level-bytes";
constexpr const char *current_level_time = "current-level-time";
constexpr const char *flush_on_eos = "flush-on-eos";
constexpr const char *leaky = "leaky";
constexpr const char *max_size_buffers = "max-size-buffers";
constexpr const char *max_size_bytes = "max-size-bytes";
constexpr const char *max_size_time = "max-size-time";
constexpr const char *min_threshold_buffers = "min-threshold-buffers";
constexpr const char *min_threshold_bytes = "min-threshold-bytes";
constexpr const char *min_threshold_time = "min-threshold-time";
constexpr const char *name = "name";
constexpr const char *parent = "parent";
constexpr const char *silent = "silent";

} // namespace Queue_Props

/**
 * @brief Class for handling queue operations.
 */
class Queue : public Element {
public:
  /**
   * @brief Destructor.
   */
  virtual ~Queue() {}

  /**
   * @brief Exposes the existing callback methods.
   */
  using Element::connect_callback;

  /**
   * @brief Connects a callback for the underrun signal.
   *
   * @param callback The Underrun_Callback instance.
   */
  void connect_callback(std::unique_ptr<Underrun_Callback> &&callback);

  /**
   * @brief Connects a callback for the running signal.
   *
   * @param callback The Running_Callback instance.
   */
  void connect_callback(std::unique_ptr<Running_Callback> &&callback);

  /**
   * @brief Connects a callback for the overrun signal.
   *
   * @param callback The Overrun_Callback instance.
   */
  void connect_callback(std::unique_ptr<Overrun_Callback> &&callback);

  /**
   * @brief Connects a callback for the pushing signal.
   *
   * @param callback The Pushing_Callback instance.
   */
  void connect_callback(std::unique_ptr<Pushing_Callback> &&callback);

protected:
  /**
   * @brief Creates a named element.
   *
   * @param name The name of the element.
   */
  Queue(const std::string &name);

  friend class Element;
};
} // namespace viflow
