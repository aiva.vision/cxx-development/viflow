#pragma once

#include <gst/gstbus.h>

#include "viflow/shared_element.hxx"
#include "viflow/signal_callback.hxx"

namespace viflow {

class Pad;
/**
 * @brief Interface for no-more-pads GCallback adapters.
 */
class No_More_Pads_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the No_More_Pads_Callback object
   */
  virtual ~No_More_Pads_Callback() {}

  /**
   * @brief Called by the dummy adapter function when no more dynamic pads will
   * be generated.
   *
   * @param elem The element that emitted the signal.
   */
  virtual void process(const Shared_Element<> &elem) = 0;
};

/**
 * @brief Interface for pad-added GCallback adapters.
 */
class Pad_Added_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the Pad_Added_Callback object
   */
  virtual ~Pad_Added_Callback() {}

  /**
   * @brief Called by the dummy adapter function when a new pad is added.
   *
   * @param elem The element that emitted the signal.
   * @param new_pad The new pad that was added.
   */
  virtual void process_pad(const Shared_Element<> &elem, Pad &new_pad) = 0;
};

/**
 * @brief Interface for pad-removed GCallback adapters.
 */
class Pad_Removed_Callback : public Signal_Callback {
public:
  /**
   * @brief Destroy the Pad_Removed_Callback object
   */
  virtual ~Pad_Removed_Callback() {}

  /**
   * @brief Called by the dummy adapter function when a pad is removed.
   *
   * @param elem The element that emitted the signal.
   * @param old_pad The pad that was removed.
   */
  virtual void process_pad(const Shared_Element<> &elem, GstPad *old_pad) = 0;
};

} // namespace viflow
