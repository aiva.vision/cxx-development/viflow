#pragma once

#include <gst/gstsample.h>

#include "viflow/buffer.hxx"
#include "viflow/unique_ptr.hxx"

namespace viflow {

/**
 * @brief Unreferences a GstSample instance.
 * @param sample The GstSample instance.
 */
inline void gst_sample_unref(GstSample *sample) {
  gst_mini_object_unref(GST_MINI_OBJECT_CAST(sample));
}

/**
 * @brief Useful when dealing with low level GstSample instances.
 */
using unique_sample = unique_ptr_fn<GstSample, viflow::gst_sample_unref>;

/**
 * @brief Adapter for GstSample.
 */
class Sample {
public:
  Sample(const Sample &s) = delete;
  Sample(Sample &&s) = default;
  virtual ~Sample() {}
  Sample &operator=(const Sample &s) = delete;
  Sample &operator=(Sample &&s) = default;

  /**
   * @brief Assumes ownership of the sample.
   *
   * @param sample The GstSample instance.
   */
  explicit Sample(GstSample *sample);

  /**
   * @brief Checks if the low level sample is null.
   *
   * @return bool True if the sample is null, false otherwise.
   */
  bool is_empty() const noexcept;

  /**
   * @brief Returns the low level sample.
   *
   * @return GstSample* Pointer to the low level sample.
   */
  GstSample *get_core() const noexcept;

  /**
   * @brief Returns the associated buffer.
   *
   * @return Buffer The associated buffer.
   */
  Buffer get_buffer() const noexcept;
  ;

private:
  unique_sample core;
};

} // namespace viflow
