#pragma once

#include "viflow/bin.hxx"

namespace viflow {
/**
 * @brief Adapter for a GStreamer pipeline.
 */
class Pipeline : public Bin {
public:
  /**
   * @brief This class is very likely to be inherited from.
   */
  virtual ~Pipeline() {}

protected:
  /**
   * @brief Assumes ownership for the element.
   *
   * @param pipeline The GstElement instance.
   */
  Pipeline(GstElement *pipeline);

  /**
   * @brief Creates the low level pipeline with gst_pipeline_new().
   *
   * @param name The name of the pipeline.
   *
   * @throw Factory_Error If the pipeline cannot be created.
   */
  Pipeline(const std::string &name);

  friend class Element;
};

using Shared_Pipeline = Shared_Element<Pipeline>;

/**
 * @brief Creates a new pipeline based on command line syntax.
 * @param name The name of the pipeline.
 * @param pipeline The command line syntax for the pipeline.
 * @return Shared_Pipeline The created pipeline.
 */
Shared_Pipeline parse_launch(const std::string &name,
                             const std::string &pipeline);

} // namespace viflow
