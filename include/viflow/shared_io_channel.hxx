#pragma once

#include <memory>

namespace viflow {

class IO_Channel;

/**
 * @brief A template alias for a shared pointer to an IO_Channel.
 */
using Shared_IO_Channel = std::shared_ptr<IO_Channel>;;

} // namespace viflow
