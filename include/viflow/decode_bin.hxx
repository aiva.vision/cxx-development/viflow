#pragma once

#include "viflow/child_proxy.hxx"
#include "viflow/decode_bin_callback.hxx"

namespace viflow {

/**
 * @brief Namespace for Decode Bin signal names
 */
namespace Decode_Bin_Signals {

constexpr const char *drained = "drained";
constexpr const char *unknown_type = "unknown-type";

} // namespace Decode_Bin_Signals

/**
 * @brief Class representing a Decode Bin element.
 */
class Decode_Bin : public Child_Proxy {
public:
  /**
   * @brief Destroy the Decode_Bin object
   */
  virtual ~Decode_Bin() {}

  /**
   * @brief Expose the existing callback methods.
   */
  using Child_Proxy::connect_callback;

  /**
   * @brief Connects a callback for the drained signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<Drained_Callback> &&callback);

  /**
   * @brief Connects a callback for the unknown-type signal.
   *
   * @param callback The callback to connect
   */
  void connect_callback(std::unique_ptr<Unknown_Type_Callback> &&callback);

protected:
  /**
   * @brief Construct a new Decode_Bin element.
   */
  using Child_Proxy::Child_Proxy;

  /**
   * @brief Creates an element with the decodebin factory.
   *
   * @param name The name of the element.
   */
  Decode_Bin(const std::string &name);

  friend class Element;
};

} // namespace viflow
