#pragma once

#include <glib.h>

#include "viflow/shared_io_channel.hxx"

namespace viflow {

/**
 * @brief This class represents an IO channel watch.
 */
class IO_Channel_Watch {
public:
  /**
   * @brief Virtual destructor.
   */
  virtual ~IO_Channel_Watch() {}

  /**
   * @brief Pure virtual function to process input.
   *
   * @param channel The shared IO channel to process.
   * @param condition The condition to process.
   *
   * @return gboolean Returns a gboolean indicating the success of the process.
   */
  virtual gboolean process(const Shared_IO_Channel &channel,
                           GIOCondition condition) = 0;
};
} // namespace viflow
