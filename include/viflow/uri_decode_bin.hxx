#pragma once

#include "viflow/decode_bin.hxx"
#include "viflow/uri_decode_bin_callback.hxx"

namespace viflow {

/**
 * @brief Namespace for URI_Decode_Bin signals.
 */
namespace URI_Decode_Bin_Signals {

constexpr const char *source_setup = "source-setup";

} // namespace URI_Decode_Bin_Signals

/**
 * @brief Namespace for URI_Decode_Bin properties.
 */
namespace URI_Decode_Bin_Props {

constexpr const char *async_handling = "async-handling";
constexpr const char *buffer_duration = "buffer-duration";
constexpr const char *buffer_size = "buffer-size";
constexpr const char *caps = "caps";
constexpr const char *connection_speed = "connection-speed";
constexpr const char *download = "download";
constexpr const char *expose_all_streams = "expose-all-streams";
constexpr const char *message_forward = "message-forward";
constexpr const char *name = "name";
constexpr const char *parent = "parent";
constexpr const char *ring_buffer_max_size = "ring-buffer-max-size";
constexpr const char *source = "source";
constexpr const char *subtitle_encoding = "subtitle-encoding";
constexpr const char *uri = "uri";
constexpr const char *use_buffering = "use-buffering";

} // namespace URI_Decode_Bin_Props

/**
 * @brief Class for handling URI decode bin operations.
 */
class URI_Decode_Bin : public Decode_Bin {
public:
  virtual ~URI_Decode_Bin() {}

  /**
   * @brief Exposes the existing callback methods.
   */
  using Decode_Bin::connect_callback;

  /**
   * @brief Connects a callback for the source-setup signal.
   *
   * @param callback The Source_Setup_Callback instance.
   */
  void connect_callback(std::unique_ptr<Source_Setup_Callback> &&callback);

protected:
  /**
   * @brief Creates an element with the uridecodebin factory.
   *
   * @param name The name of the element.
   */
  URI_Decode_Bin(const std::string &name);

  friend class Element;
};

} // namespace viflow
