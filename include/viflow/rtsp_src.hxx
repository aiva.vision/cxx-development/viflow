#pragma once

#include "viflow/element.hxx"
#include "viflow/rtsp_src_callback.hxx"

namespace viflow {

/**
 * @brief Namespace for RTSP_Src signals.
 */
namespace RTSP_Src_Signals {

constexpr const char *on_sdp = "on-sdp";
constexpr const char *select_stream = "select-stream";
constexpr const char *new_manager = "new-manager";
constexpr const char *request_rtcp_key = "request-rtcp-key";
constexpr const char *accept_certificate = "accept-certificate";
constexpr const char *before_send = "before-send";

} // namespace RTSP_Src_Signals

/**
 * @brief Namespace for RTSP_Src properties.
 */
namespace RTSP_Src_Props {

constexpr const char *async_handling = "async-handling";
constexpr const char *backchannel = "backchannel";
constexpr const char *buffer_mode = "buffer-mode";
constexpr const char *connection_speed = "connection-speed";
constexpr const char *default_rtsp_version = "default-rtsp-version";
constexpr const char *do_retransmission = "do-retransmission";
constexpr const char *do_rtcp = "do-rtcp";
constexpr const char *do_rtsp_keep_alive = "do-rtsp-keep-alive";
constexpr const char *drop_on_latency = "drop-on-latency";
constexpr const char *latency = "latency";
constexpr const char *location = "location";
constexpr const char *max_rtcp_rtp_time_diff = "max-rtcp-rtp-time-diff";
constexpr const char *max_ts_offset = "max-ts-offset";
constexpr const char *max_ts_offset_adjutment = "max-ts-offset-adjustment";
constexpr const char *message_forward = "message-forward";
constexpr const char *multicast_iface = "multicast-iface";
constexpr const char *name = "name";
constexpr const char *nat_method = "nat-method";
constexpr const char *ntp_sync = "ntp-sync";
constexpr const char *ntp_time_source = "ntp-time-source";
constexpr const char *parent = "parent";
constexpr const char *port_range = "port-range";
constexpr const char *probation = "probation";
constexpr const char *protocols = "protocols";
constexpr const char *proxy = "proxy";
constexpr const char *proxy_id = "proxy-id";
constexpr const char *proxy_pw = "proxy-pw";
constexpr const char *retry = "retry";
constexpr const char *rfc7273_sync = "rfc7273-sync";
constexpr const char *rtp_blocksize = "rtp-blocksize";
constexpr const char *sdes = "sdes";
constexpr const char *short_header = "short-header";
constexpr const char *tcp_timeout = "tcp-timeout";
constexpr const char *teardown_timeout = "teardown-timeout";
constexpr const char *timeout = "timeout";
constexpr const char *tls_database = "tls-database";
constexpr const char *tls_interaction = "tls-interaction";
constexpr const char *tls_validation_flags = "tls-validation-flags";
constexpr const char *udp_buffer_size = "udp-buffer-size";
constexpr const char *udp_reconnect = "udp-reconnect";
constexpr const char *user_agent = "user-agent";
constexpr const char *user_id = "user-id";
constexpr const char *user_pw = "user-pw";

} // namespace RTSP_Src_Props

/**
 * @brief Class for handling RTSP source operations.
 */
class RTSP_Src : public Element {
public:
  /**
   * @brief Destructor.
   */
  virtual ~RTSP_Src() {}

  /**
   * @brief Exposes the existing callback methods.
   */
  using Element::connect_callback;

  /**
   * @brief Connects a callback for the on-sdp signal.
   *
   * @param callback The On_SDP_Callback instance.
   */
  void connect_callback(std::unique_ptr<On_SDP_Callback> &&callback);

  /**
   * @brief Connects a callback for the select-stream signal.
   *
   * @param callback The Select_Stream_Callback instance.
   */
  void connect_callback(std::unique_ptr<Select_Stream_Callback> &&callback);

  /**
   * @brief Connects a callback for the new-manager signal.
   *
   * @param callback The New_Manager_Callback instance.
   */
  void connect_callback(std::unique_ptr<New_Manager_Callback> &&callback);

  /**
   * @brief Connects a callback for the request-rtcp-key signal.
   *
   * @param callback The Request_RTCP_Key_Callback instance.
   */
  void connect_callback(std::unique_ptr<Request_RTCP_Key_Callback> &&callback);

  /**
   * @brief Connects a callback for the accept-certificate signal.
   *
   * @param callback The Accept_Certificate_Callback instance.
   */
  void
  connect_callback(std::unique_ptr<Accept_Certificate_Callback> &&callback);

  /**
   * @brief Connects a callback for the before-send signal.
   *
   * @param callback The Before_Send_Callback instance.
   */
  void connect_callback(std::unique_ptr<Before_Send_Callback> &&callback);

protected:
  /**
   * @brief Creates a named element.
   *
   * @param name The name of the element.
   */
  RTSP_Src(const std::string &name);

  friend class Element;
};
;

} // namespace viflow
