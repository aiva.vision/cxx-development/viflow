#pragma once

#include <gst/gstbuffer.h>

#include "viflow/exceptions.hxx"

namespace viflow {
/**
 * @brief This class represents the information of a map.
 */
class Map_Info {
public:
  Map_Info(Map_Info &&) = default;
  Map_Info(const Map_Info &) = default;
  Map_Info &operator=(Map_Info &&) = default;
  Map_Info &operator=(const Map_Info &) = default;
  /**
   * @brief Constructor that maps the buffer info.
   *
   * @param buf The GstBuffer instance.
   * @param flags The GstMapFlags instance.
   */
  explicit Map_Info(GstBuffer *buf, GstMapFlags flags = GST_MAP_READ);

  /**
   * @brief Unmaps the buffer info.
   */
  virtual ~Map_Info();

  /**
   * @brief Performs a checked cast for the mapped data.
   *
   * @tparam T The type to cast to.
   *
   * @return T* The casted data.
   *
   * @throw Exception If the cast fails.
   */
  template <typename T> T *data() {
    auto data = reinterpret_cast<T *>(map_info.data);
    if (!data) {
      throw Exception("Failed to cast the mapped info data from buffer");
    }
    return data;
  }

private:
  /**
   * @brief The GstBuffer instance.
   */
  GstBuffer *buffer{};

  /**
   * @brief The GstMapInfo instance.
   */
  GstMapInfo map_info{};
};

} // namespace viflow
