#pragma once

#include "viflow/element_factory.hxx"
#include "viflow/message.hxx"

/*
   Prints information about a Pad Template, including its Capabilities.
   */
void print_pad_templates_information(viflow::Element_Factory &factory);

/*
   Prints the capabilities for the named pad.
   */
void print_pad_capabilities(const viflow::Shared_Element<> &elem,
                            const std::string &pad_name);

/*
   Handles bus messages.
   */
void handle_message(viflow::Message &message);
