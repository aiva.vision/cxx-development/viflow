#include <gst/gst.h>
#include <gst/gstelement.h>
#include <iostream>

#include "utils.hxx"
#include "viflow/pipeline.hxx"

void stop(int signal_number) { viflow::Element::instance("pipeline")->stop(); }

int main(int argc, char *argv[]) {
  /*
     Register CTRL-C handler.
     */
  signal(SIGINT, stop);
  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);
  /*
     Create the factories.
     */
  viflow::Element_Factory source_factory("audiotestsrc");
  viflow::Element_Factory sink_factory("autoaudiosink");
  /*
     Print information about the pad templates of these factories
     */
  print_pad_templates_information(source_factory);
  print_pad_templates_information(sink_factory);
  /*
     Ask the factories to instantiate actual elements.
     */
  auto source = source_factory.create_element("source");
  auto sink = sink_factory.create_element("sink");
  /*
     Create the pipeline.
     */
  auto pipeline =
      viflow::Element::register_instance<viflow::Pipeline>("pipeline");

  pipeline->add(source);
  pipeline->append(sink);
  /*
     Print initial negotiated caps (in NULL state).
     */
  std::cout << "In NULL state:\n";
  print_pad_capabilities(sink, "sink");
  /*
     Change the pipeline's state.
     */
  pipeline->play();
  /*
     Retrieve the bus.
     */
  auto &bus = pipeline->get_bus();
  /*
     Properly cast the message type after binary OR.
     */
  auto message_type = static_cast<GstMessageType>(
      GST_MESSAGE_STATE_CHANGED | GST_MESSAGE_ERROR | GST_MESSAGE_EOS);

  /*
     Listen to the bus.
     */
  do {
    auto state = pipeline->get_state();
    /*
       Break out of the loop if the pipeline has stopped.
       */
    if (state.current == GST_STATE_NULL) {
      break;
    }

    auto message = bus->timed_pop_filtered(100 * GST_MSECOND, message_type);

    if (!message.empty()) {
      handle_message(message);
    }
  } while (true);
  /*
     Allow the pipeline to free its resources.
     */
  pipeline->stop();
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
