#include <gst/gst.h>
#include <iostream>

#include "viflow/exceptions.hxx"
#include "viflow/pipeline.hxx"
#include "viflow/query.hxx"

#include "utils.hxx"

const char *g_indent = "      ";

static gboolean print_field(GQuark field, const GValue *value,
                            gpointer user_data) {
  std::unique_ptr<char> str{gst_value_serialize(value)};

  std::cout << g_indent << g_quark_to_string(field) << ": " << str.get()
            << std::endl;
  return true;
}

static void print_capabilities(const viflow::Capabilities &caps) {

  if (caps.is_any()) {
    std::cout << g_indent << "ANY\n";
    return;
  }
  if (caps.is_empty()) {
    std::cout << g_indent << "EMPTY\n";
    return;
  }

  for (unsigned int i = 0; i < caps.get_size(); i++) {
    GstStructure *structure = caps.get_structure(i);

    std::cout << g_indent << gst_structure_get_name(structure) << std::endl;

    gst_structure_foreach(structure, print_field, nullptr);
  }
}

void print_pad_templates_information(viflow::Element_Factory &factory) {

  std::cout << "Pad Templates for '" << factory.get_name() << "':\n";
  if (!factory.get_pad_templates_count()) {
    std::cout << "  none\n";
    return;
  }

  auto templates_it = factory.get_static_pad_templates();
  while (*templates_it) {
    GstStaticPadTemplate *pad_template =
        static_cast<GstStaticPadTemplate *>(*templates_it);
    ++templates_it;

    if (pad_template->direction == GST_PAD_SRC) {
      std::cout << "  SRC template: '" << pad_template->name_template << "'\n";
    } else if (pad_template->direction == GST_PAD_SINK) {
      std::cout << "  SINK template: '" << pad_template->name_template << "'\n";
    } else {

      std::cout << "  UNKNOWN!!! template: '" << pad_template->name_template
                << "'\n";
    }

    if (pad_template->presence == GST_PAD_ALWAYS)
      std::cout << "    Availability: Always\n";
    else if (pad_template->presence == GST_PAD_SOMETIMES)
      std::cout << "    Availability: Sometimes\n";
    else if (pad_template->presence == GST_PAD_REQUEST)
      std::cout << "    Availability: On request\n";
    else
      std::cout << "    Availability: UNKNOWN!!!\n";

    if (pad_template->static_caps.string) {
      std::cout << "    Capabilities:\n";
      viflow::Capabilities caps{
          gst_static_caps_get(&pad_template->static_caps)};
      print_capabilities(caps);
    }
    std::cout << std::endl;
  }
}

void print_pad_capabilities(const viflow::Shared_Element<> &elem,
                            const std::string &pad_name) {
  /*
     Retrieve pad.
     */
  auto &pad = elem->get_pad(pad_name);
  /*
     Retrieve negotiated caps (or acceptable caps if negotiation is not
     finished).
     */
  auto caps = pad.get_caps();

  std::cout << "Caps for the " << pad_name << " pad:\n";

  print_capabilities(caps);
}

void handle_message(viflow::Message &message) {

  switch (message.get_type()) {
  case GST_MESSAGE_ERROR: {
    auto context = message.parse_error();
    std::cerr << "Received an error from element " << message.get_source_name()
              << " with content \"" << context.error.message()
              << "\" and with debug info: \"" << context.debug.get() << "\""
              << std::endl;
    break;
  }
  case GST_MESSAGE_EOS:
    std::cerr << "End-Of-Stream reached.\n";
    break;
  case GST_MESSAGE_STATE_CHANGED: {

    auto pipeline = viflow::Element::instance<viflow::Pipeline>("pipeline");
    auto context = message.parse_state_changed();

    if (pipeline->get_name() == message.get_source_name()) {

      std::cout << "Pipeline state has changed from "
                << viflow::to_string(context.old_state) << " to "
                << viflow::to_string(context.new_state) << std::endl;

      print_pad_capabilities(pipeline->child("sink"), "sink");
    }
    break;
  }
  default:
    /* We should not reach here because we only asked for ERRORs and EOS */
    std::cerr << "Unexpected message received.\n";
    break;
  }
}
