add_executable(formats_and_capabilities
  source/utils.cxx
  source/main.cxx)

target_link_libraries(formats_and_capabilities
  PRIVATE
    aiva::viflow
    PkgConfig::gstreamer)

target_include_directories(formats_and_capabilities
  PRIVATE
    ${CMAKE_SOURCE_DIR}/include
    include)
