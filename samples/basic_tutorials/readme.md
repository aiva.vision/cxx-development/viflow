# Samples

These samples are mirroring the official GStreamer samples from the [basic tutorials](https://gstreamer.freedesktop.org/documentation/tutorials/basic/index.html).

### Index

1. [Hello World](hello_world)
2. [Concepts](concepts)
3. [Dynamic pipelines](dynamic_pipelines)
4. [Time management](time_management)
5. [Media player](media_player)
6. [Formats and capabilities](formats_and_capabilities)
7. [Multithreading and pad availability](multithreading_and_pad_availability)
8. [Short-cutting the pipeline](short_cutting_the_pipeline)
12. [Streaming](streaming)
13. [Playback speed](playback_speed)
