# Hello World

This sample reimplements the basic tutorial 1 from the GStreamer [basic tutorials](https://gstreamer.freedesktop.org/documentation/tutorials/basic/index.html).

Note how the boilerplate is significantly reduced and the cleanup is completely abstracted away.
