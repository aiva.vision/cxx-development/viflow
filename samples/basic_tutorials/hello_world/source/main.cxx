#include <gst/gst.h>

#include "viflow/pipeline.hxx"

int main(int argc, char *argv[]) {
  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);
  /*
     Create the pipeline.
     */
  auto pipeline = viflow::parse_launch(
      "pipeline",
      "playbin "
      "uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/"
      "sintel_trailer-480p.webm");
  /*
     Change the pipeline's state.
     */
  pipeline->play();
  /*
     Retrieve the bus.
     */
  auto &bus = pipeline->get_bus();
  /*
     Properly cast the message type after binary OR.
     */
  auto message_type =
      static_cast<GstMessageType>(GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
  /*
     Block until EOS is received.
     */
  auto message = bus->timed_pop_filtered(GST_CLOCK_TIME_NONE, message_type);
  /*
     Allow the pipeline to free its resources.
     */
  pipeline->stop();
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
