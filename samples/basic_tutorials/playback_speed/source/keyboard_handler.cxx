#include <iostream>

#include "viflow/exceptions.hxx"
#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "keyboard_handler.hxx"

Keyboard_Handler::Keyboard_Handler(const viflow::Shared_Pipeline &p)
    : pipeline{p} {};

gboolean Keyboard_Handler::process(const viflow::Shared_IO_Channel &channel,
                                   GIOCondition condition) {
  std::string line;
  try {
    line = channel->read_line();
  } catch (const viflow::IO_Error_Status &e) {
    std::cerr << e.what() << std::endl;
    return true;
  }

  switch (tolower(line[0])) {
  case 'p':
    playing = !playing;
    pipeline->set_state(playing ? GST_STATE_PLAYING : GST_STATE_PAUSED);
    std::cout << "Setting state to " << (playing ? "PLAYING" : "PAUSE")
              << std::endl;
    break;
  case 's':
    if (g_ascii_isupper(line[0])) {
      rate *= 2.0;
    } else {
      rate /= 2.0;
    }
    send_seek_event();
    break;
  case 'd':
    rate *= -1.0;
    send_seek_event();
    break;
  case 'n': {
    viflow::Event step_event(GST_FORMAT_BUFFERS, 1, std::abs(rate), true,
                             false);

    auto video_sink = pipeline->child("video_sink");
    video_sink->send_event(std::move(step_event));

    std::cout << "Stepping one frame\n";
    break;
  }
  case 'q': {
    auto &loop = viflow::Main_Loop::instance();

    pipeline->stop();
    loop->quit();

    break;
  }
  }
  return true;
}

void Keyboard_Handler::send_seek_event() {
  /*
     Create the seek event.
     */
  auto seek_event = create_event(rate);
  /*
     Send the event
     */
  auto video_sink = pipeline->child("video_sink");
  video_sink->send_event(std::move(seek_event));

  std::cout << "Current rate: " << rate << std::endl;
}

viflow::Event Keyboard_Handler::create_event(float rate) {
  /*
     Obtain the current position, needed for the seek event
     */
  auto position = pipeline->query_position(GST_FORMAT_TIME);
  if (position < 0) {
    throw viflow::Exception{"Failed to query the position"};
  }
  auto seek_flags =
      static_cast<GstSeekFlags>(GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE);

  if (rate > 0) {
    return viflow::Event(rate, GST_FORMAT_TIME, seek_flags, GST_SEEK_TYPE_SET,
                         position, GST_SEEK_TYPE_END, 0);
  } else {
    return viflow::Event(rate, GST_FORMAT_TIME, seek_flags, GST_SEEK_TYPE_SET,
                         0, GST_SEEK_TYPE_SET, position);
  }
}
