#include <cstdio>
#include <gst/audio/audio.h>
#include <iostream>

#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "keyboard_handler.hxx"

void stop(int signal_number) {
  /*
     Stop the pipeline.
     */
  viflow::Element::instance("pipeline")->stop();
  /*
     Quit from the main loop.
     */
  viflow::Main_Loop::instance()->quit();
}

int main(int argc, char *argv[]) {
  /*
     Register CTRL-C handler.
     */
  signal(SIGINT, stop);
  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);
  /*
     Print usage information.
     */
  std::cout
      << "USAGE: Choose one of the following options, then press enter:\n"
         " 'P' to toggle between PAUSE and PLAY\n"
         " 'S' to increase playback speed, 's' to decrease playback speed\n"
         " 'D' to toggle playback direction\n"
         " 'N' to move to next frame (in the current direction, better in "
         "PAUSE)\n"
         " 'Q' to quit\n";
  /*
     Create the pipeline.
     */
  auto pipeline = viflow::parse_launch(
      "pipeline",
      "playbin "
      "uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/"
      "sintel_trailer-480p.webm");
  /*
     Create the IO Channel.
     */
  auto channel = viflow::IO_Channel::instance(fileno(stdin));
  channel->add_watch(std::make_unique<Keyboard_Handler>(pipeline));
  /*
     Change the pipeline's state.
     */
  pipeline->play();
  /*
     Create a Main Loop and set it to run.
     */
  auto &loop = viflow::Main_Loop::register_instance();
  loop->run();
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
