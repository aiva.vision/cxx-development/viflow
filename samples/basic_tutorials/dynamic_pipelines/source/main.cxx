#include <gst/gst.h>
#include <gst/gstelement.h>
#include <gst/gstevent.h>
#include <gst/gstregistry.h>
#include <iostream>

#include "source_callback.hxx"
#include "viflow/pipeline.hxx"

void stop(int signal_number) {
  /*
     Send EOS and let the pipeline stop cleanly.
     */
  viflow::Element::instance("pipeline")->send_event(gst_event_new_eos());
}

bool has_plugin(const std::string &factory) {

  viflow::unique_gst_ptr<GstPluginFeature> feature{
      gst_registry_lookup_feature(gst_registry_get(), factory.c_str())};

  return !feature.empty();
}

int main(int argc, char *argv[]) {
  /*
     Register CTRL-C handler.
     */
  signal(SIGINT, stop);
  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);
  /*
     Create the pipeline.
     */
  auto pipeline =
      viflow::Element::register_instance<viflow::Pipeline>("pipeline");
  /*
     Add the source first.
     */
  pipeline->add("uridecodebin", "source");
  /*
     Build the audio part.
     */
  pipeline->add("audioconvert", "audioconvert");
  pipeline->append("audioresample", "resample");
  pipeline->append("autoaudiosink", "audiosink");
  /*
     Build the video part.
     */
  if (has_plugin("nvvideoconvert")) {
    /*
       If nvvideoconvert is found, the uridecodebin element must have picked the
       nvv4l2decoder plugin, which is going to output frames allocated on the
       VRAM. Therefore, we must copy the frames back to the RAM.
       */
    pipeline->add("nvvideoconvert", "videoconvert");
  } else {
    pipeline->add("videoconvert", "videoconvert");
  }
  pipeline->append("autovideosink", "videosink");
  /*
     Retrieve the source element.
     */
  auto source = pipeline->child("source");
  /*
     Set the URI.
     */
  source->set_property("uri",
                       "https://www.freedesktop.org/software/gstreamer-sdk/"
                       "data/media/sintel_trailer-480p.webm");
  /*
     Connect to the pad-added signal.
     */
  source->connect_callback(std::make_unique<Source_Callback>());
  /*
     Change the pipeline's state.
     */
  pipeline->play();
  /*
     Retrieve the bus.
     */
  auto &bus = pipeline->get_bus();
  /*
     Properly cast the message type after binary OR.
     */
  auto message_type =
      static_cast<GstMessageType>(GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
  /*
     Block until EOS is received.
     */
  do {
    auto message = bus->timed_pop_filtered(GST_CLOCK_TIME_NONE, message_type);

    switch (message.get_type()) {
    case GST_MESSAGE_ERROR: {
      auto context = message.parse_error();
      std::cerr << "Received an error from element "
                << message.get_source_name() << " with content \""
                << context.error.message() << "\" and with debug info: \""
                << context.debug.get() << "\"" << std::endl;
      pipeline->stop();
      break;
    }
    case GST_MESSAGE_EOS:
      std::cerr << "End-Of-Stream reached.\n";
      pipeline->stop();
      break;
    default:
      /* We should not reach here because we only asked for ERRORs and EOS */
      std::cerr << "Unexpected message received.\n";
      break;
    }
  } while (pipeline->get_state().current == GST_STATE_PLAYING);
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
