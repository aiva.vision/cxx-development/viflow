#include <iostream>

#include "source_callback.hxx"

void Source_Callback::process_pad(const viflow::Shared_Element<> &elem,
                                  viflow::Pad &new_pad) {

  std::cout << "Received new pad " << new_pad.get_name() << " from "
            << elem->get_name() << ".\n";

  auto pipeline = viflow::Element::instance<viflow::Pipeline>("pipeline");

  auto &audio_sink_pad = pipeline->child("audioconvert")->get_pad("sink");
  auto &video_sink_pad = pipeline->child("videoconvert")->get_pad("sink");

  auto caps = new_pad.get_current_caps();
  auto structure = caps.get_structure(0);

  /*
     If the audio converter is linked, the job is already done.
     */
  if (!audio_sink_pad.is_linked()) {

    std::string media_type = gst_structure_get_name(structure);

    if (media_type.starts_with("audio/x-raw")) {
      new_pad.link(audio_sink_pad);
      std::cout << "Linked the new pad to the audio converter's sink pad.\n";
    }
  }
  /*
     If the video converter is linked, the job is already done.
     */
  if (!video_sink_pad.is_linked()) {

    std::string media_type = gst_structure_get_name(structure);

    if (media_type.starts_with("video/x-raw")) {
      new_pad.link(video_sink_pad);
      std::cout << "Linked the new pad to the video converter's sink pad.\n";
    }
  }
}
