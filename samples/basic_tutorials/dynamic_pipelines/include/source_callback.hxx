#pragma once

#include "viflow/element_callback.hxx"
#include "viflow/pipeline.hxx"

class Source_Callback : public viflow::Pad_Added_Callback {
public:
  Source_Callback() {}

  ~Source_Callback() {}

  void process_pad(const viflow::Shared_Element<> &elem, viflow::Pad &new_pad);
};
