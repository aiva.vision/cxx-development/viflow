#include <gst/gst.h>

#include <iostream>
#include <sstream>

// include gtk+3.0, libgtk-3 is needed
#include <gdk/gdk.h> // Gimp Drawing Kit
#include <gtk/gtk.h> // Gimp Tool Kit

#include "util.hxx"
#include "viflow/pipeline.hxx"

/* This function is called when the PLAY button is clicked */
static void play_cb(GtkButton *button, Context *context) {
  context->pipeline->set_state(GST_STATE_PLAYING);
}

/* This function is called when the PAUSE button is clicked */
static void pause_cb(GtkButton *button, Context *context) {
  context->pipeline->set_state(GST_STATE_PAUSED);
}

/* This function is called when the STOP button is clicked */
static void stop_cb(GtkButton *button, Context *context) {
  context->pipeline->set_state(GST_STATE_READY);
}

/* This function is called when the main window is closed */
static void delete_event_cb(GtkWidget *widget, GdkEvent *event,
                            Context *context) {
  stop_cb(NULL, context);
  gtk_main_quit();
}

/* This function is called when the slider changes its position. We perform a
 * seek to the new position here. */
static void slider_cb(GtkRange *range, Context *context) {

  gdouble value = gtk_range_get_value(GTK_RANGE(context->slider));

  std::cout << "DEBUG Simple Seek position: "
            << static_cast<gint64>(value * GST_SECOND) << "\n";

  std::cout << "DEBUG Simple Seek value & GST_SECONDS: " << value << " "
            << GST_SECOND << "\n";

  context->pipeline->seek_simple(
      GST_FORMAT_TIME,
      static_cast<GstSeekFlags>(GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT),
      static_cast<gint64>(value * GST_SECOND));
}

/* This creates all the GTK+ widgets that compose our application, and registers
 * the callbacks */
void create_ui(Context *context) {

  std::cout << "Creating main window\n";
  auto main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  g_signal_connect(G_OBJECT(main_window), "delete-event",
                   G_CALLBACK(delete_event_cb), context);

  std::cout << "Creating play button\n";
  auto play_button = gtk_button_new_from_icon_name("media-playback-start",
                                                   GTK_ICON_SIZE_SMALL_TOOLBAR);
  g_signal_connect(G_OBJECT(play_button), "clicked", G_CALLBACK(play_cb),
                   context);

  std::cout << "Creating pause button\n";
  auto pause_button = gtk_button_new_from_icon_name(
      "media-playback-pause", GTK_ICON_SIZE_SMALL_TOOLBAR);
  g_signal_connect(G_OBJECT(pause_button), "clicked", G_CALLBACK(pause_cb),
                   context);

  std::cout << "Creating stop button\n";
  auto stop_button = gtk_button_new_from_icon_name("media-playback-stop",
                                                   GTK_ICON_SIZE_SMALL_TOOLBAR);
  g_signal_connect(G_OBJECT(stop_button), "clicked", G_CALLBACK(stop_cb),
                   context);

  std::cout << "Creating slider\n";
  context->slider =
      gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 100, 1);
  gtk_scale_set_draw_value(GTK_SCALE(context->slider), 0);
  context->slider_update_signal_id =
      g_signal_connect(G_OBJECT(context->slider), "value-changed",
                       G_CALLBACK(slider_cb), context);

  context->streams_list = gtk_text_view_new();
  gtk_text_view_set_editable(GTK_TEXT_VIEW(context->streams_list), FALSE);

  std::cout << "Creating controls\n";
  auto controls = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(controls), play_button, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(controls), pause_button, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(controls), stop_button, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(controls), context->slider, TRUE, TRUE, 2);

  std::cout << "Creating hbox\n";
  auto main_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_pack_start(GTK_BOX(main_hbox), context->sink_widget, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(main_hbox), context->streams_list, FALSE, FALSE,
                     2);

  std::cout << "Creating box\n";
  auto main_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_box_pack_start(GTK_BOX(main_box), main_hbox, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(main_box), controls, FALSE, FALSE, 0);
  gtk_container_add(GTK_CONTAINER(main_window), main_box);
  gtk_window_set_default_size(GTK_WINDOW(main_window), 640, 480);

  std::cout << "Display all\n";
  gtk_widget_show_all(main_window);
}

/* This function is called periodically to refresh the GUI */
gboolean refresh_ui(Context *context) {
  /* We do not want to update anything unless we are in the PAUSED or PLAYING
   * states */
  if (context->state < GST_STATE_PAUSED)
    return TRUE;

  /* If we didn't know it yet, query the stream duration */
  if (!GST_CLOCK_TIME_IS_VALID(context->duration)) {
    try {
      context->duration = context->pipeline->query_duration(GST_FORMAT_TIME);

      /* Set the range of the slider to the clip duration, in SECONDS */
      gtk_range_set_range(GTK_RANGE(context->slider), 0,
                          static_cast<gdouble>(context->duration) / GST_SECOND);

    } catch (const viflow::Exception &err) {
      std::cerr << err.what() << "\n";
    }
  }

  try {
    auto current = context->pipeline->query_position(GST_FORMAT_TIME);
    /* Block the "value-changed" signal, so the slider_cb function is not called
      (which would trigger a seek the user has not requested) */
    g_signal_handler_block(context->slider, context->slider_update_signal_id);
    /* Set the position of the slider to the current pipeline position, in
     * SECONDS */
    gtk_range_set_value(GTK_RANGE(context->slider),
                        static_cast<gdouble>(current) / GST_SECOND);
    /* Re-enable the signal */
    g_signal_handler_unblock(context->slider, context->slider_update_signal_id);

  } catch (const viflow::Exception &err) {
    std::cerr << err.what() << "\n";
  }
  return TRUE;
}

template <typename T>
static void try_insert(const viflow::Tag_List &tags, const std::string &tag,
                       GtkTextBuffer *text) {
  try {
    auto value = tags.get<T>(tag);
    std::stringstream ss;
    ss << " " << tag << ": " << value << "\n";

    gtk_text_buffer_insert_at_cursor(text, ss.str().c_str(), -1);

  } catch (const std::exception &e) {
    std::cout << " " << tag << ": unknown, " << e.what() << "\n";
  }
}

/* Extract metadata from all the streams and write it to the text widget in the
 * GUI */
void analyze_streams(Context *context) {
  // guint rate;
  GtkTextBuffer *text;

  /* Clean current contents of the widget */
  text = gtk_text_view_get_buffer(GTK_TEXT_VIEW(context->streams_list));
  gtk_text_buffer_set_text(text, "", -1);

  /* Read some properties */
  int n_video = context->pipeline->get_property<int>("n-video");
  int n_audio = context->pipeline->get_property<int>("n-audio");
  int n_text = context->pipeline->get_property<int>("n-text");

  if (context->pipeline->get_state().current != GST_STATE_PLAYING) {
    return;
  }

  for (int i = 0; i < n_video; i++) {
    std::cout << "video stream " << i << ":\n";

    auto tags = context->pipeline->get_video_tags(i);

    try_insert<std::string>(tags, GST_TAG_VIDEO_CODEC, text);
  }

  for (int i = 0; i < n_audio; i++) {
    std::cout << "audio stream " << i << ":\n";

    auto tags = context->pipeline->get_audio_tags(i);

    try_insert<std::string>(tags, GST_TAG_AUDIO_CODEC, text);
    try_insert<std::string>(tags, GST_TAG_LANGUAGE_CODE, text);
    try_insert<uint>(tags, GST_TAG_BITRATE, text);
  }

  for (int i = 0; i < n_text; i++) {
    std::cout << "subtitle stream " << i << ":\n";

    auto tags = context->pipeline->get_text_tags(i);

    try_insert<std::string>(tags, GST_TAG_LANGUAGE_CODE, text);
  }
}
