// https://gstreamer.freedesktop.org/documentation/tutorials/basic/toolkit-integration.html?gi-language=c
#include <gst/gst.h>
#include <iostream>

// include gtk+3.0, libgtk-3 is needed
#include <gdk/gdk.h> // Gimp Drawing Kit
#include <gtk/gtk.h> // Gimp Tool Kit

#include "callback.hxx"
#include "gimp_utils.hxx"
#include "util.hxx"

static void stop(int signal_number) {
  /*
     Send EOS and let the pipeline stop cleanly.
     */
  viflow::Element::instance("pipeline")->send_event(gst_event_new_eos());
}

int main(int argc, char *argv[]) {
  /*
     Register CTRL-C handler.
     */
  signal(SIGINT, stop);

  /*
     Initialize GTK
     */
  gtk_init(&argc, &argv);

  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);

  /*
     Initialize Context
     */
  auto context = std::make_shared<Context>();

  /*
     Creating Elements
     */
  context->pipeline =
      viflow::Element::register_instance<viflow::Playbin>("playbin");

  /*
     Here we create the GTK Sink element which will provide us with a GTK widget
     where GStreamer will render the video at and we can add to our UI. Try to
     create the OpenGL version of the video sink, and fallback if that fails
     */
  try {
    auto videosink =
        viflow::Element::register_instance("glsinkbin", "videosink");
    auto gtkglsink =
        viflow::Element::register_instance("gtkglsink", "gtkglsink");

    std::cout << "Successfully created GTK GL Sink\n";

    videosink->set_property("sink", gtkglsink->get_core());
    context->sink_widget = gtkglsink->get_property<GtkWidget *>("widget");

  } catch (const viflow::Exception &err) {

    std::cerr << "Could not create gtkglsink, falling back to gtksink.\n";

    auto videosink = viflow::Element::register_instance("gtksink", "videosink");
    context->sink_widget = videosink->get_property<GtkWidget *>("widget");
  }
  /*
     Set the URI.
     */
  context->pipeline->set_property(
      viflow::Playbin_Properties::uri,
      "https://gstreamer.freedesktop.org/data/media/"
      "sintel_trailer-480p.webm");
  /*
     Set the video-sink
     */
  context->pipeline->set_property(
      viflow::Playbin_Properties::video_sink,
      viflow::Element::instance("videosink")->get_core());

  /*
     Connect to interesting signals in playbin
     */
  context->pipeline->connect_callback(std::make_unique<Video_Tags_Cb>());
  context->pipeline->connect_callback(std::make_unique<Audio_Tags_Cb>());
  context->pipeline->connect_callback(std::make_unique<Text_Tags_Cb>());

  create_ui(context.get());

  /*
     Instruct the bus to emit signals for each received message, and connect to
     the interesting signals.
     */
  auto &bus = context->pipeline->get_bus();
  bus->add_signal_watch(std::make_unique<Error_Callback>(), "message::error");
  bus->add_signal_watch(std::make_unique<EOS_Callback>(), "message::eos");
  bus->add_signal_watch(std::make_unique<State_Change_Callback>(context),
                        "message::state-changed");
  bus->add_signal_watch(std::make_unique<Application_Callback>(context),
                        "message::application");

  /*
     Start playing
     */
  context->pipeline->play();

  /*
     Register a function that GLib will call every second
     */
  g_timeout_add_seconds(1, (GSourceFunc)refresh_ui, context.get());

  /*
     Start the GTK main loop. We will not regain control until gtk_main_quit is
     called.
     */
  gtk_main();

  context->pipeline->stop();
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
