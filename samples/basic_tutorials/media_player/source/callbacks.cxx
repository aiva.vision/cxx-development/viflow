#include <iostream>

#include <gst/gst.h>

// include gtk+3.0, libgtk-3 is needed
#include <gtk/gtk.h> // Gimp Tool Kit

#include "callback.hxx"
#include "gimp_utils.hxx"

void post_application_message(const viflow::Shared_Element<> &elem,
                              const std::string &name) {

  auto message = gst_message_new_application(
      GST_OBJECT(elem->get_core()), gst_structure_new_empty(name.c_str()));

  elem->post_message(message);
}

void Error_Callback::process_message(const viflow::Bus &bus,
                                     const viflow::Message &message) {

  auto context = message.parse_error();
  std::cerr << "Received an error from element " << message.get_source_name()
            << " with content \"" << context.error.message()
            << "\" and with debug info: \"" << context.debug.get() << "\""
            << std::endl;

  /* Set the pipeline to READY (which stops playback) */
  auto pipeline = viflow::Element::instance("playbin");
  pipeline->set_state(GST_STATE_READY);
}

/*
   This function is called when an End-Of-Stream message is posted on the bus.
   We just set the pipeline to READY (which stops playback)
   */
void EOS_Callback::process_message(const viflow::Bus &bus,
                                   const viflow::Message &message) {
  std::cout << "End-Of-Stream reached.\n";

  auto pipeline = viflow::Element::instance("playbin");
  pipeline->set_state(GST_STATE_READY);
}

/*
   This function is called when the pipeline changes states. We use it to keep
   track of the current state.
   */
void State_Change_Callback::process_message(const viflow::Bus &bus,
                                            const viflow::Message &message) {
  auto state = message.parse_state_changed(); //! WRITE DOWN
  if (message.get_source_name() == "playbin") {

    context->state = state.new_state;
    std::cout << "State set to " << gst_element_state_get_name(context->state)
              << "\n";
    if (state.new_state == GST_STATE_PAUSED &&
        state.old_state == GST_STATE_READY) {
      /*
         For extra responsiveness, we refresh the GUI as soon as we reach the
         PAUSED state
         */
      refresh_ui(context.get());
    }
  }
}

/*
   This function is called when an "application" message is posted on the bus.
   Here we retrieve the message posted by the tags_cb callback
   */
void Application_Callback::process_message(const viflow::Bus &bus,
                                           const viflow::Message &message) {
  if (g_strcmp0(gst_structure_get_name(message.get_structure()),
                "tags-changed") == 0) {
    /*
       If the message is the "tags-changed" (only one we are currently issuing),
       update the stream info GUI
       */
    analyze_streams(context.get());
  }
}
