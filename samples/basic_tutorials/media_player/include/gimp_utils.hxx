#pragma once

#include "util.hxx"
#include "viflow/pipeline.hxx"
#include "viflow/tag_list.hxx"

gboolean refresh_ui(Context *context);

void analyze_streams(Context *context);

void create_ui(Context *context);