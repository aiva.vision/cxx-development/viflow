#pragma once

#include "util.hxx"
#include "viflow/element_callback.hxx"
#include "viflow/pipeline.hxx"
#include "viflow/playbin_callback.hxx"

void post_application_message(const viflow::Shared_Element<> &elem,
                              const std::string &name);

class Video_Tags_Cb : public viflow::Video_Tags_Changed_Callback {
public:
  void process(const viflow::Shared_Element<> &playbin, int stream) override {
    post_application_message(playbin, "tags-changed");
  }
};

class Audio_Tags_Cb : public viflow::Audio_Tags_Changed_Callback {
public:
  void process(const viflow::Shared_Element<> &playbin, int stream) override {
    post_application_message(playbin, "tags-changed");
  }
};

class Text_Tags_Cb : public viflow::Text_Tags_Changed_Callback {
public:
  void process(const viflow::Shared_Element<> &playbin, int stream) override {
    post_application_message(playbin, "tags-changed");
  }
};

class Error_Callback : public viflow::Bus_Signal_Watch {
public:
  void process_message(const viflow::Bus &bus,
                       const viflow::Message &message) override;
};

class EOS_Callback : public viflow::Bus_Signal_Watch {
public:
  void process_message(const viflow::Bus &bus,
                       const viflow::Message &message) override;
};

class State_Change_Callback : public viflow::Bus_Signal_Watch {
public:
  State_Change_Callback(std::shared_ptr<Context> &c) : context{c} {}

  void process_message(const viflow::Bus &bus,
                       const viflow::Message &message) override;

private:
  std::shared_ptr<Context> context;
};

class Application_Callback : public viflow::Bus_Signal_Watch {
public:
  Application_Callback(const std::shared_ptr<Context> &c) : context{c} {}

  void process_message(const viflow::Bus &bus,
                       const viflow::Message &message) override;

private:
  std::shared_ptr<Context> context;
};
