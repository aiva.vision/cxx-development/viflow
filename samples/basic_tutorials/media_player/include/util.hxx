#pragma once

#include <gst/gst.h>

#include "viflow/pipeline.hxx"
#include "viflow/playbin.hxx"

// include gtk+3.0, libgtk-3 is needed
#include <gtk/gtk.h>  // Gimp Tool Kit

struct Context {
  viflow::Shared_Element<viflow::Playbin>
      pipeline; /* Our one and only pipeline */

  GtkWidget *sink_widget{
      nullptr}; /* The widget where our video will be displayed */
  GtkWidget *slider{
      nullptr}; /* Slider widget to keep track of current position */
  GtkWidget *streams_list{
      nullptr}; /* Text widget to display info about the streams */
  gulong slider_update_signal_id{
      0}; /* Signal ID for the slider update signal */

  GstState state{GST_STATE_NULL}; /* Current state of the pipeline */
  guint64 duration{
      GST_CLOCK_TIME_NONE}; /* Duration of the clip, in nanoseconds */
};
