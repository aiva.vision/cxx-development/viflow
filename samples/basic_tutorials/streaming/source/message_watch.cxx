#include <iostream>

#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "message_watch.hxx"

unsigned int g_chunk_size = 1024;

unsigned int g_sample_rate = 44100;

Message_Watch::Message_Watch(bool live) : is_live{live} {}

void Message_Watch::process_message(const viflow::Bus &bus,
                                    const viflow::Message &message) {

  auto pipeline = viflow::Element::instance("pipeline");
  auto &loop = viflow::Main_Loop::instance();

  switch (message.get_type()) {
  case GST_MESSAGE_ERROR: {
    auto context = message.parse_error();
    std::cerr << "Received an error from element " << message.get_source_name()
              << " with content \"" << context.error.message()
              << "\" and with debug info: \"" << context.debug.get() << "\""
              << std::endl;
    pipeline->stop();
    loop->quit();
    break;
  }
  case GST_MESSAGE_EOS:
    std::cerr << "End-Of-Stream reached.\n";
    pipeline->stop();
    loop->quit();
    break;
  case GST_MESSAGE_BUFFERING: {
    /*
       Buffering does not make sense on live streams.
       */
    if (is_live) {
      break;
    }
    auto percent = message.parse_buffering();
    g_print("Buffering (%3d%%)\r", percent);
    if (percent < 100) {
      /*
         Wait until buffering is complete.
         */
      pipeline->pause();
    } else {
      /*
         Resume playing.
         */
      pipeline->play();
    }
    break;
  }
  case GST_MESSAGE_CLOCK_LOST:
    std::cerr << "The clock has been lost.\n";
    pipeline->pause();
    pipeline->play();
    break;
  default:
    /*
       Ignore, we are getting every message type.
       */
    break;
  }
}
