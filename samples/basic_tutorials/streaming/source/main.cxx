#include <gst/audio/audio.h>
#include <iostream>

#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "message_watch.hxx"

void stop(int signal_number) {
  /*
     Stop the pipeline.
     */
  viflow::Element::instance("pipeline")->stop();
  /*
     Quit from the main loop.
     */
  auto &loop = viflow::Main_Loop::instance();
  loop->quit();
}

int main(int argc, char *argv[]) {
  /*
     Register CTRL-C handler.
     */
  signal(SIGINT, stop);
  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);
  /*
     Create the pipeline.
     */
  auto pipeline = viflow::parse_launch(
      "pipeline",
      "playbin "
      "uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/"
      "sintel_trailer-480p.webm");
  /*
     Change the pipeline's state.
     */
  auto ret = pipeline->play();
  bool live = false;
  if (ret == GST_STATE_CHANGE_NO_PREROLL) {
    live = true;
  }
  /*
     Retrieve the bus.
     */
  auto &bus = pipeline->get_bus();
  /*
     Register a signal watch for the bus.
     */
  bus->add_signal_watch(std::make_unique<Message_Watch>(live), "message");
  /*
     Create a Main Loop and set it to run.
     */
  auto &loop = viflow::Main_Loop::register_instance();
  loop->run();
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
