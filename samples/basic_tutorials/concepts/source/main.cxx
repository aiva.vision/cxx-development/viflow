#include <gst/gst.h>
#include <gst/gstelement.h>
#include <iostream>

#include "viflow/pipeline.hxx"

int main(int argc, char *argv[]) {
  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);
  /*
     Create the pipeline.
     */
  auto pipeline =
      viflow::Element::register_instance<viflow::Pipeline>("pipeline");
  /*
     Add the elements to the pipeline.
     */
  pipeline->add("videotestsrc", "source");
  /*
     Appending an element also performs linking.
     */
  pipeline->append("vertigotv", "filter");
  pipeline->append("videoconvert", "convert");
  pipeline->append("autovideosink", "sink");
  /*
     Retrieve the source element.
     */
  auto source = pipeline->child("source");
  /*
     Modify the source's properties.
     */
  source->set_property("pattern", 0);
  /*
     Change the pipeline's state.
     */
  pipeline->play();
  /*
     Retrieve the bus.
     */
  auto &bus = pipeline->get_bus();
  /*
     Properly cast the message type after binary OR.
     */
  auto message_type =
      static_cast<GstMessageType>(GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
  /*
     Block until EOS is received.
     */
  auto message = bus->timed_pop_filtered(GST_CLOCK_TIME_NONE, message_type);

  switch (message.get_type()) {
  case GST_MESSAGE_ERROR: {
    auto context = message.parse_error();
    std::cerr << "Received an error from element " << message.get_source_name()
              << " with content \"" << context.error.message()
              << "\" and with debug info: \"" << context.debug.get() << "\""
              << std::endl;
    break;
  }
  case GST_MESSAGE_EOS:
    std::cerr << "End-Of-Stream reached.\n";
    break;
  default:
    /* We should not reach here because we only asked for ERRORs and EOS */
    std::cerr << "Unexpected message received.\n";
    break;
  }
  /*
     Allow the pipeline to free its resources.
     */
  pipeline->stop();
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
