#include <gst/gst.h>
#include <gst/gstelement.h>
#include <iostream>

#include "viflow/pipeline.hxx"
#include "viflow/tee.hxx"

int main(int argc, char *argv[]) {
  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);
  /*
     Create the pipeline.
     */
  auto pipeline =
      viflow::Element::register_instance<viflow::Pipeline>("viflow");
  /*
     Add the audio elements to the pipeline.
     */
  pipeline->add("audiotestsrc", "audio_source");
  pipeline->append<viflow::Tee>("tee");
  pipeline->add("queue", "audio_queue");
  pipeline->append("audioconvert", "audio_convert");
  pipeline->append("audioresample", "audio_resample");
  pipeline->append("autoaudiosink", "audio_sink");
  /*
     Add the video elements to the pipeline.
     */
  pipeline->add("queue", "video_queue");
  pipeline->append("wavescope", "visual");
  pipeline->append("videoconvert", "csp");
  pipeline->append("autovideosink", "video_sink");
  /*
     Configure elements.
     */
  auto audio_source = pipeline->child("audio_source");
  audio_source->set_property("freq", 215.0f);

  auto visual = pipeline->child("visual");
  visual->set_property("shader", 0);
  visual->set_property("style", 1);
  /*
     Manually link the Tee, which has Request pads.
     */
  pipeline->link_elements("tee", "audio_queue");
  pipeline->link_elements("tee", "video_queue");
  /*
     Change the pipeline's state.
     */
  pipeline->play();
  /*
     Retrieve the bus.
     */
  auto &bus = pipeline->get_bus();
  /*
     Properly cast the message type after binary OR.
     */
  auto message_type =
      static_cast<GstMessageType>(GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
  /*
     Block until EOS is received.
     */
  auto message = bus->timed_pop_filtered(GST_CLOCK_TIME_NONE, message_type);
  /*
     Allow the pipeline to free its resources.
     */
  pipeline->stop();
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
