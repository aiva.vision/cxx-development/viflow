#pragma once

#include "viflow/message.hxx"

class Message_Handler {
public:
  Message_Handler();

  ~Message_Handler() {}
  /*
     Parses the bus message.
     */
  void handle_message(viflow::Message &message);

  bool should_terminate();
  bool can_seek();
  long get_duration();

private:
  bool terminate;
  bool seek_enabled;
  long duration;
};
