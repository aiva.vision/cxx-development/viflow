#include <iostream>

#include "viflow/element.hxx"
#include "viflow/query.hxx"

#include "message_handler.hxx"

Message_Handler::Message_Handler()
    : terminate{false}, seek_enabled{false}, duration{-1} {}

void Message_Handler::handle_message(viflow::Message &message) {

  switch (message.get_type()) {
  case GST_MESSAGE_ERROR: {
    auto context = message.parse_error();
    std::cerr << "Received an error from element " << message.get_source_name()
              << " with content \"" << context.error.message()
              << "\" and with debug info: \"" << context.debug.get() << "\""
              << std::endl;
    terminate = true;
    break;
  }
  case GST_MESSAGE_EOS:
    std::cerr << "End-Of-Stream reached.\n";
    terminate = true;
    break;
  case GST_MESSAGE_DURATION_CHANGED: {
    std::cerr << "The duration has changed.\n";
    auto playbin = viflow::Element::instance("playbin");
    duration = playbin->query_duration(GST_FORMAT_TIME);
    break;
  }
  case GST_MESSAGE_STATE_CHANGED: {
    auto playbin = viflow::Element::instance("playbin");
    auto context = message.parse_state_changed();

    if (playbin->get_name() == message.get_source_name()) {
      std::cout << "Playbin state has changed from "
                << viflow::to_string(context.old_state) << " to "
                << viflow::to_string(context.new_state) << std::endl;
      /*
         Query if seeking is allowed.
         */
      if (context.new_state == GST_STATE_PLAYING) {
        viflow::Seeking_Query query(GST_FORMAT_TIME);

        if (query.query_element(playbin)) {

          auto query_context = query.parse_seeking();
          seek_enabled = query_context.seekable;

          if (seek_enabled) {

            g_print("Seeking is enabled from %" GST_TIME_FORMAT
                    " to %" GST_TIME_FORMAT "\n",
                    GST_TIME_ARGS(query_context.start_segment),
                    GST_TIME_ARGS(query_context.end_segment));
          } else {
            std::cerr << "Seeking is disabled for this stream.\n";
          }
        } else {
          std::cerr << "Seeking query failed\n";
        }
      }
    }
    break;
  }
  default:
    /* We should not reach here because we only asked for ERRORs and EOS */
    std::cerr << "Unexpected message received.\n";
    terminate = true;
    break;
  }
}

bool Message_Handler::should_terminate() { return terminate; }

bool Message_Handler::can_seek() { return seek_enabled; }

long Message_Handler::get_duration() { return duration; }
