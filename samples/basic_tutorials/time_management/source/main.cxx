#include <gst/gst.h>
#include <gst/gstelement.h>
#include <iostream>

#include "message_handler.hxx"
#include "viflow/pipeline.hxx"
#include "viflow/playbin.hxx"

void stop(int signal_number) { viflow::Element::instance("playbin")->stop(); }

int main(int argc, char *argv[]) {
  /*
     Register CTRL-C handler.
     */
  signal(SIGINT, stop);
  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);
  /*
     Create the pipeline.
     */
  auto playbin = viflow::Element::register_instance<viflow::Playbin>("playbin");
  /*
     Set the URI.
     */
  playbin->set_property(viflow::Playbin_Properties::uri,
                        "https://www.freedesktop.org/software/gstreamer-sdk/"
                        "data/media/sintel_trailer-480p.webm");
  /*
     Change the pipeline's state.
     */
  playbin->play();
  /*
     Retrieve the bus.
     */
  auto &bus = playbin->get_bus();
  /*
     Properly cast the message type after binary OR.
     */
  auto message_type = static_cast<GstMessageType>(
      GST_MESSAGE_STATE_CHANGED | GST_MESSAGE_ERROR | GST_MESSAGE_EOS |
      GST_MESSAGE_DURATION_CHANGED);

  bool already_seeked = false;
  Message_Handler handler{};
  /*
     Listen to the bus.
     */
  do {
    auto state = playbin->get_state();
    /*
       Break out of the loop if the pipeline has stopped.
       */
    if (state.current == GST_STATE_NULL) {
      break;
    }

    auto message = bus->timed_pop_filtered(100 * GST_MSECOND, message_type);

    if (!message.empty()) {
      handler.handle_message(message);
    } else {
      /*
         The timeout expired.
         */
      if (state.current != GST_STATE_PLAYING) {
        continue;
      }
      /*
         Get the current position.
         */
      auto position = playbin->query_position(GST_FORMAT_TIME);

      /*
         Print current position and total duration.
         */
      g_print("Position %" GST_TIME_FORMAT " / %" GST_TIME_FORMAT "\r",
              GST_TIME_ARGS(position), GST_TIME_ARGS(handler.get_duration()));
      /*
         Seek if we are allowed to, and the time is right.
         */
      if (handler.can_seek() && !already_seeked && position > 10 * GST_SECOND) {

        std::cout << "\nReached 10s, performing seek.\n";
        auto seek_flags = static_cast<GstSeekFlags>(GST_SEEK_FLAG_FLUSH |
                                                    GST_SEEK_FLAG_KEY_UNIT);
        if (playbin->seek_simple(GST_FORMAT_TIME, seek_flags,
                                 30 * GST_SECOND)) {
          std::cout << "Performed seek successfully.\n";
          already_seeked = true;
        }
      }
    }
  } while (!handler.should_terminate());
  /*
     Stop the playbin and let ViFLow free the resources.
     */
  playbin->stop();
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
