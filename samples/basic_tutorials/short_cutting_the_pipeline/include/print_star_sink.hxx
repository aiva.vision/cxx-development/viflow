#include "viflow/simple_app_sink.hxx"

class Print_Start_Sink : public viflow::Simple_App_Sink {
public:
  GstFlowReturn process(viflow::Sample &sample);

protected:
  Print_Start_Sink(const std::string &name);

  friend class viflow::Element;
};
