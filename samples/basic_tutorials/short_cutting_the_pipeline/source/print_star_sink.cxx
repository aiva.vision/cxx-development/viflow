#include <iostream>

#include "print_star_sink.hxx"

Print_Start_Sink::Print_Start_Sink(const std::string &name)
    : viflow::Simple_App_Sink{name} {}

GstFlowReturn Print_Start_Sink::process(viflow::Sample &sample) {
  /*
     Print a * to indicate a received buffer.
     */
  if (!sample.is_empty()) {
    /*
       Then the sample contains a buffer.
       */
    std::cout << "*" << std::endl;
    return GST_FLOW_OK;
  }

  return GST_FLOW_ERROR;
}
