#include <gst/audio/audio.h>
#include <iostream>

#include "viflow/app_sink.hxx"
#include "viflow/app_source.hxx"
#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"
#include "viflow/tee.hxx"

#include "callbacks.hxx"
#include "print_star_sink.hxx"

void stop(int signal_number) {
  /*
     Stop the pipeline.
     */
  viflow::Element::instance("pipeline")->stop();
  /*
     Quit from the main loop.
     */
  auto &loop = viflow::Main_Loop::instance();
  loop->quit();
}

int main(int argc, char *argv[]) {
  /*
     Register CTRL-C handler.
     */
  signal(SIGINT, stop);
  /*
     Initialize GStreamer
     */
  gst_init(&argc, &argv);
  /*
     Initialize the context.
     */
  Context context;
  /*
     Create the pipeline.
     */
  auto pipeline =
      viflow::Element::register_instance<viflow::Pipeline>("pipeline");
  /*
     Add the appsink and appsrc custom elements.
     */
  auto app_source = pipeline->add<viflow::App_Source>("app_source");
  auto tee = pipeline->append<viflow::Tee>("tee");
  /*
     Add the audio elements to the pipeline.
     */
  pipeline->add("queue", "audio_queue");
  pipeline->append("audioconvert", "audio_convert1");
  pipeline->append("audioresample", "audio_resample");
  pipeline->append("autoaudiosink", "audio_sink");
  /*
     Add the video elements to the pipeline.
     */
  pipeline->add("queue", "video_queue");
  pipeline->append("audioconvert", "audio_convert2");
  pipeline->append("wavescope", "visual");
  pipeline->append("videoconvert", "video_convert");
  pipeline->append("autovideosink", "video_sink");
  pipeline->add("queue", "app_queue");
  pipeline->append<Print_Start_Sink>("print_star_app_sink");
  /*
     Configure the visual element.
     */
  auto visual = pipeline->child("visual");
  visual->set_property("shader", 0);
  visual->set_property("style", 1);
  /*
     Build the audio capabilities.
     */
  GstAudioInfo info;
  gst_audio_info_set_format(&info, GST_AUDIO_FORMAT_S16, g_sample_rate, 1,
                            NULL);
  viflow::Capabilities audio_caps(gst_audio_info_to_caps(&info));
  /*
     Configure the app_source element.
     */
  app_source->set_property("format", GST_FORMAT_TIME);
  app_source->set_property("caps", audio_caps.get_core());
  /*
     Connect callbacks to the app source.
     */
  app_source->connect_callback(
      std::make_unique<Stop_Feed_Callback>(&context));
  app_source->connect_callback(
      std::make_unique<Start_Feed_Callback>(&context));
  /*
     Manually link the Tee, which has Request pads.
     */
  pipeline->link_elements("tee", "audio_queue");
  pipeline->link_elements("tee", "video_queue");
  pipeline->link_elements("tee", "app_queue");
  /*
     Retrieve the bus.
     */
  auto &bus = pipeline->get_bus();
  /*
     Register a signal watch for the bus.
     */
  bus->add_signal_watch(std::make_unique<Error_Callback>(), "message::error");
  /*
     Change the pipeline's state.
     */
  pipeline->play();
  /*
     Create a Main Loop and set it to run.
     */
  auto &loop = viflow::Main_Loop::register_instance();
  loop->run();
  /*
     Guess what, no cleanup is required.
     */
  return 0;
}
