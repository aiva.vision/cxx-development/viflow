#include <iostream>

#include "viflow/app_sink.hxx"
#include "viflow/app_source.hxx"
#include "viflow/buffer.hxx"
#include "viflow/exceptions.hxx"
#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "callbacks.hxx"

unsigned int g_chunk_size = 1024;

unsigned int g_sample_rate = 44100;

void Error_Callback::process_message(const viflow::Bus &bus,
                                     const viflow::Message &message) {

  auto context = message.parse_error();
  std::cerr << "Received an error from element " << message.get_source_name()
            << " with content \"" << context.error.message()
            << "\" and with debug info: \"" << context.debug.get() << "\""
            << std::endl;

  auto pipeline = viflow::Element::instance("pipeline");
  auto &loop = viflow::Main_Loop::instance();

  pipeline->stop();
  loop->quit();
}

Stop_Feed_Callback::Stop_Feed_Callback(Context *ctx) : context{ctx} {}

void Stop_Feed_Callback::process(const viflow::Shared_Element<> &app_source) {

  if (context->sourceid != 0) {
    std::cout << "Stop feeding\n";
    g_source_remove(context->sourceid);
    context->sourceid = 0;
  }
}

/*
   This method is called by the idle GSource in the mainloop, to feed CHUNK_SIZE
   bytes into appsrc. The idle handler is added to the mainloop when appsrc
   requests us to start sending data (need-data signal) and is removed when
   appsrc has enough data (enough-data signal).
   */
static gboolean push_data(Context *context) {
  /*
     Because each sample is 16 bits.
     */
  gint num_samples = g_chunk_size / 2;
  /*
     Create a new empty buffer
     */
  viflow::Buffer buffer(g_chunk_size);
  /*
     Set its timestamp and duration
     */
  buffer.set_timestamp(gst_util_uint64_scale(context->generated_samples_count,
                                             GST_SECOND, g_sample_rate));
  buffer.set_duration(
      gst_util_uint64_scale(num_samples, GST_SECOND, g_sample_rate));
  {
    /*
       Generate some psychodelic waveforms
       */
    auto map = buffer.map(GST_MAP_WRITE);
    auto raw = map.data<gint16>();

    context->c += context->d;
    context->d -= context->c / 1000;
    gfloat freq = 1100 + 1000 * context->d;
    for (int i = 0; i < num_samples; i++) {
      context->a += context->b;
      context->b -= context->a / freq;
      raw[i] = (gint16)(500 * context->a);
    }
  }
  context->generated_samples_count += num_samples;
  /*
     Retrieve the appsrc element.
     */
  auto app_source = viflow::Element::instance<viflow::App_Source>("app_source");
  /*
     Push the buffer into the appsrc.
     */
  if (app_source->push_buffer(std::move(buffer)) != GST_FLOW_OK) {
    /*
       We got some error, stop sending data.
       */
    return false;
  }
  return true;
}

Start_Feed_Callback::Start_Feed_Callback(Context *ctx) : context{ctx} {}

void Start_Feed_Callback::process(const viflow::Shared_Element<> &app_source,
                                  unsigned int length) {

  if (context->sourceid == 0) {
    std::cout << "Start feeding\n";
    context->sourceid =
        g_idle_add(reinterpret_cast<GSourceFunc>(push_data), context);
  }
}
