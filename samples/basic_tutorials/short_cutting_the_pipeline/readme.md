# Short-cutting the pipeline

This sample reimplements the basic tutorial 8 from the GStreamer [basic tutorials](https://gstreamer.freedesktop.org/documentation/tutorials/basic/index.html).

Note how the boilerplate is significantly reduced and the cleanup is completely abstracted away.
