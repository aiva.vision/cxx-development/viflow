#include <iostream>

#include "utils.hxx"
#include "viflow/pipeline.hxx"

void stop(int signal_number)
{
    /*
    Stop the pipeline.
    */
    viflow::Element::instance("pipeline")->stop();
}

int main(int argc, char *argv[])
{
    /*
    Register CTRL-C handler.
    */
    signal(SIGINT, stop);

    /*
    Initialize GStreamer
    */
    gst_init(&argc, &argv);

    /* 
    Get a list of all visualization plugins 
    */
    auto feature_list = gst_registry_feature_filter (gst_registry_get (), filter_vis_features, FALSE, NULL);

    GstElementFactory *selected_factory {nullptr};

    /*
    Print their names
    */
    std::cout<<"Availabe visualization plugins:\n";
    for (auto walk = feature_list; walk != NULL; walk = g_list_next (walk)) {
        auto factory = GST_ELEMENT_FACTORY (walk->data);
        auto name = gst_element_factory_get_longname (factory);
        std::cout << "\t" << name << "\n";

        if (selected_factory == NULL || g_str_has_prefix (name, "GOOM")) {
            selected_factory = factory;
        }
    }

    /*
    Don't use the factory if it's still empty
    e.g. no visualization plugin found
    */
    if(!selected_factory)
    {
        std::cout << "No visualization plugin found!\n";
        return -1;
    }

    /*
    We have no selected a factory for the visualization element
    */
    std::cout << "Selected " << gst_element_factory_get_longname (selected_factory) << "\n";
    auto vis_plugin = gst_element_factory_create (selected_factory, NULL);
    if(!vis_plugin) return -1;

    /*
    Build the pipeline
    */
    auto pipeline = viflow::parse_launch("pipeline",
                                         "playbin "
                                         "uri=http://radio.hbr1.com:19800/ambient.ogg");

    /*
    Set the visualization flag
    */
    int GST_PLAY_FLAG_VIS = (1 << 3);
    auto flags = pipeline->get_property<unsigned int>("flags");
    flags |= GST_PLAY_FLAG_VIS;
    pipeline->set_property("flags", flags);

    /*
    Set vis plugin for playbin
    */
    pipeline->set_property("vis-plugin", vis_plugin);

    /*
    Start playing 
    */
    pipeline->play();

    /*
     Retrieve the bus.
    */
    auto &bus = pipeline->get_bus();

    /*
    Properly cast the message type after binary OR.
    */
    auto message_type =
      static_cast<GstMessageType>(GST_MESSAGE_ERROR | GST_MESSAGE_EOS);

    /*
    Block until EOS is received.
    */
    auto message = bus->timed_pop_filtered(GST_CLOCK_TIME_NONE, message_type);

    /*
    Allow the pipeline to free its resources.
    */
    pipeline->stop();
    gst_plugin_feature_list_free (feature_list);
    
    return 0;
}