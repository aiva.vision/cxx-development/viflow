#include "utils.hxx"

gboolean filter_vis_features (GstPluginFeature *feature, gpointer data) {
  if (!GST_IS_ELEMENT_FACTORY (feature))
    return FALSE;
  auto factory = GST_ELEMENT_FACTORY (feature);
  if (!g_strrstr (gst_element_factory_get_klass (factory), "Visualization"))
    return FALSE;

  return TRUE;
}