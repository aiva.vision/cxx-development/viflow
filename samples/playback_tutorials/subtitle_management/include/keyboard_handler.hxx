#pragma once

#include <gst/gst.h>

#include "utils.hxx"
#include "viflow/event.hxx"
#include "viflow/io_channel.hxx"
#include "viflow/io_channel_watch.hxx"
#include "viflow/pipeline.hxx"

class Keyboard_Handler : public viflow::IO_Channel_Watch {
public:
  Keyboard_Handler(const std::shared_ptr<Context> &c) : context{c} {};

  ~Keyboard_Handler() {}

  gboolean process(const viflow::Shared_IO_Channel &channel,
                   GIOCondition condition) override;

  void send_seek_event();

  viflow::Event create_event(float rate);

private:
  float rate{1.0};
  bool playing{true};
  std::shared_ptr<Context> context;
};
