# Short-cutting the pipeline

This sample reimplements the playback tutorial 3 from the GStreamer [playback tutorials](https://gstreamer.freedesktop.org/documentation/tutorials/playback/playbin-usage.html?gi-language=c).

Note how the boilerplate is significantly reduced and the cleanup is completely abstracted away.
