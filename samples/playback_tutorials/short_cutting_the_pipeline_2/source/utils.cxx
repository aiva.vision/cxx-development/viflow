#include "utils.hxx"

gboolean push_data(Context *context)
{
  /*
     Because each sample is 16 bits.
     */
  gint num_samples = CHUNK_SIZE / 2;
  /*
     Create a new empty buffer
     */
  viflow::Buffer buffer(CHUNK_SIZE);
  /*
     Set its timestamp and duration
     */
  buffer.set_timestamp(gst_util_uint64_scale(context->generated_samples_count,
                                             GST_SECOND, SAMPLE_RATE));
  buffer.set_duration(
      gst_util_uint64_scale(num_samples, GST_SECOND, SAMPLE_RATE));
  {
    /*
       Generate some psychodelic waveforms
       */
    auto map = buffer.map(GST_MAP_WRITE);
    auto raw = map.data<gint16>();

    context->c += context->d;
    context->d -= context->c / 1000;
    gfloat freq = 1100 + 1000 * context->d;
    for (int i = 0; i < num_samples; i++)
    {
      context->a += context->b;
      context->b -= context->a / freq;
      raw[i] = (gint16)(500 * context->a);
    }
  }
  context->generated_samples_count += num_samples;
  /*
     Retrieve the appsrc element.
     */
  auto app_source = viflow::Element::instance<viflow::App_Source>("app_source");
  /*
     Push the buffer into the appsrc.
     */
  if (app_source->push_buffer(std::move(buffer)) != GST_FLOW_OK)
  {
    /*
       We got some error, stop sending data.
       */
    return false;
  }
  return true;
}

/* This signal callback triggers when appsrc needs data. Here, we add an idle handler
  to the mainloop to start pushing data into the appsrc */
static void start_feed(GstElement *source, guint size, Context *context)
{
  if (context->sourceid == 0)
  {
    g_print("Start feeding\n");
    context->sourceid = g_idle_add((GSourceFunc)push_data, context);
  }
}

/* This callback triggers when appsrc has enough data and we can stop sending.
 * We remove the idle handler from the mainloop */
static void stop_feed(GstElement *source, Context *context)
{
  if (context->sourceid != 0)
  {
    g_print("Stop feeding\n");
    g_source_remove(context->sourceid);
    context->sourceid = 0;
  }
}

void source_setup(GstElement *pipeline, GstElement *source, std::shared_ptr<Context> context)
{
  g_print("Source has been created. Configuring.\n");

  /* Configure appsrc */
  GstAudioInfo info;
  gst_audio_info_set_format(&info, GST_AUDIO_FORMAT_S16, SAMPLE_RATE, 1, NULL);

  auto audio_caps = gst_audio_info_to_caps(&info);
  g_object_set(source, "caps", audio_caps, "format", GST_FORMAT_TIME, NULL);
  
  g_signal_connect(source, "need-data", G_CALLBACK(start_feed), &context);
  g_signal_connect(source, "enough-data", G_CALLBACK(stop_feed), &context);
  
  gst_caps_unref(audio_caps);
}