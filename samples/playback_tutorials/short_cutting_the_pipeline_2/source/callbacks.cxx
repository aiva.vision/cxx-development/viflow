#include <iostream>

#include "viflow/app_sink.hxx"
#include "viflow/app_source.hxx"
#include "viflow/buffer.hxx"
#include "viflow/exceptions.hxx"
#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "callbacks.hxx"
#include "utils.hxx"

void Error_Callback::process_message(const viflow::Bus &bus,
                                     const viflow::Message &message) {

  auto context = message.parse_error();
  std::cerr << "Received an error from element " << message.get_source_name()
            << " with content \"" << context.error.message()
            << "\" and with debug info: \"" << context.debug.get() << "\""
            << std::endl;

  auto pipeline = viflow::Element::instance("pipeline");
  auto &loop = viflow::Main_Loop::instance();

  pipeline->stop();
  loop->quit();
}

// Stop_Feed_Callback::Stop_Feed_Callback(Context *ctx) : context{ctx} {}

// void Stop_Feed_Callback::process(const viflow::Shared_Element<> &app_source)
// {

//   if (context->sourceid != 0) {
//     std::cout << "Stop feeding\n";
//     g_source_remove(context->sourceid);
//     context->sourceid = 0;
//   }
// }

// Start_Feed_Callback::Start_Feed_Callback(Context *ctx) : context{ctx} {}

// void Start_Feed_Callback::process(const viflow::Shared_Element<> &app_source,
//                                   unsigned int length) {

//   if (context->sourceid == 0) {
//     std::cout << "Start feeding\n";
//     context->sourceid =
//         g_idle_add(reinterpret_cast<GSourceFunc>(push_data), context);
//   }
// }
