#include <iostream>

#include <gst/gst.h>
#include <gst/audio/audio.h>

#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "callbacks.hxx"
#include "utils.hxx"

void stop(int signal_number) {
  /*
     Stop the pipeline.
     */
  viflow::Element::instance("pipeline")->stop();
  /*
     Quit from the main loop.
     */
  viflow::Main_Loop::instance()->quit();
}

int main(int argc, char *argv[]) {
  /*
    Register CTRL-C handler.
    */
  signal(SIGINT, stop);

  /*
  Initialize GStreamer
  */
  gst_init(&argc, &argv);

  /*
  Initialize the context.
  */
  auto context = std::make_shared<Context>();
  context->pipeline = viflow::parse_launch("playbin", "playbin uri=appsrc://");
  // context->pipeline->connect_callback(std::make_unique<Source_Setup_Callback>(&context));
  g_signal_connect(context->pipeline->get_core(), "source-setup", G_CALLBACK(source_setup), &context);

  /* Instruct the bus to emit signals for each received message, and connect to the interesting signals */
  auto &bus = context->pipeline->get_bus();
  bus->add_signal_watch(std::make_unique<Error_Callback>(),"message::error");

  /* Start playing the pipeline */
  context->pipeline->play();

  /*
  Create a Main Loop and set it to run.
  */
  auto &loop = viflow::Main_Loop::register_instance();
  loop->run();
  /*
  Guess what, no cleanup is required.
  */
  return 0;
}