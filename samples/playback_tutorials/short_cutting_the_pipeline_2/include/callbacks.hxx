#pragma once

#include <gst/gst.h>

#include "viflow/app_sink_callback.hxx"
#include "viflow/app_source_callback.hxx"
#include "viflow/bus_watch.hxx"
#include "viflow/element_callback.hxx"
#include "utils.hxx"

class Error_Callback : public viflow::Bus_Signal_Watch {
public:
  void process_message(const viflow::Bus &bus, const viflow::Message &message);
};

// class Stop_Feed_Callback : public viflow::Enough_Data_Callback {
// public:
//   Stop_Feed_Callback(Context *ctx);
//   /*
//      This callback triggers when appsrc has enough data and we can stop sending.
//      We remove the idle handler from the mainloop.
//      */
//   void process(const viflow::Shared_Element<> &app_source);

// private:
//   Context *context;
// };

// class Start_Feed_Callback : public viflow::Need_Data_Callback {
// public:
//   Start_Feed_Callback(Context *ctx);
//   /*
//      This signal callback triggers when appsrc needs data. Here, we add an idle
//      handler to the mainloop to start pushing data into the appsrc.
//      */
//   void process(const viflow::Shared_Element<> &app_source, unsigned int length);

// private:
//   Context *context;
// };

// class Source_Setup_Callback : public viflow::Pad_Added_Callback {
// public:
//    Source_Setup_Callback(std::shared_ptr<Context> &c) : context {c} {/*do stuff*/}

// private:
//    std::shared_ptr<Context> context;
// };