#pragma once

#include <gst/gst.h>
#include <gst/audio/audio.h>

#include "viflow/pipeline.hxx"
#include "viflow/app_source.hxx"

const unsigned int CHUNK_SIZE{1024};
const unsigned int SAMPLE_RATE{44100};

struct Context
{
  viflow::Shared_Element<> pipeline;
  viflow::Shared_Element<viflow::App_Source> app_source;

  unsigned long generated_samples_count{0}; /* Number of samples generated so far (for timestamp generation) */
  float a{0}, b{1}, c{0}, d{1};             /* For waveform generation */

  unsigned int sourceid{0};
};

void source_setup(GstElement *pipeline, GstElement *source, std::shared_ptr<Context> context);