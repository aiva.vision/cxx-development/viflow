#include "iostream"

#include "gst/gst.h"

#include "viflow/bin.hxx"
#include "viflow/pipeline.hxx"
#include "viflow/element_factory.hxx"

void stop(int signal_number)
{
    /*
    Stop the pipeline.
    */
    viflow::Element::instance("pipeline")->stop();
}

int main(int argc, char *argv[])
{
    /*
    Register CTRL-C handler.
    */
    signal(SIGINT, stop);

    /*
    Initialize GStreamer
    */
    gst_init(&argc, &argv);

    /*
    Build the pipeline
    */
    auto pipeline = viflow::parse_launch("pipeline",
                                         "playbin "
                                         "uri=https://gstreamer.freedesktop.org/data/media/"
                                         "sintel_trailer-480p.webm");

    /*
    Start playing 
    */
    pipeline->play();

    /*
    Create the elements inside the sink bin
    */
    viflow::Element_Factory equalzier_factory{"equalizer-3bands"};
    viflow::Element_Factory audio_convert{"audioconvert"};
    viflow::Element_Factory audio_sink{"autoaudiosink"};

    auto equalzier = equalzier_factory.create_element("equalizer");
    auto convert = audio_convert.create_element("convert");
    auto sink = audio_convert.create_element("audio_sink");

    /*
    Create the sink bin, add the elements and link them
    */
    auto bin = viflow::Element::register_instance<viflow::Bin>("audio_sink_bin");
    bin->add(equalzier);
    bin->append(convert);
    bin->append(sink);
    bin->set_sink_elem("equalizer");

    /*
    Configuring the equalzier
    */
    equalzier->set_property("band1", static_cast<double>(-24.0));
    equalzier->set_property("band2", static_cast<double>(-24.0));

    /*
    Set playbin's audio sink to be our sink bin
    */
    pipeline->set_property("audio-sink", bin->get_core());

    /*
     Retrieve the bus.
    */
    auto &bus = pipeline->get_bus();

    /*
    Properly cast the message type after binary OR.
    */
    auto message_type =
      static_cast<GstMessageType>(GST_MESSAGE_ERROR | GST_MESSAGE_EOS);

    /*
    Block until EOS is received.
    */
    auto message = bus->timed_pop_filtered(GST_CLOCK_TIME_NONE, message_type);

    /*
    Allow the pipeline to free its resources.
    */
    pipeline->stop();
    return 0;
}
