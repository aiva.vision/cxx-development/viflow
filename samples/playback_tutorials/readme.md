# Samples

These samples are mirroring the official GStreamer samples from the [playback tutorials](https://gstreamer.freedesktop.org/documentation/tutorials/playback/index.html).

### Index

1. [Playbin usage](to do)
2. [Subtitle management](to do)
3. [Short-cuttin the pipeline](to do)
4. [Progressive streaming](to do)
5. [Color Balance](to do)
6. [Audio visualization](to do)
7. [Custon playbin sinks](to do)
8. [Hardware-accelerated video decoding](to do)
9. [Digital audio pass-through](to do)
