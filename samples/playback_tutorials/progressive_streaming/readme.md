# Progressive Streaming

This sample reimplements the basic tutorial 4 from the GStreamer [playback tutorials](https://gstreamer.freedesktop.org/documentation/tutorials/basic/index.html).

Note how the boilerplate is significantly reduced and the cleanup is completely abstracted away.
