#include "viflow/pipeline.hxx"

const int GRAPH_LENGTH {78};

struct Context
{
    viflow::Shared_Element<> pipeline;
    bool is_live;
    int buffering_level;
};

enum
{
    GST_PLAY_FLAG_DOWNLOAD = (1 << 7) /* Enable progressive download (on selected formats) */
};