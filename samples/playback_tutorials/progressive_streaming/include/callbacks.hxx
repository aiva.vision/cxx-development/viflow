#pragma once

#include <gst/gst.h>

#include "viflow/bus_watch.hxx"

class Message_Watch : public viflow::Bus_Signal_Watch {
public:
  Message_Watch(bool live);

  ~Message_Watch() {}

  void process_message(const viflow::Bus &bus, const viflow::Message &message);

private:
  bool is_live;
};