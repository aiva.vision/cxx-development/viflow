#include <gst/audio/audio.h>
#include <iostream>

#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "callbacks.hxx"
#include "utils.hxx"
#include "viflow/unique_ptr.hxx"

void stop(int signal_number) {
  /*
     Stop the pipeline.
     */
  viflow::Element::instance("pipeline")->stop();
  /*
     Quit from the main loop.
     */
  auto &loop = viflow::Main_Loop::instance();
  loop->quit();
}

static void got_location(GstObject *gstobject, GstObject *prop_object,
                         GParamSpec *prop, gpointer data) {

  viflow::unique_g_str location;
  g_object_get(G_OBJECT(prop_object), "temp-location",
               location.get_address_of(), NULL);
  g_print("Temporary file: %s\n", location.get());

  /* Uncomment this line to keep the temporary file after the program exits */
  /* g_object_set (G_OBJECT (prop_object), "temp-remove", FALSE, NULL); */
}

static gboolean refresh_ui(std::shared_ptr<Context> &context) {
  GstQuery *query;
  gboolean result;

  query = gst_query_new_buffering(GST_FORMAT_PERCENT);
  result = gst_element_query(context->pipeline->get_core(), query);
  if (result) {
    gint n_ranges, range, i;
    gchar graph[GRAPH_LENGTH + 1];

    memset(graph, ' ', GRAPH_LENGTH);
    graph[GRAPH_LENGTH] = '\0';

    n_ranges = gst_query_get_n_buffering_ranges(query);
    for (range = 0; range < n_ranges; range++) {
      gint64 start, stop;
      gst_query_parse_nth_buffering_range(query, range, &start, &stop);
      start = start * GRAPH_LENGTH / (stop - start);
      stop = stop * GRAPH_LENGTH / (stop - start);
      for (i = static_cast<int>(start); i < stop; i++)
        graph[i] = '-';
    }

    auto position = context->pipeline->query_position(GST_FORMAT_TIME);
    auto duration = context->pipeline->query_duration(GST_FORMAT_TIME);
    if (position && GST_CLOCK_TIME_IS_VALID(position) && duration &&
        GST_CLOCK_TIME_IS_VALID(duration)) {
      i = static_cast<int>(GRAPH_LENGTH * static_cast<double>(position) /
                           static_cast<double>(duration + 1));
      graph[i] = context->buffering_level < 100 ? 'X' : '>';
    }
    std::cout << graph;
    if (context->buffering_level < 100) {
      std::cout << " Buffering: " << context->buffering_level;
    } else {
      std::cout << "\t";
    }
    std::cout << "\r";
  }

  return TRUE;
}

int main(int argc, char *argv[]) {
  /*
  Register CTRL-C handler.
  */
  signal(SIGINT, stop);

  /*
  Initialize GStreamer
  */
  gst_init(&argc, &argv);

  /*
  Initialize Context
  */
  auto context = std::make_shared<Context>();

  /*
  Create the pipeline.
  */
  context->pipeline = viflow::parse_launch(
      "pipeline",
      "playbin "
      "uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/"
      "sintel_trailer-480p.webm");
  /*
  Set the download flag
  */
  auto flags = context->pipeline->get_property<unsigned int>("flags");
  flags |= GST_PLAY_FLAG_DOWNLOAD;
  context->pipeline->set_property("flags", flags);

  /* Uncomment this line to limit the amount of downloaded data */
  /* g_object_set (pipeline, "ring-buffer-max-size", (guint64)4000000, NULL); */

  /*
  Change the pipeline's state.
  */
  auto ret = context->pipeline->play();
  bool live = false;
  if (ret == GST_STATE_CHANGE_NO_PREROLL) {
    live = true;
  }

  /*
  Retrieve the bus.
  */
  auto &bus = context->pipeline->get_bus();

  /*
  Register a signal watch for the bus.
  */
  bus->add_signal_watch(std::make_unique<Message_Watch>(live), "message");
  g_signal_connect(context->pipeline->get_core(), "deep-notify::temp-location",
                   G_CALLBACK(got_location), NULL);

  /* Register a function that GLib will call every second */
  g_timeout_add_seconds(1, (GSourceFunc)refresh_ui, &context);

  /*
  Create a Main Loop and set it to run.
  */
  auto &loop = viflow::Main_Loop::register_instance();
  loop->run();

  /*
  Guess what, no cleanup is required.
  */
  return 0;
}
