add_executable(playbin_usage 
            source/keyboard_handler.cxx
            source/message_handler.cxx
            source/main.cxx)

target_link_libraries(playbin_usage
    PRIVATE
        aiva::viflow
        PkgConfig::gstreamer)

target_include_directories(playbin_usage
    PRIVATE 
    ${CMAKE_SOURCE_DIR}/include
    include)