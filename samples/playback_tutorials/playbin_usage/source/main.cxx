#include <gst/gst.h>
#include <iostream>

#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "keyboard_handler.hxx"
#include "message_handler.hxx"
#include "utils.hxx"

static void stop(int signal_number) {
  /*
     Stop the pipeline.
     */
  viflow::Element::instance("playbin")->send_event(gst_event_new_eos());
  /*
     Quit from the main loop.
     */
  // viflow::Main_Loop::instance()->quit();
}

int main(int argc, char *argv[]) {
  /*
  Register CTRL-C handler.
  */
  signal(SIGINT, stop);

  /*
  Initialize GStreamer
  */
  gst_init(&argc, &argv);

  /*
  Initialize Context
  */
  auto context = std::make_shared<Context>();

  /*
  Create the elements
  */
  context->pipeline =
      viflow::Element::register_instance<viflow::Playbin>("playbin");

  /*
   Set URI
   */
  context->pipeline->set_property(
      "uri", "https://gstreamer.freedesktop.org/data/media/"
             "sintel_cropped_multilingual.webm");

  /* Set flags to show Audio and Video but ignore Subtitles */
  int flags = context->pipeline->get_property<Flags>("flags");
  flags |= GST_PLAY_FLAG_VIDEO | GST_PLAY_FLAG_AUDIO;
  flags &= ~GST_PLAY_FLAG_TEXT;
  context->pipeline->set_property("flags", flags);

  /* Set connection speed. This will affect some internal decisions of playbin
   */
  context->pipeline->set_property("connection-speed", 56);

  /* Add a bus watch, so we get notified when a message arrives */
  auto &bus = context->pipeline->get_bus();
  bus->add_watch(std::make_unique<Message_Handler>(context));

  /* Create the IO Channel */
  auto channel = viflow::IO_Channel::instance(fileno(stdin));
  channel->add_watch(std::make_unique<Keyboard_Handler>(context));

  /* Start playing */
  context->pipeline->play();

  /* Create a GLib Main Loop and set it to run */
  auto &loop = viflow::Main_Loop::register_instance();
  loop->run();

  return 0;
}
