#include <iostream>

#include "message_handler.hxx"
#include "viflow/main_loop.hxx"
#include "viflow/tag_list.hxx"

template <typename T>
static void try_print(const viflow::Tag_List &tags, const std::string &tag) {
  try {
    auto value = tags.get<T>(tag);
    std::cout << " " << tag << ": " << value << "\n";

  } catch (const std::exception &e) {
    std::cout << " " << tag << ": unknown, " << e.what() << "\n";
  }
}

void Message_Handler::analyze_streams() {

  context->n_video = context->pipeline->get_property<int>("n-video");
  context->n_audio = context->pipeline->get_property<int>("n-audio");
  context->n_text = context->pipeline->get_property<int>("n-text");

  std::cout << context->n_video << " video stream(s), " << context->n_audio
            << " audio stream(s), " << context->n_text << " text stream(s)\n\n";

  for (int i = 0; i < context->n_video; ++i) {

    auto tags = context->pipeline->get_video_tags(i);
    std::cout << "video stream " << i << ":\n";

    try_print<std::string>(tags, GST_TAG_VIDEO_CODEC);
  }
  std::cout << std::endl;

  for (int i = 0; i < context->n_audio; ++i) {
    auto tags = context->pipeline->get_audio_tags(i);

    std::cout << "audio streams " << i << ":\n";

    try_print<std::string>(tags, GST_TAG_AUDIO_CODEC);
    try_print<std::string>(tags, GST_TAG_LANGUAGE_CODE);
    try_print<uint>(tags, GST_TAG_BITRATE);
  }
  std::cout << std::endl;

  for (int i = 0; i < context->n_text; ++i) {

    std::cout << "subtitle stream " << i << ":\n";
    try {
      auto tags = context->pipeline->get_text_tags(i);
      try_print<std::string>(tags, GST_TAG_LANGUAGE_CODE);

    } catch (const viflow::Exception &e) {
      std::cout << "no tags found\n";
    }
  }

  context->current_video = context->pipeline->get_property<int>(
      viflow::Playbin_Properties::current_video);

  context->current_audio = context->pipeline->get_property<int>(
      viflow::Playbin_Properties::current_audio);

  context->current_text = context->pipeline->get_property<int>(
      viflow::Playbin_Properties::current_text);

  std::cout << "\nCurrently playing video stream " << context->current_video
            << ", audio stream " << context->current_audio
            << " and subtitle stream " << context->current_text << "\n";
  std::cout << "Type any number and hit ENTER to select a different subtitle "
               "stream\n";
}

bool Message_Handler::process_message(const viflow::Bus &bus,
                                      const viflow::Message &message) {
  switch (message.get_type()) {
  case GST_MESSAGE_ERROR: {

    auto err_context = message.parse_error();
    std::cerr << "Received an error from element " << message.get_source_name()
              << " with content \"" << err_context.error.message()
              << "\" and with debug info: \"" << err_context.debug.get() << "\""
              << "\"" << std::endl;

    viflow::Element::instance("playbin")->stop();
    viflow::Main_Loop::instance()->quit();
    break;
  }
  case GST_MESSAGE_EOS: {

    std::cerr << "End-Of-Stream reached. \n";

    viflow::Element::instance("playbin")->stop();
    viflow::Main_Loop::instance()->quit();
    break;
  }
  case GST_MESSAGE_STATE_CHANGED: {

    if (context->pipeline->get_name() == message.get_source_name()) {

      auto state = message.parse_state_changed();
      std::cout << "State set to " << state.new_state << "\n";

      if (state.new_state == GST_STATE_PLAYING) {
        analyze_streams();
      }
    }
    break;
  }
  default: {
    break;
  }
  }
  /* We want to keep receiving messages */
  return true;
}
