#include <iostream>

#include "viflow/exceptions.hxx"
#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "keyboard_handler.hxx"

gboolean Keyboard_Handler::process(const viflow::Shared_IO_Channel &channel,
                                   GIOCondition condition) {
  std::string line;
  try {
    line = channel->read_line();
    int index = std::stoll(line);

    if (index < 0 || index >= context->n_audio) {
      std::cerr << "Index out of bounds!\n";
    } else {
      /*
         If the input was a valid audio stream index, set the current audio
         stream.
         */
      std::cout << "Setting current audio stream to " << index << "\n";
      context->pipeline->set_property(viflow::Playbin_Properties::current_audio,
                                      index);
    }
  } catch (const viflow::IO_Error_Status &err) {
    std::cerr << err.what() << std::endl;
    return true;

  } catch (const std::exception &err) {
    std::cerr << "Unexpected exception: " << err.what() << std::endl;
    return true;
  }
  return true;
}
