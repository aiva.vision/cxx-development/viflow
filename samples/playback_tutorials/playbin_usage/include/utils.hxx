#pragma once

#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"
#include "viflow/playbin.hxx"

struct Context {
  viflow::Shared_Element<viflow::Playbin>
      pipeline; /* Playbin the only element */

  int n_video{0}; /* Number of embedded video streams */
  int n_audio{0}; /* Number of embedded audio streams */
  int n_text{0};  /* Number of embedded subtitle streams */

  int current_video{0}; /* Currently playing video stream */
  int current_audio{0}; /* Currently playing audio stream */
  int current_text{0};  /* Currently playing subtitle stream */
};

enum Flags {
  GST_PLAY_FLAG_VIDEO = (1 << 0), /* We want video output */
  GST_PLAY_FLAG_AUDIO = (1 << 1), /* We want audio aoutput */
  GST_PLAY_FLAG_TEXT = (1 << 2)   /* We want subtitle output */
};
