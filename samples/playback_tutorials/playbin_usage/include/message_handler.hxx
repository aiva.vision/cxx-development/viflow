#pragma once

#include <gst/gst.h>
#include <gst/gstbus.h>

#include "utils.hxx"
#include "viflow/message.hxx"
#include "viflow/bus_watch.hxx"
#include "viflow/message.hxx"

class Message_Handler : public viflow::Bus_Watch {
public:
    Message_Handler(const std::shared_ptr<Context> &c) : context {c} {/*do stuff*/};

    ~Message_Handler() {}

    bool process_message(const viflow::Bus &bus, const viflow::Message &message)override;

private:
    void analyze_streams();

    std::shared_ptr<Context> context;
    char *debug_info;
    GError *err;
};