#include <iostream>
#include <stdio.h>
#include <string.h>

#include <gst/gst.h>

#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "keyboard_handler.hxx"

void stop(int signal_number) {

  viflow::Element::instance("pipeline")->stop();
  viflow::Main_Loop::instance()->quit();
}

int main(int argc, char *argv[]) {
  /* Register CTRL-C handler. */
  signal(SIGINT, stop);

  /* Initialize GStreamer */
  gst_init(&argc, &argv);

  /* Print usage map */
  std::cout << "USAGE: Choose one of the following options, then press enter:\n"
               " 'C' to increase contrast, 'c' to decrease contrast\n"
               " 'B' to increase brightness, 'b' to decrease brightness\n"
               " 'H' to increase hue, 'h' to decrease hue\n"
               " 'S' to increase saturation, 's' to decrease saturation\n"
               " 'Q' to quit\n";

  /* Create the pipeline. */
  auto pipeline = viflow::parse_launch(
      "pipeline",
      "playbin "
      "uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/"
      "sintel_trailer-480p.webm");

  /* Create the IO Channel */
  auto channel = viflow::IO_Channel::instance(fileno(stdin));
  channel->add_watch(std::make_unique<Keyboard_Handler>(pipeline));

  /* Start playing */
  pipeline->play();

  /* Create a GLib Main Loop and set it to run */
  auto &loop = viflow::Main_Loop::register_instance();
  loop->run();

  return 0;
}
