#include <utils.hxx>

void print_current_values(const viflow::Shared_Pipeline &pipeline) {
  /*
     Output Color Balance values
     */
  auto channels =
      gst_color_balance_list_channels(GST_COLOR_BALANCE(pipeline->get_core()));

  for (auto l = channels; l != NULL; l = l->next) {

    auto channel = static_cast<GstColorBalanceChannel *>(l->data);
    int value = gst_color_balance_get_value(
        GST_COLOR_BALANCE(pipeline->get_core()), channel);

    std::cout << channel->label << ": "
              << 100 * (value - channel->min_value) /
                     (channel->max_value - channel->min_value);
  }
  std::cout << std::endl;
}
