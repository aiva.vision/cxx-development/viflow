#include <iostream>

#include "viflow/exceptions.hxx"
#include "viflow/main_loop.hxx"
#include "viflow/pipeline.hxx"

#include "keyboard_handler.hxx"
#include "utils.hxx"

static void update_color_channel(const gchar *channel_name, gboolean increase,
                                 GstColorBalance *cb) {
  GstColorBalanceChannel *channel = nullptr;

  /* Retrieve the list of channels and locate the requested one */
  auto channels = gst_color_balance_list_channels(cb);
  for (auto l = channels; l != nullptr; l = l->next) {
    auto tmp = static_cast<GstColorBalanceChannel *>(l->data);

    if (g_strrstr(tmp->label, channel_name)) {
      channel = tmp;
      break;
    }
  }
  if (!channel)
    return;

  /* Change the channel's value */
  auto step = 0.1 * (channel->max_value - channel->min_value);
  auto value = gst_color_balance_get_value(cb, channel);

  if (increase) {
    value = (gint)(value + step);

    if (value > channel->max_value)
      value = channel->max_value;

  } else {
    value = (gint)(value - step);

    if (value < channel->min_value)
      value = channel->min_value;
  }
  gst_color_balance_set_value(cb, channel, value);
}

gboolean Keyboard_Handler::process(const viflow::Shared_IO_Channel &channel,
                                   GIOCondition condition) {
  std::string line;
  try {
    line = channel->read_line();

  } catch (const viflow::IO_Error_Status &err) {
    std::cerr << err.what() << std::endl;
    return true;

  } catch (const std::exception &err) {
    std::cerr << "Unexpected exception: " << err.what() << std::endl;
    return true;
  }

  switch (towlower(line[0])) {
  case 'c': {
    update_color_channel("CONTRAST", g_ascii_isupper(line[0]),
                         GST_COLOR_BALANCE(pipeline->get_core()));
    break;
  }
  case 'b': {
    update_color_channel("BRIGHTNESS", g_ascii_isupper(line[0]),
                         GST_COLOR_BALANCE(pipeline->get_core()));
    break;
  }
  case 'h': {
    update_color_channel("HUE", g_ascii_isupper(line[0]),
                         GST_COLOR_BALANCE(pipeline->get_core()));
    break;
  }
  case 's': {
    update_color_channel("SATURATION", g_ascii_isupper(line[0]),
                         GST_COLOR_BALANCE(pipeline->get_core()));
    break;
  }
  case 'q': {
    auto &loop = viflow::Main_Loop::instance();

    pipeline->stop();
    loop->quit();
    break;
  }
  default:
    break;
  }

  print_current_values(pipeline);
  return true;
}
