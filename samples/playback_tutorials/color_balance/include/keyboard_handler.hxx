#pragma once

#include <gst/gst.h>

#include "viflow/event.hxx"
#include "viflow/io_channel.hxx"
#include "viflow/io_channel_watch.hxx"
#include "viflow/pipeline.hxx"

#include <gst/video/colorbalance.h>

class Keyboard_Handler : public viflow::IO_Channel_Watch {
public:
  Keyboard_Handler(const viflow::Shared_Pipeline &p) : pipeline{p} {};

  ~Keyboard_Handler(){};

  gboolean process(const viflow::Shared_IO_Channel &channel,
                   GIOCondition condition);

  void send_seek_event();

  viflow::Event create_event(float rate);

private:
  float rate{1.0};
  bool playing{true};

  viflow::Shared_Pipeline pipeline;
};
