#pragma once

#include <gst/video/colorbalance.h>
#include <iostream>

#include "viflow/pipeline.hxx"

void print_current_values(const viflow::Shared_Pipeline &pipeline);
