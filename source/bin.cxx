#include <gst/gstbin.h>
#include <gst/gstpad.h>

#include "viflow/bin.hxx"
#include "viflow/exceptions.hxx"

viflow::Bin::Bin(GstElement *elem) : Element{elem} {
  if (!GST_IS_BIN(core.get())) {
    throw Exception{"Tried to initialize a Bin with a regular element."};
  }
}

viflow::Bin::Bin(const std::string &name) : Element{gst_bin_new(name.c_str())} {
  if (!core.get()) {
    throw Factory_Error("bin", name, "gst_bin_new failed");
  }
}

bool viflow::Bin::contains(const std::string &name) {
  return children.contains(name);
}

bool viflow::Bin::has_last_added() const noexcept {
  return !last_added_child.expired();
}

viflow::Shared_Element<> viflow::Bin::last_added() {

  if (auto last = last_added_child.lock()) {
    return last;
  }
  throw viflow::Exception{"There is no valid last element for bin '" +
                          get_name() + "'"};
}

viflow::Shared_Element<> viflow::Bin::add(const std::string &factory,
                                          const std::string &name) {

  return add(Element::register_instance(factory, name));
}

viflow::Shared_Element<> viflow::Bin::append(const std::string &factory,
                                             const std::string &name) {

  return append(Element::register_instance(factory, name));
}

viflow::Shared_Element<> viflow::Bin::remove(const std::string &name) {

  auto elem = child(name);
  /*
     Lock againg only after retrieving the child.
     */
  std::unique_lock<std::mutex> lck{children_mtx};
  if (!gst_bin_remove(GST_BIN(core.get()), elem->get_core())) {
    throw Exception{"Failed to remove element " + name + " from " + get_name()};
  }
  children.erase(name);
  auto last = last_added_child.lock();

  if (last && last->get_name() == name) {
    last_added_child.reset();
  }
  return elem;
}

void viflow::Bin::link_elements(const std::string &elem1,
                                const std::string &elem2) {

  child(elem1)->link(child(elem2));
}

void viflow::Bin::link_elements(const std::string &elem1,
                                const std::string &src_pad_name,
                                const std::string &elem2,
                                const std::string &sink_pad_name) {

  child(elem1)->link(child(elem2), src_pad_name, sink_pad_name);
}

void viflow::Bin::add_ghost_pad(const std::string &elem,
                                const std::string &pad_name) {
  auto &pad = child(elem)->get_pad(pad_name);
  add_pad(pad.create_ghost_pad(pad_name));
}

void viflow::Bin::set_src_elem(const std::string &elem) {
  add_ghost_pad(elem, "src");
}

void viflow::Bin::set_sink_elem(const std::string &elem) {
  add_ghost_pad(elem, "sink");
}

void viflow::Bin::unregister() {
  /*
     First unregister the children.
     */
  for (auto &[k, e] : children) {
    if (auto elem = e.lock()) {
      elem->unregister();
    }
  }
  /*
     Unregister this instance too.
     */
  viflow::Element::unregister();
}
