#include "viflow/queue.hxx"

viflow::Queue::Queue(const std::string &name) : Element{"queue", name} {}

static void underrun_handler(GstElement *queue,
                             viflow::Underrun_Callback *callback) {

  auto vi_queue = viflow::Element::convert(queue);
  /*
     Process the signal.
     */
  callback->process(vi_queue);
}

void viflow::Queue::connect_callback(
    std::unique_ptr<Underrun_Callback> &&callback) {

  connect_callback(Queue_Signals::underrun, underrun_handler,
                   std::move(callback));
}

static void running_handler(GstElement *queue,
                            viflow::Running_Callback *callback) {

  auto vi_queue = viflow::Element::convert(queue);
  /*
     Process the signal.
     */
  callback->process(vi_queue);
}

void viflow::Queue::connect_callback(
    std::unique_ptr<Running_Callback> &&callback) {

  connect_callback(Queue_Signals::running, running_handler,
                   std::move(callback));
}

static void overrun_handler(GstElement *queue,
                            viflow::Overrun_Callback *callback) {

  auto vi_queue = viflow::Element::convert(queue);
  /*
     Process the signal.
     */
  callback->process(vi_queue);
}

void viflow::Queue::connect_callback(
    std::unique_ptr<Overrun_Callback> &&callback) {

  connect_callback(Queue_Signals::overrun, overrun_handler,
                   std::move(callback));
}

static void pushing_handler(GstElement *queue,
                            viflow::Pushing_Callback *callback) {

  auto vi_queue = viflow::Element::convert(queue);
  /*
     Process the signal.
     */
  return callback->process(vi_queue);
}

void viflow::Queue::connect_callback(
    std::unique_ptr<Pushing_Callback> &&callback) {

  connect_callback(Queue_Signals::pushing, pushing_handler,
                   std::move(callback));
}
