#include <gst/gstsample.h>

#include "viflow/split_mux_sink.hxx"

viflow::Split_Mux_Sink::Split_Mux_Sink(const std::string &name)
    : Element{"splitmuxsink", name} {}

static char *
format_location_handler(GstElement *splitmux, unsigned fragment_id,
                        viflow::Format_Location_Callback *callback) {

  auto vi_splitmux = viflow::Element::convert(splitmux);
  /*
     Process the signal.
     */
  auto location = callback->process(vi_splitmux, fragment_id);

  return strdup(location.c_str());
}

void viflow::Split_Mux_Sink::connect_callback(
    std::unique_ptr<Format_Location_Callback> &&callback) {

  connect_callback(Split_Mux_Sink_Signals::format_location,
                   format_location_handler, std::move(callback));
}

static char *
format_location_full_handler(GstElement *splitmux, unsigned fragment_id,
                             GstSample *first_sample,
                             viflow::Format_Location_Full_Callback *callback) {

  auto vi_splitmux = viflow::Element::convert(splitmux);
  /*
     Process the signal.
     */
  auto location = callback->process(vi_splitmux, fragment_id, first_sample);

  return strdup(location.c_str());
}

void viflow::Split_Mux_Sink::connect_callback(
    std::unique_ptr<Format_Location_Full_Callback> &&callback) {

  connect_callback(Split_Mux_Sink_Signals::format_location_full,
                   format_location_full_handler, std::move(callback));
}

static void element_added_handler(GstElement *splitmux, GstElement *elem,
                                  viflow::Muxer_Added_Callback *callback) {

  auto vi_splitmux = viflow::Element::convert(splitmux);
  auto vi_elem = viflow::Element::convert(elem);
  /*
     Process the signal.
     */
  callback->process(vi_splitmux, vi_elem);
}

void viflow::Split_Mux_Sink::connect_callback(
    std::unique_ptr<Muxer_Added_Callback> &&callback) {

  connect_callback(Split_Mux_Sink_Signals::muxer_added, element_added_handler,
                   std::move(callback));
}

void viflow::Split_Mux_Sink::connect_callback(
    std::unique_ptr<Sink_Added_Callback> &&callback) {

  connect_callback(Split_Mux_Sink_Signals::sink_added, element_added_handler,
                   std::move(callback));
}

void viflow::Split_Mux_Sink::split_now() const noexcept {
  g_signal_emit_by_name(core.get(), Split_Mux_Sink_Action_Signals::split_now);
}

void viflow::Split_Mux_Sink::split_after() const noexcept {
  g_signal_emit_by_name(core.get(), Split_Mux_Sink_Action_Signals::split_after);
}
