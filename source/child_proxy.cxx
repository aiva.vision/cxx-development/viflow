#include <gst/gstchildproxy.h>

#include "viflow/child_proxy.hxx"

static void child_proxy_cb_handler(GstChildProxy *self, GObject *object,
                                   gchar *name,
                                   viflow::Child_Added_Callback *callback) {

  auto vi_src = viflow::Element::convert(GST_ELEMENT(self));
  /*
     Process the signal.
     */
  callback->process(vi_src, object, name);
}

void viflow::Child_Proxy::connect_callback(
    std::unique_ptr<Child_Added_Callback> &&callback) {

  connect_callback(Child_Proxy_Signals::child_added, child_proxy_cb_handler,
                   std::move(callback));
}

void viflow::Child_Proxy::connect_callback(
    std::unique_ptr<Child_Removed_Callback> &&callback) {

  connect_callback(Child_Proxy_Signals::child_removed, child_proxy_cb_handler,
                   std::move(callback));
}
