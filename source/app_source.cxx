#include <gst/app/gstappsrc.h>

#include "viflow/app_source.hxx"
#include "viflow/app_source_callback.hxx"

viflow::App_Source::App_Source(const std::string &name)
    : Element{"appsrc", name} {}

GstFlowReturn viflow::App_Source::push_buffer(Buffer &&buffer) {

  return gst_app_src_push_buffer(reinterpret_cast<GstAppSrc *>(core.get()),
                                 buffer.release());
}

static void enough_data_handler(GstElement *source, void *user_data) {

  viflow::Enough_Data_Callback *callback =
      static_cast<viflow::Enough_Data_Callback *>(user_data);

  auto app_source = viflow::Element::convert(source);

  callback->process(app_source);
}

void viflow::App_Source::connect_callback(
    std::unique_ptr<Enough_Data_Callback> &&callback) {

  connect_callback(App_Source_Signals::enough_data, enough_data_handler,
                   std::move(callback));
}

static void need_data_handler(GstElement *source, unsigned int size,
                              void *user_data) {
  viflow::Need_Data_Callback *callback =
      static_cast<viflow::Need_Data_Callback *>(user_data);

  auto app_source = viflow::Element::convert(source);

  callback->process(app_source, size);
}

void viflow::App_Source::connect_callback(
    std::unique_ptr<Need_Data_Callback> &&callback) {

  connect_callback(App_Source_Signals::need_data, need_data_handler,
                   std::move(callback));
}

static void seek_data_handler(GstElement *source, unsigned long offset,
                              void *user_data) {
  viflow::Seek_Data_Callback *callback =
      static_cast<viflow::Seek_Data_Callback *>(user_data);

  auto app_source = viflow::Element::convert(source);

  callback->process(app_source, offset);
}

void viflow::App_Source::connect_callback(
    std::unique_ptr<Seek_Data_Callback> &&callback) {

  connect_callback(App_Source_Signals::seek_data, seek_data_handler,
                   std::move(callback));
}
