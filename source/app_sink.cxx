#include <gst/app/gstappsink.h>

#include "viflow/app_sink.hxx"
#include "viflow/app_sink_callback.hxx"

viflow::App_Sink::App_Sink(const std::string &name)
    : Element{"appsink", name} {}

static void eos_handler(GstElement *sink, void *user_data) {

  viflow::EoS_Callback *callback =
      static_cast<viflow::EoS_Callback *>(user_data);

  auto app_sink = viflow::Element::convert(sink);

  callback->process(app_sink);
}

void viflow::App_Sink::connect_callback(
    std::unique_ptr<EoS_Callback> &&callback) {

  connect_callback(App_Sink_Signals::eos, eos_handler, std::move(callback));
}

static GstFlowReturn new_preroll_handler(GstElement *sink, void *user_data) {

  viflow::New_Preroll_Callback *callback =
      static_cast<viflow::New_Preroll_Callback *>(user_data);

  auto app_sink = viflow::Element::convert(sink);

  return callback->process(app_sink);
}

void viflow::App_Sink::connect_callback(
    std::unique_ptr<New_Preroll_Callback> &&callback) {

  connect_callback(App_Sink_Signals::new_preroll, new_preroll_handler,
                   std::move(callback));
}

viflow::Sample viflow::App_Sink::get_sample() const noexcept {

  auto sample =
      gst_app_sink_pull_sample(reinterpret_cast<GstAppSink *>(core.get()));
  return Sample(sample);
}

static GstFlowReturn new_sample_handler(GstElement *sink, void *user_data) {

  viflow::New_Sample_Callback *callback =
      static_cast<viflow::New_Sample_Callback *>(user_data);

  auto app_sink = viflow::Element::convert(sink);

  return callback->process(app_sink);
}

void viflow::App_Sink::connect_callback(
    std::unique_ptr<New_Sample_Callback> &&callback) {

  connect_callback(App_Sink_Signals::new_sample, new_sample_handler,
                   std::move(callback));
}

static gboolean new_serialized_event_handler(GstElement *sink,
                                             void *user_data) {

  viflow::New_Serialized_Event_Callback *callback =
      static_cast<viflow::New_Serialized_Event_Callback *>(user_data);

  auto app_sink = viflow::Element::convert(sink);

  return callback->process(app_sink);
}

void viflow::App_Sink::connect_callback(
    std::unique_ptr<New_Serialized_Event_Callback> &&callback) {

  connect_callback(App_Sink_Signals::new_serialized_event,
                   new_serialized_event_handler, std::move(callback));
}
