#include "viflow/element_factory.hxx"
#include "viflow/exceptions.hxx"

#include <gst/gst.h>

viflow::Element_Factory::Element_Factory(const std::string &name)
    : core{gst_element_factory_find(name.c_str())} {
  if (!core) {
    throw Factory_Not_Found{name};
  }
}

const char *viflow::Element_Factory::get_name() const noexcept {
  return gst_element_factory_get_longname(core.get());
}

unsigned int viflow::Element_Factory::get_pad_templates_count() const noexcept {
  return gst_element_factory_get_num_pad_templates(core.get());
}

viflow::GList_Iterator
viflow::Element_Factory::get_static_pad_templates() const noexcept {
  return GList_Iterator{
      gst_element_factory_get_static_pad_templates(core.get())};
}

GstElementFactory *viflow::Element_Factory::get_core() const noexcept {
  return core.get();
}

viflow::Shared_Element<>
viflow::Element_Factory::create_element(const std::string &name) const {

  unique_element elem{gst_element_factory_create(core.get(), name.c_str())};
  if (!elem) {
    throw Factory_Error{get_name(), name};
  }
  return Element::register_instance(elem.release());
}
