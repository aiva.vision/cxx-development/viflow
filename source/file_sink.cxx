#include "viflow/file_sink.hxx"

viflow::File_Sink::File_Sink(const std::string &name)
    : Element{"filesink", name} {}
