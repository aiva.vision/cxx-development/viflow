#include "viflow/event.hxx"
#include "viflow/element.hxx"
#include "viflow/exceptions.hxx"

viflow::Event viflow::Event::ref(GstEvent *e) {
  return Event{gst_event_ref(e)};
}

viflow::Event viflow::Event::ref(const Event &e) { return ref(e.get_core()); }

viflow::Event::Event(double rate, GstFormat format, GstSeekFlags flags,
                     GstSeekType start_type, long start, GstSeekType stop_type,
                     long stop)
    : core{gst_event_new_seek(rate, format, flags, start_type, start, stop_type,
                              stop)} {
  if (!core) {
    throw Exception("Failed to create new seek event");
  }
}

viflow::Event::Event(GstFormat format, unsigned long amount, double rate,
                     bool flush, bool intermediate)
    : core{gst_event_new_step(format, amount, rate, flush, intermediate)} {
  if (!core) {
    throw Exception("Failed to create new step event");
  }
}

viflow::Event::Event(GstEvent *event) : core{event} {
  if (!core) {
    throw Exception("Tried to initialize Event from a null value");
  }
}

GstEvent *viflow::Event::get_core() const noexcept { return core.get(); }

GstEvent *viflow::Event::release() noexcept { return core.release(); }
