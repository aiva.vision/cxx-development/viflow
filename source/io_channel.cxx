#include "viflow/io_channel.hxx"
#include "viflow/exceptions.hxx"
#include "viflow/unique_resources.hxx"

std::unordered_map<int, std::shared_ptr<viflow::IO_Channel>>
    viflow::IO_Channel::channels;

std::shared_ptr<viflow::IO_Channel> viflow::IO_Channel::instance(int fd) {

  if (!channels.contains(fd)) {

    auto ch = channels.insert_or_assign(
        fd, std::shared_ptr<IO_Channel>{new IO_Channel{fd}});

    return ch.first->second;
  }
  return channels.at(fd);
}

std::shared_ptr<viflow::IO_Channel>
viflow::IO_Channel::instance(GIOChannel *channel) {

  auto fd = g_io_channel_unix_get_fd(channel);

  if (!channels.contains(fd)) {

    auto ch = channels.insert_or_assign(
        fd, std::shared_ptr<IO_Channel>{new IO_Channel{channel}});

    return ch.first->second;
  }
  return channels.at(fd);
}

viflow::IO_Channel::IO_Channel(int file_number)
    : core{g_io_channel_unix_new(file_number)} {
  if (!core) {
    throw Exception("Failed to create IO_Channel");
  }
}

viflow::IO_Channel::IO_Channel(GIOChannel *channel) : core{channel} {
  if (!core) {
    throw Exception("Tried to initialize IO_Channel from a null value");
  }
}

GIOChannel *viflow::IO_Channel::get_core() const noexcept { return core.get(); }

std::string viflow::IO_Channel::read_line() const {

  unique_g_str line;
  auto status = g_io_channel_read_line(core.get(), line.get_address_of(),
                                       nullptr, nullptr, nullptr);

  if (status != G_IO_STATUS_NORMAL) {
    throw IO_Error_Status(status);
  }
  return line ? line.get() : "";
}

static gboolean handle_keyboard(GIOChannel *source, GIOCondition cond,
                                void *user_data) {

  auto channel = viflow::IO_Channel::instance(source);

  auto watch = static_cast<viflow::IO_Channel_Watch *>(user_data);

  return watch->process(channel, cond);
}

unsigned int
viflow::IO_Channel::add_watch(std::unique_ptr<IO_Channel_Watch> &&watch) {
  if (channel_watch) {
    throw Exception{"Tried to add a second watch to IO_Channel"};
  }
  channel_watch = std::move(watch);
  return g_io_add_watch(core.get(), G_IO_IN, (GIOFunc)handle_keyboard,
                        channel_watch.get());
}
