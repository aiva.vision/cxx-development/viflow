#include "viflow/rtsp_src.hxx"

viflow::RTSP_Src::RTSP_Src(const std::string &name)
    : Element{"rtspsrc", name} {}

static void on_sdp_handler(GstElement *rtspsrc, GstSDPMessage *message,
                           viflow::On_SDP_Callback *callback) {

  auto vi_src = viflow::Element::convert(rtspsrc);
  /*
     Process the signal.
     */
  callback->process(vi_src, message);
}

void viflow::RTSP_Src::connect_callback(
    std::unique_ptr<On_SDP_Callback> &&callback) {

  connect_callback(RTSP_Src_Signals::on_sdp, on_sdp_handler,
                   std::move(callback));
}

static gboolean
select_stream_handler(GstElement *rtspsrc, guint num, GstCaps *caps,
                      viflow::Select_Stream_Callback *callback) {

  auto vi_src = viflow::Element::convert(rtspsrc);
  /*
     Process the signal.
     */
  return callback->process(vi_src, num, caps);
}

void viflow::RTSP_Src::connect_callback(
    std::unique_ptr<Select_Stream_Callback> &&callback) {

  connect_callback(RTSP_Src_Signals::select_stream, select_stream_handler,
                   std::move(callback));
}

static void new_manager_handler(GstElement *rtspsrc, GstElement *manager,
                                viflow::New_Manager_Callback *callback) {

  auto vi_src = viflow::Element::convert(rtspsrc);
  auto vi_manager = viflow::Element::convert(manager);
  /*
     Process the signal.
     */
  callback->process(vi_src, vi_manager);
}

void viflow::RTSP_Src::connect_callback(
    std::unique_ptr<New_Manager_Callback> &&callback) {

  connect_callback(RTSP_Src_Signals::new_manager, new_manager_handler,
                   std::move(callback));
}

static GstCaps *
request_rtcp_key_handler(GstElement *rtspsrc, guint num,
                         viflow::Request_RTCP_Key_Callback *callback) {

  auto vi_src = viflow::Element::convert(rtspsrc);
  /*
     Process the signal.
     */
  return callback->process(vi_src, num);
}

void viflow::RTSP_Src::connect_callback(
    std::unique_ptr<Request_RTCP_Key_Callback> &&callback) {

  connect_callback(RTSP_Src_Signals::request_rtcp_key, request_rtcp_key_handler,
                   std::move(callback));
}

static gboolean
accept_certificate_handler(GstElement *rtspsrc, GTlsConnection *peer_cert,
                           GTlsCertificate *errors,
                           GTlsCertificateFlags *user_data,
                           viflow::Accept_Certificate_Callback *callback) {

  auto vi_src = viflow::Element::convert(rtspsrc);
  /*
     Process the signal.
     */
  return callback->process(vi_src, peer_cert, errors, user_data);
}

void viflow::RTSP_Src::connect_callback(
    std::unique_ptr<Accept_Certificate_Callback> &&callback) {

  connect_callback(RTSP_Src_Signals::accept_certificate,
                   accept_certificate_handler, std::move(callback));
}

static gboolean before_send_handler(GstElement *rtspsrc,
                                    GstRTSPMessage *message,
                                    viflow::Before_Send_Callback *callback) {

  auto vi_src = viflow::Element::convert(rtspsrc);
  /*
     Process the signal.
     */
  return callback->process(vi_src, message);
}

void viflow::RTSP_Src::connect_callback(
    std::unique_ptr<Before_Send_Callback> &&callback) {

  connect_callback(RTSP_Src_Signals::before_send, before_send_handler,
                   std::move(callback));
}
