#include <gst/gstparse.h>
#include <gst/gstpipeline.h>

#include "viflow/error.hxx"
#include "viflow/exceptions.hxx"
#include "viflow/pipeline.hxx"

viflow::Pipeline::Pipeline(GstElement *pipeline) : Bin{pipeline} {
  if (!GST_IS_PIPELINE(core.get())) {
    throw Exception{"Tried to initialize a Pipeline with a regular element."};
  }
}

viflow::Pipeline::Pipeline(const std::string &name)
    : Bin{gst_pipeline_new(name.c_str())} {
  if (!core) {
    throw Exception{"Failed to create the viflow pipeline."};
  }
}

viflow::Shared_Pipeline viflow::parse_launch(const std::string &name,
                                             const std::string &pipeline) {
  unique_error err;
  unique_element p{gst_parse_launch(pipeline.c_str(), err.get_address_of())};

  if (!p) {
    throw Parse_Launch_Error(err->message);
  }
  g_object_set(p.get(), "name", name.c_str(), nullptr);

  return Element::register_instance<Pipeline>(p.release());
}
