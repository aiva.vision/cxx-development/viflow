#include "viflow/tee.hxx"

viflow::Tee::Tee(const std::string &name) : Element{"tee", name} {}

viflow::Shared_Element<> viflow::Tee::link(const Shared_Element<> &elem) {
  return link(elem, "src_%u", "sink");
}
