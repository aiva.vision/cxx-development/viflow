#include "viflow/main_loop.hxx"
#include "viflow/exceptions.hxx"

std::unique_ptr<viflow::Main_Loop> viflow::Main_Loop::main_loop;

bool viflow::Main_Loop::is_registered() noexcept {
  return main_loop != nullptr;
}

std::unique_ptr<viflow::Main_Loop> &
viflow::Main_Loop::register_instance(GMainContext *ctx, bool is_running) {
  if (main_loop) {
    throw Exception{"Tried to register a new Main_Loop instance a second time"};
  }
  main_loop.reset(new Main_Loop(ctx, is_running));
  return main_loop;
}

std::unique_ptr<viflow::Main_Loop> &viflow::Main_Loop::instance() {
  if (!main_loop) {
    throw Exception{"Requested a Main_Loop instance before registering it"};
  }
  return main_loop;
}

viflow::Main_Loop::Main_Loop(GMainContext *ctx, bool is_running)
    : core{g_main_loop_new(ctx, is_running)} {}

bool viflow::Main_Loop::is_running() const noexcept {
  return g_main_loop_is_running(core.get());
}

GMainLoop *viflow::Main_Loop::get_core() const noexcept { return core.get(); }

void viflow::Main_Loop::run() const noexcept { g_main_loop_run(core.get()); }

void viflow::Main_Loop::quit() const noexcept { g_main_loop_quit(core.get()); }
