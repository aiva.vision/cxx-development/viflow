#include "viflow/capabilities.hxx"
#include "viflow/error.hxx"
#include "viflow/exceptions.hxx"
#include "viflow/unique_resources.hxx"

viflow::Capabilities::Capabilities() : Capabilities{gst_caps_new_any()} {}

viflow::Capabilities
viflow::Capabilities::from_string(const std::string &caps) {
  return Capabilities{gst_caps_from_string(caps.c_str())};
}

viflow::Capabilities viflow::Capabilities::ref(const GstCaps *c) {
  return Capabilities{gst_caps_ref(const_cast<GstCaps *>(c))};
}

viflow::Capabilities viflow::Capabilities::ref(const Capabilities &c) {
  return ref(c.get_core());
}

viflow::Capabilities viflow::Capabilities::copy(const GstCaps *c) {
  return Capabilities{gst_caps_copy(c)};
}

viflow::Capabilities viflow::Capabilities::copy(const Capabilities &c) {
  return copy(c.get_core());
}

viflow::Capabilities
viflow::Capabilities::intersect(GstCaps *caps_1, GstCaps *caps_2,
                                GstCapsIntersectMode mode) {

  return Capabilities{gst_caps_intersect_full(caps_1, caps_2, mode)};
}

viflow::Capabilities
viflow::Capabilities::intersect(const Capabilities &caps_1,
                                const Capabilities &caps_2,
                                GstCapsIntersectMode mode) {

  return intersect(caps_1.get_core(), caps_2.get_core(), mode);
}

bool viflow::Capabilities::can_intersect(GstCaps *caps_1, GstCaps *caps_2) {

  return gst_caps_can_intersect(caps_1, caps_2);
}

bool viflow::Capabilities::can_intersect(const Capabilities &caps_1,
                                         const Capabilities &caps_2) {

  return can_intersect(caps_1.get_core(), caps_2.get_core());
}

std::string viflow::Capabilities::to_string(const GstCaps *caps) {

  unique_g_str s{gst_caps_to_string(caps)};
  if (!s) {
    throw Exception{"Failed to convert caps to string"};
  }
  return s.get();
}

viflow::Capabilities::Capabilities(const Capabilities &caps)
    : core{gst_caps_copy(caps.get_core())} {

  if (!core) {
    throw Exception{"Faled to copy caps"};
  }
}

viflow::Capabilities::Capabilities(GstCaps *caps) : core{caps} {
  if (!core) {
    throw Exception{"Tried to initialize Capabilities from a null falue."};
  }
}

viflow::Capabilities::Capabilities(const std::string &media_type)
    : core{gst_caps_new_empty_simple(media_type.c_str())} {
  if (!core) {
    throw Bad_Caps{media_type};
  }
}

viflow::Capabilities::Capabilities(const std::string &media_type,
                                   const std::string &format,
                                   const std::string &features)

    : core{gst_caps_new_simple(media_type.c_str(), viflow::Caps_Fields::format,
                               G_TYPE_STRING, format.c_str(), nullptr)} {
  if (!core) {
    throw Bad_Caps{media_type + "," + format};
  }
  if (!features.empty()) {
    viflow::Features f{features};
    set_features(0, std::move(f));
  }
}

viflow::Capabilities &
viflow::Capabilities::operator=(const Capabilities &caps) {

  core.reset(gst_caps_copy(caps.get_core()));

  return *this;
}

bool viflow::Capabilities::is_any() const noexcept {
  return gst_caps_is_any(core.get());
}

bool viflow::Capabilities::is_fixed() const noexcept {
  return gst_caps_is_fixed(core.get());
}

bool viflow::Capabilities::is_empty() const noexcept {
  return gst_caps_is_empty(core.get());
}

unsigned int viflow::Capabilities::get_size() const noexcept {
  return gst_caps_get_size(core.get());
}

GstCaps *viflow::Capabilities::get_core() const noexcept { return core.get(); }

GstCaps *viflow::Capabilities::release() noexcept { return core.release(); }

GstStructure *
viflow::Capabilities::get_structure(unsigned int index) const noexcept {
  return gst_caps_get_structure(core.get(), index);
}

GstCapsFeatures *
viflow::Capabilities::get_features(unsigned int index) const noexcept {
  return gst_caps_get_features(core.get(), index);
}

void viflow::Capabilities::set_features(unsigned int index,
                                        Features &&features) {
  gst_caps_set_features(core.get(), index, features.release());
}

std::string viflow::Capabilities::to_string() const {
  return to_string(core.get());
}

void viflow::Capabilities::simplify() {

  unique_caps caps{gst_caps_simplify(core.release())};
  if (!caps) {
    throw Exception{"Filed to simplify caps"};
  }
  core = std::move(caps);
}

void viflow::Capabilities::make_writable() {

  unique_caps caps{gst_caps_make_writable(core.release())};
  if (!caps) {
    throw Exception{"Filed to make caps writable"};
  }
  core = std::move(caps);
}
