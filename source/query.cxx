#include <gst/gstquery.h>

#include "viflow/capabilities.hxx"
#include "viflow/element.hxx"
#include "viflow/pad.hxx"
#include "viflow/query.hxx"

viflow::Query::Query(GstQuery *query) : core{query} {}

bool viflow::Query::query_element(const Shared_Element<> &elem) {
  return gst_element_query(elem->get_core(), core.get());
}

bool viflow::Query::query_pad(Pad &pad) {
  return gst_pad_query(pad.get_core(), core.get());
}

viflow::Seeking_Query::Seeking_Query(GstFormat format)
    : Query{gst_query_new_seeking(format)} {}

viflow::Seeking_Context viflow::Seeking_Query::parse_seeking() {

  Seeking_Context context{};
  gst_query_parse_seeking(get_core(), &context.format, &context.seekable,
                          &context.start_segment, &context.end_segment);
  return context;
}
