#include "viflow/sample.hxx"

viflow::Sample::Sample(GstSample *sample) : core{sample} {}

bool viflow::Sample::is_empty() const noexcept { return core.empty(); }

GstSample *viflow::Sample::get_core() const noexcept { return core.get(); }

viflow::Buffer viflow::Sample::get_buffer() const noexcept {
  return Buffer::ref(gst_sample_get_buffer(core.get()));
}
