#include "viflow/playbin.hxx"

viflow::Playbin::Playbin(const std::string &name)
    : viflow::Pipeline{gst_element_factory_make("playbin", name.c_str())} {}

viflow::Playbin::Playbin(const std::string &name, const std::string &uri)
    : Playbin{name} {
  set_property(Playbin_Properties::uri, uri.c_str());
}

viflow::Sample viflow::Playbin::convert_sample(GstCaps *caps) {
  GstSample *sample = nullptr;
  g_signal_emit_by_name(get_core(), Playbin_Action_Signals::convert_sample,
                        caps, nullptr);
  return viflow::Sample{sample};
}

viflow::Sample viflow::Playbin::convert_sample(const Capabilities &caps) {
  return convert_sample(caps.get_core());
}

viflow::Pad &viflow::Playbin::get_audio_pad(int stream) {
  GstPad *pad = nullptr;
  g_signal_emit_by_name(get_core(), Playbin_Action_Signals::get_audio_pad,
                        stream, &pad);
  return Pad::convert(pad);
}

viflow::Tag_List viflow::Playbin::get_audio_tags(int stream) {
  GstTagList *tags = nullptr;
  g_signal_emit_by_name(get_core(), Playbin_Action_Signals::get_audio_tags,
                        stream, &tags);
  return Tag_List{tags};
}

viflow::Pad &viflow::Playbin::get_text_pad(int stream) {
  GstPad *pad = nullptr;
  g_signal_emit_by_name(get_core(), Playbin_Action_Signals::get_text_pad,
                        stream, &pad);
  return Pad::convert(pad);
}

viflow::Tag_List viflow::Playbin::get_text_tags(int stream) {
  GstTagList *tags = nullptr;
  g_signal_emit_by_name(get_core(), Playbin_Action_Signals::get_text_tags,
                        stream, &tags);
  return Tag_List{tags};
}

viflow::Pad &viflow::Playbin::get_video_pad(int stream) {
  GstPad *pad = nullptr;
  g_signal_emit_by_name(get_core(), Playbin_Action_Signals::get_video_pad,
                        stream, &pad);
  return Pad::convert(pad);
}

viflow::Tag_List viflow::Playbin::get_video_tags(int stream) {
  GstTagList *tags = nullptr;
  g_signal_emit_by_name(get_core(), Playbin_Action_Signals::get_video_tags,
                        stream, &tags);
  return Tag_List{tags};
}

static void
about_to_finish_handler(GstElement *playbin,
                        viflow::About_To_Finish_Callback *callback) {

  auto vi_playbin = viflow::Element::convert(playbin);
  /*
     Process the signal.
     */
  callback->process(vi_playbin);
}
void viflow::Playbin::connect_callback(
    std::unique_ptr<viflow::About_To_Finish_Callback> &&callback) {

  connect_callback(Playbin_Signals::about_to_finish, about_to_finish_handler,
                   std::move(callback));
}

static void audio_changed_handler(GstElement *playbin,
                                  viflow::Audio_Changed_Callback *callback) {

  auto vi_playbin = viflow::Element::convert(playbin);
  /*
     Process the signal.
     */
  callback->process(vi_playbin);
}

void viflow::Playbin::connect_callback(
    std::unique_ptr<viflow::Audio_Changed_Callback> &&callback) {

  connect_callback(Playbin_Signals::audio_changed, audio_changed_handler,
                   std::move(callback));
}

static void
audio_tags_changed_handler(GstElement *playbin, int stream,
                           viflow::Audio_Tags_Changed_Callback *callback) {

  auto vi_playbin = viflow::Element::convert(playbin);
  /*
     Process the signal.
     */
  callback->process(vi_playbin, stream);
}

void viflow::Playbin::connect_callback(
    std::unique_ptr<viflow::Audio_Tags_Changed_Callback> &&callback) {

  connect_callback(Playbin_Signals::audio_tags_changed,
                   audio_tags_changed_handler, std::move(callback));
}

static void element_setup_handler(GstElement *playbin, GstElement *element,
                                  viflow::Element_Setup_Callback *callback) {

  auto vi_playbin = viflow::Element::convert(playbin);
  auto vi_element = viflow::Element::convert(element);
  /*
     Process the signal.
     */
  callback->process(vi_playbin, vi_element);
}

void viflow::Playbin::connect_callback(
    std::unique_ptr<viflow::Element_Setup_Callback> &&callback) {

  connect_callback(Playbin_Signals::element_setup, element_setup_handler,
                   std::move(callback));
}

static void source_setup_handler(GstElement *playbin, GstElement *source,
                                 viflow::Source_Setup_Callback *callback) {

  auto vi_playbin = viflow::Element::convert(playbin);
  auto vi_source = viflow::Element::convert(source);
  /*
     Process the signal.
     */
  callback->process(vi_playbin, vi_source);
}

void viflow::Playbin::connect_callback(
    std::unique_ptr<viflow::Source_Setup_Callback> &&callback) {

  connect_callback(Playbin_Signals::source_setup, source_setup_handler,
                   std::move(callback));
}

static void text_changed_handler(GstElement *playbin,
                                 viflow::Text_Changed_Callback *callback) {

  auto vi_playbin = viflow::Element::convert(playbin);
  /*
     Process the signal.
     */
  callback->process(vi_playbin);
}

void viflow::Playbin::connect_callback(
    std::unique_ptr<viflow::Text_Changed_Callback> &&callback) {

  connect_callback(Playbin_Signals::text_changed, text_changed_handler,
                   std::move(callback));
}

static void
text_tags_changed_handler(GstElement *playbin, int stream,
                          viflow::Text_Tags_Changed_Callback *callback) {

  auto vi_playbin = viflow::Element::convert(playbin);
  /*
     Process the signal.
     */
  callback->process(vi_playbin, stream);
}

void viflow::Playbin::connect_callback(
    std::unique_ptr<viflow::Text_Tags_Changed_Callback> &&callback) {

  connect_callback(Playbin_Signals::text_tags_changed,
                   text_tags_changed_handler, std::move(callback));
}

static void video_changed_handler(GstElement *playbin,
                                  viflow::Video_Changed_Callback *callback) {

  auto vi_playbin = viflow::Element::convert(playbin);
  /*
     Process the signal.
     */
  callback->process(vi_playbin);
}

void viflow::Playbin::connect_callback(
    std::unique_ptr<viflow::Video_Changed_Callback> &&callback) {

  connect_callback(Playbin_Signals::video_changed, video_changed_handler,
                   std::move(callback));
}

static void
video_tags_changed_handler(GstElement *playbin, int stream,
                           viflow::Video_Tags_Changed_Callback *callback) {

  auto vi_playbin = viflow::Element::convert(playbin);
  /*
     Process the signal.
     */
  callback->process(vi_playbin, stream);
}

void viflow::Playbin::connect_callback(
    std::unique_ptr<viflow::Video_Tags_Changed_Callback> &&callback) {

  connect_callback(Playbin_Signals::video_tags_changed,
                   video_tags_changed_handler, std::move(callback));
}
