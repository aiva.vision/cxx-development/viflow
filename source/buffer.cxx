#include <gst/gst.h>

#include "viflow/buffer.hxx"
#include "viflow/exceptions.hxx"

viflow::Buffer viflow::Buffer::ref(GstBuffer *b) {
  return Buffer{gst_buffer_ref(b)};
}

viflow::Buffer viflow::Buffer::ref(const Buffer &b) {
  return ref(b.core.get());
}

viflow::Buffer viflow::Buffer::from(GstPadProbeInfo *info, bool ref) {

  auto b = static_cast<GstBuffer *>(info->data);

  if (ref) {
    return Buffer::ref(b);
  }
  return Buffer{b};
}

viflow::Buffer::Buffer(GstBuffer *buffer) : core{buffer} {}

viflow::Buffer::Buffer(unsigned long size)
    : core{gst_buffer_new_and_alloc(size)} {
  if (!core) {
    throw Exception{"Failed to allocate a low level buffer."};
  }
}

GstBuffer *viflow::Buffer::get_core() const noexcept { return core.get(); }

bool viflow::Buffer::is_writable() const noexcept {
  return gst_buffer_is_writable(core.get());
}

void viflow::Buffer::make_writable() {

  auto buf = core.release();
  core.reset(gst_buffer_make_writable(buf));
}

void viflow::Buffer::set_timestamp(unsigned long timestamp) {
  GST_BUFFER_TIMESTAMP(core.get()) = timestamp;
}

void viflow::Buffer::set_duration(unsigned long duration) {
  GST_BUFFER_DURATION(core.get()) = duration;
}

viflow::Map_Info viflow::Buffer::map(GstMapFlags flags) {
  return Map_Info{core.get(), flags};
}

GstBuffer *viflow::Buffer::release() noexcept { return core.release(); }
