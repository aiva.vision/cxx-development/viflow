#include "viflow/app_transform.hxx"
#include "viflow/pad_probe.hxx"
#include "viflow/queue.hxx"

class Event_Probe : public viflow::Pad_Probe {
public:
  Event_Probe(viflow::App_Transform *transform) : app_transform{transform} {}

  GstPadProbeReturn process(GstPad *pad, GstPadProbeInfo *info) {

    auto event = static_cast<GstEvent *>(info->data);
    app_transform->process(event);

    return GST_PAD_PROBE_OK;
  }

  GstPadProbeType get_type() const noexcept {
    return GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM;
  }

private:
  viflow::App_Transform *app_transform;
};

class Buffer_Probe : public viflow::Pad_Probe {
public:
  Buffer_Probe(viflow::App_Transform *transform) : app_transform{transform} {}

  GstPadProbeReturn process(GstPad *pad, GstPadProbeInfo *info) {

    auto buffer = viflow::Buffer::from(info);
    app_transform->process(buffer);

    return GST_PAD_PROBE_OK;
  }

  GstPadProbeType get_type() const noexcept {
    return GST_PAD_PROBE_TYPE_BUFFER;
  }

private:
  viflow::App_Transform *app_transform;
};

viflow::App_Transform::App_Transform(const std::string &name) : Bin{name} {

  auto sink = add("capsfilter", get_name() + "_sink_caps");
  auto queue = append<Queue>(get_name() + "_queue");
  auto src = append("capsfilter", get_name() + "_src_caps");

  set_sink_elem(sink->get_name());
  set_src_elem(src->get_name());

  queue->get_pad("sink").add_probe(std::make_unique<Event_Probe>(this));
  queue->get_pad("src").add_probe(std::make_unique<Buffer_Probe>(this));
}

viflow::Shared_Element<viflow::Queue> viflow::App_Transform::queue() {
  return child<Queue>(get_name() + "_queue");
}

void viflow::App_Transform::set_sink_caps(const Capabilities &caps) {

  child(get_name() + "_sink_caps")->set_property("caps", caps.get_core());
}

void viflow::App_Transform::set_src_caps(const Capabilities &caps) {

  child(get_name() + "_src_caps")->set_property("caps", caps.get_core());
}
