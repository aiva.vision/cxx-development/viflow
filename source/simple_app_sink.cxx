#include <gst/app/gstappsink.h>

#include "viflow/simple_app_sink.hxx"

static GstFlowReturn new_sample_handler(GstElement *sink, void *user_data) {

  auto app_sink = static_cast<viflow::Simple_App_Sink *>(user_data);

  auto sample = viflow::Sample{gst_app_sink_pull_sample(
      reinterpret_cast<GstAppSink *>(app_sink->get_core()))};

  return app_sink->process(sample);
}

viflow::Simple_App_Sink::Simple_App_Sink(const std::string &name)
    : Element{"appsink", name} {

  set_property("enable-last-sample", false);
  set_property("sync", false);
  set_property("qos", false);
  set_property("emit-signals", true);

  g_signal_connect(core.get(), "new-sample", G_CALLBACK(new_sample_handler),
                   this);
}
