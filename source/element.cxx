#include <gst/gst.h>
#include <memory>
#include <mutex>

#include "viflow/element.hxx"
#include "viflow/element_callback.hxx"
#include "viflow/exceptions.hxx"

std::unordered_map<std::string, viflow::Shared_Element<>>
    viflow::Element::instances_register{};

std::mutex viflow::Element::register_mutex{};

bool viflow::Element::is_registered(const std::string &name) {
  return instances_register.contains(name);
}

bool viflow::Element::is_registered(GstElement *elem) {
  return is_registered(GST_ELEMENT_NAME(elem));
}

viflow::Shared_Element<>
viflow::Element::register_instance(const std::string &factory,
                                   const std::string &name) {

  std::unique_lock<std::mutex> register_lock{register_mutex};

  if (!instances_register.contains(name)) {

    auto new_elem = instances_register.insert_or_assign(
        name, Shared_Element<>{new Element(factory, name)});

    return new_elem.first->second;

  } else {
    throw Element_Already_Registered(name);
  }
}

void viflow::Element::unregister_instance(const std::string &name) {

  std::unique_lock<std::mutex> register_lock{register_mutex};
  instances_register.erase(name);
}

viflow::Element::Element(GstElement *elem)
    : core{static_cast<GstElement *>(gst_object_ref_sink(elem))} {

  if (!core) {
    throw Exception{"Tried to initialize an element from a null value"};
  }
}

viflow::Element::Element(const std::string &factory, const std::string &name)
    : Element() {

  unique_element elem{gst_element_factory_make(factory.c_str(), name.c_str())};

  if (!elem) {
    throw Exception{std::string{"Failed to initialize the '"} + name +
                    "' element from the '" + factory + "' factory"};
  }
  core.reset(static_cast<GstElement *>(gst_object_ref_sink(elem.release())));
}

viflow::Element::~Element() {

  if (core) {
    for (auto &[name, pad] : pads) {
      if (pad.is_dynamic()) {
        gst_element_release_request_pad(core.get(), pad.get_core());
      }
    }
  }
}

viflow::State_Change viflow::Element::get_state() const noexcept {
  State_Change state;
  state.ret = gst_element_get_state(core.get(), &state.current, &state.pending,
                                    GST_CLOCK_TIME_NONE);
  return state;
}

GstStateChangeReturn viflow::Element::set_state(GstState state) noexcept {
  return gst_element_set_state(core.get(), state);
}

viflow::Pad &viflow::Element::get_pad(const std::string &name) {
  /*
     The name has the form of 'src_%u', so the pad has to be generated.
     */
  if (name.find('%') != std::string::npos) {
    auto request_pad =
        Pad(gst_element_get_request_pad(core.get(), name.c_str()), true);
    std::string pad_name = request_pad.get_name();
    auto new_pad = pads.insert_or_assign(pad_name, std::move(request_pad));
    return new_pad.first->second;
  }
  /*
     Otherwise, the pad may be already registered.
     */
  if (!pads.contains(name)) {
    /*
       First try to retrieve a static pad.
       */
    unique_pad new_pad{gst_element_get_static_pad(core.get(), name.c_str())};
    bool is_dynamic = false;
    if (!new_pad) {
      /*
         If the pad does not exist, request a dynamic one.
         */
      new_pad.reset(gst_element_get_request_pad(core.get(), name.c_str()));
      if (!new_pad) {
        throw Pad_Request_Error{name};
      }
      is_dynamic = true;
    }
    auto inserted_pad =
        pads.insert_or_assign(name, Pad(new_pad.release(), is_dynamic));
    return inserted_pad.first->second;
  }
  return pads.at(name);
}

viflow::Pad &viflow::Element::register_pad(Pad &&pad) {
  auto pad_name = pad.get_name();
  if (!pads.contains(pad.get_name())) {
    pads.insert_or_assign(pad_name, std::move(pad));
  } else {
    throw Pad_Already_Registered(pad_name, get_name());
  }
  return pads.at(pad_name);
}

viflow::Pad &viflow::Element::register_pad(GstPad *pad, bool dynamic) {
  return register_pad(Pad(pad, dynamic));
}

void viflow::Element::unregister_pad(const std::string &name) {

  auto &pad = pads.at(name);
  if (pad.is_dynamic()) {
    gst_element_release_request_pad(core.get(), pad.get_core());
  }
  pads.erase(name);
}

bool viflow::Element::add_pad(Pad &&pad) {
  auto ret = gst_element_add_pad(core.get(), pad.get_core());
  if (ret) {
    register_pad(std::move(pad));
  }
  return ret;
}

GstElement *viflow::Element::get_core() const noexcept { return core.get(); }

std::string viflow::Element::get_name() const noexcept {
  return GST_ELEMENT_NAME(core.get());
}

bool viflow::Element::has_parent() const {
  return GST_ELEMENT_PARENT(core.get()) != nullptr;
}

std::unique_ptr<viflow::Bus> &viflow::Element::get_bus() noexcept {
  /*
     Return the bus if already registered. Otherwise try to retrieve it.
     */
  if (!bus) {
    bus = std::make_unique<Bus>(gst_element_get_bus(core.get()));
  }
  return bus;
}

long viflow::Element::query_position(GstFormat format) {
  long position{-1};
  if (!gst_element_query_position(core.get(), format, &position)) {
    throw Exception{"Failed to query position for " + get_name()};
  }
  return position;
}

long viflow::Element::query_duration(GstFormat format) {
  long duration{-1};
  if (!gst_element_query_duration(core.get(), format, &duration)) {
    throw Exception{"Failed to query duration for " + get_name()};
  }
  return duration;
}

bool viflow::Element::seek(double rate, GstFormat format, GstSeekFlags flags,
                           GstSeekType start_type, long start,
                           GstSeekType stop_type, long stop) {
  return gst_element_seek(core.get(), rate, format, flags, start_type, start,
                          stop_type, stop);
}

bool viflow::Element::seek_simple(GstFormat format, GstSeekFlags flags,
                                  long position) {
  return gst_element_seek_simple(core.get(), format, flags, position);
}

bool viflow::Element::send_event(GstEvent *event) noexcept {
  return gst_element_send_event(get_core(), event);
}

bool viflow::Element::send_event(Event &&event) noexcept {
  return gst_element_send_event(get_core(), event.release());
}

bool viflow::Element::post_message(GstMessage *message) noexcept {
  return gst_element_post_message(get_core(), message);
}

bool viflow::Element::post_message(Message &&message) noexcept {
  return gst_element_post_message(get_core(), message.release());
}

viflow::Shared_Element<> viflow::Element::link(const Shared_Element<> &elem) {

  if (!gst_element_link(core.get(), elem->get_core())) {
    throw Element_Link_Error(get_name(), elem->get_name());
  }
  return elem;
}

viflow::Shared_Element<>
viflow::Element::link(const Shared_Element<> &elem,
                      const std::string &src_pad_name,
                      const std::string &sink_pad_name) {

  auto &source_pad = get_pad(src_pad_name);
  auto &sink_pad = elem->get_pad(sink_pad_name);

  source_pad.link(sink_pad);
  return elem;
}

void viflow::Element::unlink(GstElement *elem) {
  gst_element_unlink(core.get(), elem);
}

void viflow::Element::unlink(const Shared_Element<> &elem) {
  unlink(elem->get_core());
}

bool viflow::Element::is_connected(const std::string &name) const noexcept {
  return registered_callbacks.contains(name);
}

static void no_more_pads_handler(GstElement *src,
                                 viflow::No_More_Pads_Callback *callback) {

  auto vi_src = viflow::Element::convert(src);
  /*
     Process the new pad.
     */
  callback->process(vi_src);
}

void viflow::Element::connect_callback(
    std::unique_ptr<No_More_Pads_Callback> &&callback) {

  connect_callback(Element_Signals::no_more_pads, no_more_pads_handler,
                   std::move(callback));
}

static void pad_added_handler(GstElement *src, GstPad *new_pad,
                              viflow::Pad_Added_Callback *callback) {

  auto vi_src = viflow::Element::convert(src);
  /*
     Register the new pad with the element if necessary.
     */
  auto &vi_pad = vi_src->get_pad(GST_PAD_NAME(new_pad));
  /*
     Process the new pad.
     */
  callback->process_pad(vi_src, vi_pad);
}

void viflow::Element::connect_callback(
    std::unique_ptr<Pad_Added_Callback> &&callback) {

  connect_callback(Element_Signals::pad_added, pad_added_handler,
                   std::move(callback));
}

static void pad_removed_handler(GstElement *src, GstPad *old_pad,
                                viflow::Pad_Removed_Callback *callback) {

  auto vi_src = viflow::Element::convert(src);
  /*
     Unregister the pad.
     */
  vi_src->unregister_pad(GST_PAD_NAME(old_pad));
  /*
     Process the old pad.
     */
  callback->process_pad(vi_src, old_pad);
}

void viflow::Element::connect_callback(
    std::unique_ptr<Pad_Removed_Callback> &&callback) {

  connect_callback(Element_Signals::pad_removed, pad_removed_handler,
                   std::move(callback));
}

void viflow::Element::disconnect_callback(const std::string &name) {

  auto it = registered_callbacks.find(name);
  if (it != registered_callbacks.end()) {
    g_signal_handler_disconnect(core.get(), it->second.first);
    registered_callbacks.erase(it);
  }
}

bool viflow::Element::sync_state_with_parent() noexcept {
  return gst_element_sync_state_with_parent(core.get());
}

GstStateChangeReturn viflow::Element::play() {
  auto ret = set_state(GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    throw State_Change_Error(get_name(), "PLAYING");
  }
  return ret;
}

GstStateChangeReturn viflow::Element::pause() {
  auto ret = set_state(GST_STATE_PAUSED);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    throw State_Change_Error(get_name(), "PAUSED");
  }
  return ret;
}

GstStateChangeReturn viflow::Element::stop() {
  auto ret = set_state(GST_STATE_NULL);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    throw State_Change_Error(get_name(), "STOPPED");
  }
  return ret;
}

void viflow::Element::unregister() {
  viflow::Element::unregister_instance(get_name());
}
