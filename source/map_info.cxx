#include "viflow/map_info.hxx"
#include <gst/gstbuffer.h>

viflow::Map_Info::Map_Info(GstBuffer *buf, GstMapFlags flags) : buffer{buf} {

  if (!gst_buffer_map(buffer, &map_info, flags)) {
    throw Exception("Failed to map the info data from buffer");
  }
}

viflow::Map_Info::~Map_Info() {
  if (map_info.data) {
    gst_buffer_unmap(buffer, &map_info);
  }
}
