#include "viflow/features.hxx"
#include "viflow/exceptions.hxx"

viflow::Features::Features(const std::string &feature)
    : core{gst_caps_features_new(feature.c_str(), nullptr)} {}

viflow::Features::Features(GstCapsFeatures *features) : core{features} {
  if (!core) {
    throw viflow::Exception{"Tried to initialize Features from a null falue."};
  }
}

bool viflow::Features::contains(const std::string &feature) const noexcept {
  return gst_caps_features_contains(core.get(), feature.c_str());
}

GstCapsFeatures *viflow::Features::get_core() const noexcept {
  return core.get();
}

GstCapsFeatures *viflow::Features::release() noexcept { return core.release(); }
