#include <spdlog/spdlog.h>

#include "viflow/bus.hxx"
#include "viflow/bus_watch.hxx"
#include "viflow/exceptions.hxx"
#include "viflow/message.hxx"

viflow::Bus viflow::Bus::ref(GstBus *b) {
  return Bus{static_cast<GstBus *>(gst_object_ref(b))};
}

viflow::Bus::Bus(GstBus *bus) : core{bus} {
  if (!core) {
    throw Exception{"Tried to initialize Bus from a null value"};
  }
}

viflow::Bus::~Bus() {
  if (has_watch()) {
    remove_watch();
  }
  for (size_t i = 0; i < bus_signal_watches.size(); ++i) {
    gst_bus_remove_signal_watch(core.get());
  }
  bus_signal_watches.clear();
}

bool viflow::Bus::has_watch() { return (bus_watch != nullptr); }

/*
   Dummy function used as adapter for a Bus_Watch.
   */
static gboolean bus_callback(GstBus *bus, GstMessage *message,
                             gpointer user_data) {

  auto vi_bus = viflow::Bus::ref(bus);
  auto vi_message = viflow::Message::ref(message);

  auto watch = static_cast<viflow::Bus_Watch *>(user_data);

  try {
    return watch->process_message(vi_bus, vi_message);

  } catch (const std::exception &e) {

    spdlog::error("Caught an exception in the bus callback: {}", e.what());
    vi_bus.remove_watch();

    return false;

  } catch (...) {

    spdlog::error("Caught an unexpected exception in the bus callback.");
    vi_bus.remove_watch();

    return false;
  }

  return true;
}

unsigned int viflow::Bus::add_watch(std::unique_ptr<Bus_Watch> &&watch) {

  auto id = gst_bus_add_watch(core.get(), bus_callback, watch.get());

  if (!id) {
    throw Exception{"The bus already has a watch"};
  }
  bus_watch_id = id;
  bus_watch = std::move(watch);

  return bus_watch_id;
}

bool viflow::Bus::remove_watch() noexcept {
  bus_watch.reset(nullptr);
  return gst_bus_remove_watch(core.get());
}

/*
   Dummy function used as adapter for a Bus_Watch.
   */
static void bus_signal_callback(GstBus *bus, GstMessage *message,
                                gpointer user_data) {

  auto vi_bus = viflow::Bus::ref(bus);
  auto vi_message = viflow::Message::ref(message);

  auto watch = static_cast<viflow::Bus_Signal_Watch *>(user_data);

  watch->process_message(vi_bus, vi_message);
}

void viflow::Bus::add_signal_watch(std::unique_ptr<Bus_Signal_Watch> &&watch,
                                   const std::string &signal) noexcept {
  if (has_signal_watch(signal)) {
    remove_signal_watch(signal);
  }
  bus_signal_watches.insert_or_assign(signal, std::move(watch));

  gst_bus_add_signal_watch(core.get());
  g_signal_connect(G_OBJECT(core.get()), signal.c_str(),
                   G_CALLBACK(bus_signal_callback),
                   bus_signal_watches.at(signal).get());
}

void viflow::Bus::remove_signal_watch(const std::string &signal) noexcept {

  gst_bus_remove_signal_watch(core.get());
  bus_signal_watches.erase(signal);
}

viflow::Message viflow::Bus::pop() noexcept {
  return Message{gst_bus_pop(core.get())};
}

viflow::Message viflow::Bus::timed_pop_filtered(GstClockTime timeout,
                                                GstMessageType type) noexcept {

  return Message{gst_bus_timed_pop_filtered(core.get(), timeout, type)};
}
