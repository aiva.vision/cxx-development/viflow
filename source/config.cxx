#include <filesystem>

#include "viflow/config.hxx"

std::filesystem::path viflow::get_abs_path(const libconfig::Setting &setting,
                                           const std::string &field) {

  std::filesystem::path p;

  try {
    p = setting[field];
  } catch (const libconfig::SettingNotFoundException &e) {
    throw Misssing_Property(e.getPath());
  } catch (const libconfig::SettingTypeException &e) {
    throw Property_Type_Error(e.getPath());
  }
  if (p.is_absolute()) {
    return p;
  }
  auto config_path = std::filesystem::absolute(setting.getSourceFile());
  return config_path.parent_path() / p;
}
