#include "viflow/exceptions.hxx"

using namespace std::string_literals;

viflow::Exception::Exception(const std::string &m) : message{m} {}

const char *viflow::Exception::what() const noexcept { return message.c_str(); }

viflow::Factory_Error::Factory_Error(const std::string &factory,
                                     const std::string &element)
    : Exception{std::string{"Failed to create element '" + element +
                            "' with factory '" + factory + "'"}} {}

viflow::Factory_Error::Factory_Error(const std::string &factory,
                                     const std::string &element,
                                     const std::string &message)
    : Exception{std::string{"Failed to create element '" + element +
                            "' with factory '" + factory + "': " + message}} {}

viflow::Factory_Not_Found::Factory_Not_Found(const std::string &factory)
    : Exception{std::string{"Factory '"} + factory + "' hasn't been found"} {}

viflow::Parse_Launch_Error::Parse_Launch_Error(const std::string &message)
    : Exception{std::string{
                    "Pipeline::parse_launch failed to create the pipeline: "} +
                message} {}

viflow::Element_Not_Registered::Element_Not_Registered(
    const std::string &element)
    : Exception{std::string{"The requested element hasn't been registered: "} +
                element} {}

viflow::Element_Already_Registered::Element_Already_Registered(
    const std::string &element)
    : Exception{std::string{"The element '"} + element +
                "' has already been registered"} {}

viflow::Element_Not_Added::Element_Not_Added(const std::string &bin,
                                             const std::string &elem)
    : Exception{std::string{"The element '"} + elem +
                "' has not been added yet to bin '" + bin + "'"} {}

viflow::Element_Add_Error::Element_Add_Error(const std::string &bin,
                                             const std::string &elem)
    : Exception{std::string{"Failed to add element '"} + elem + "' to bin '" +
                bin + "'"} {}

viflow::Element_Link_Error::Element_Link_Error(const std::string &elem1,
                                               const std::string &elem2)
    : Exception{std::string{"Failed to link element '"} + elem1 + "' with '" +
                elem2 + "'"} {}

viflow::Bad_Element_Cast::Bad_Element_Cast(const std::string &elem)
    : Exception{std::string{"Attempted to cast '"} + elem +
                "' to the wrong type."} {}

viflow::State_Change_Error::State_Change_Error(const std::string &elem,
                                               const std::string &state)
    : Exception{std::string{"Failed to change state for element '"} + elem +
                "' to " + state} {}

viflow::Pad_Already_Registered::Pad_Already_Registered(
    const std::string &pad, const std::string &element)
    : Exception{std::string{"The pad '"} + pad +
                "' has already been registered in element '" + element + "'"} {}

viflow::Pad_Request_Error::Pad_Request_Error(const std::string &pad)
    : Exception{std::string{"Failed to create the requested pad: "} + pad} {}

std::string viflow::Pad_Link_Error::to_string(GstPadLinkReturn ret) noexcept {

  switch (ret) {
  case GST_PAD_LINK_OK:
    return "GST_PAD_LINK_OK";
  case GST_PAD_LINK_WRONG_HIERARCHY:
    return "GST_PAD_LINK_WRONG_HIERARCHY";
  case GST_PAD_LINK_WAS_LINKED:
    return "GST_PAD_LINK_WAS_LINKED";
  case GST_PAD_LINK_WRONG_DIRECTION:
    return "GST_PAD_LINK_WRONG_DIRECTION";
  case GST_PAD_LINK_NOFORMAT:
    return "GST_PAD_LINK_NOFORMAT";
  case GST_PAD_LINK_NOSCHED:
    return "GST_PAD_LINK_NOSCHED";
  case GST_PAD_LINK_REFUSED:
    return "GST_PAD_LINK_REFUSED";
  default:
    return "Unknown error";
  }
}

viflow::Pad_Link_Error::Pad_Link_Error(const std::string &pad1,
                                       const std::string &pad2,
                                       GstPadLinkReturn ret)
    : Exception{std::string{"Failed to link pad '"} + pad1 + "' and '" + pad2 +
                "', with return " + to_string(ret)},
      link_return{ret} {}

viflow::Pad_Link_Error::Pad_Link_Error(const std::string &pad1,
                                       const std::string &caps1,
                                       const std::string &pad2,
                                       const std::string &caps2,
                                       GstPadLinkReturn ret)
    : Exception{std::string{"Failed to link pad '"} + pad1 + "' and '" + pad2 +
                "', with return " + to_string(ret) +
                "\nFirst pad caps: " + caps1 + "\nSecond pad caps: " + caps2},
      link_return{ret} {}

GstPadLinkReturn viflow::Pad_Link_Error::get_link_return() const noexcept {
  return link_return;
}

viflow::Pad_Unlink_Error::Pad_Unlink_Error(const std::string &pad1,
                                           const std::string &pad2)
    : Exception{std::string{"Failed to unlink pad '"} + pad1 + "' from '" +
                pad2 + "'"} {}

viflow::Bad_Caps::Bad_Caps(const std::string &caps)
    : Exception{std::string{"Failed to allocate caps for '"} + caps + "'"} {}

viflow::Unavailable_Caps::Unavailable_Caps(const std::string &pad)
    : Exception{std::string{"The capabilities for '"} + pad +
                "' are unavailable"} {}

viflow::Wrong_Message_Type::Wrong_Message_Type(GstMessageType expected,
                                               GstMessageType actual)
    : Exception{std::string{"Expected message with type "} +
                GST_MESSAGE_TYPE_NAME(expected) + ", but got " +
                GST_MESSAGE_TYPE_NAME(actual)} {}

viflow::Wrong_Message_Type::Wrong_Message_Type(GstMessageType t)
    : Exception{"Wrong message type: "s + GST_MESSAGE_TYPE_NAME(t)} {}

viflow::IO_Error_Status::IO_Error_Status(GIOStatus status)
    : Exception{std::string{"Received an error status from IO_Channel: "} +
                std::to_string(status)},
      error_status{status} {}

GIOStatus viflow::IO_Error_Status::get_status() const noexcept {
  return error_status;
}

viflow::Misssing_Property::Misssing_Property(const std::string &property)
    : Exception{std::string{"The mandatory property '"} + property +
                "' has not been found in the config"} {}

viflow::Misssing_Property::Misssing_Property(const std::string &property,
                                             const std::string &element)
    : Exception{std::string{"The mandatory property '"} + property +
                "' has not been found in the config for element '" + element +
                "'"} {}

viflow::Property_Type_Error::Property_Type_Error(const std::string &property)
    : Exception{std::string{"The '"} + property +
                "' property has the wrong type"} {}

viflow::Property_Type_Error::Property_Type_Error(const std::string &property,
                                                 const std::string &element)
    : Exception{std::string{"The '"} + property +
                "' property has the wrong type for the '" + element +
                "' element"} {}
