#include <gst/gstcompat.h>
#include <gst/gstghostpad.h>
#include <gst/gstpad.h>
#include <gst/gstutils.h>
#include <gst/gstvalue.h>
#include <memory>

#include "viflow/buffer.hxx"
#include "viflow/element.hxx"
#include "viflow/exceptions.hxx"
#include "viflow/pad.hxx"
#include "viflow/unique_ptr.hxx"

viflow::Pad &viflow::Pad::convert(GstPad *pad) {
  if (!pad) {
    throw viflow::Exception{"Can not convert null pad"};
  }
  std::string name{GST_PAD_NAME(pad)};

  if (auto parent = GST_OBJECT_PARENT(pad)) {
    return Element::convert(GST_ELEMENT(parent))->get_pad(name);
  }
  throw Exception{"Pad has no parent: " + name};
}

viflow::Pad::Pad(GstPad *pad, bool is_dynamic)
    : core{pad}, dynamic{is_dynamic} {
  if (!core) {
    throw Exception("Tried to initialize pad with a null value.");
  }
}

viflow::Pad::~Pad() {
  if (core) {
    if (probe_id) {
      remove_probe();
    }
  }
}

bool viflow::Pad::is_linked() const noexcept {
  return gst_pad_is_linked(core.get());
}

std::string viflow::Pad::get_name() const noexcept {
  return GST_PAD_NAME(core.get());
}

GstPad *viflow::Pad::get_core() const noexcept { return core.get(); }

viflow::Pad &viflow::Pad::get_peer_pad() const {

  unique_pad peer_pad{gst_pad_get_peer(core.get())};

  if (!peer_pad) {
    throw viflow::Exception{"Pad has no peer: " + get_name()};
  }
  return convert(peer_pad.get());
}

viflow::Shared_Element<> viflow::Pad::get_parent() const {

  if (auto parent = GST_OBJECT_PARENT(core.get())) {
    return Element::convert(GST_ELEMENT(parent));
  }
  throw Exception{"Pad has no parent: " + get_name()};
}

viflow::Capabilities viflow::Pad::get_caps() const {
  try {
    return get_current_caps();

  } catch (const Unavailable_Caps &e) {
    return query_caps();
  }
}

bool viflow::Pad::has_current_caps() const noexcept {
  return gst_pad_has_current_caps(core.get());
}

viflow::Capabilities viflow::Pad::get_current_caps() const {
  unique_caps caps{gst_pad_get_current_caps(core.get())};
  if (!caps) {
    throw Unavailable_Caps(get_name());
  }
  return Capabilities(caps.release());
}

viflow::Capabilities viflow::Pad::get_allowed_caps() const {
  unique_caps caps{gst_pad_get_allowed_caps(core.get())};
  if (!caps) {
    throw Unavailable_Caps(get_name());
  }
  return Capabilities(caps.release());
}

viflow::Capabilities viflow::Pad::query_caps() const {
  unique_caps caps{gst_pad_query_caps(core.get(), nullptr)};
  if (!caps) {
    throw Unavailable_Caps(get_name());
  }
  return Capabilities(caps.release());
}

bool viflow::Pad::set_caps(const Capabilities &caps) noexcept {
  return gst_pad_set_caps(core.get(), caps.get_core());
}

void viflow::Pad::use_fixed_caps() { gst_pad_use_fixed_caps(core.get()); }

viflow::Pad
viflow::Pad::create_ghost_pad(const std::string &name) const noexcept {
  /*
     This returns a floating ref, so we have to increase the ref count.
     */
  return Pad(static_cast<GstPad *>(
      gst_object_ref_sink(gst_ghost_pad_new(name.c_str(), core.get()))));
}

void viflow::Pad::link(GstPad *pad) {

  auto ret = gst_pad_link(core.get(), pad);

  if (ret != GST_PAD_LINK_OK) {
    auto name = GST_PAD_NAME(pad);

    Capabilities caps1{gst_pad_query_caps(core.get(), nullptr)};
    Capabilities caps2{gst_pad_query_caps(pad, nullptr)};

    throw Pad_Link_Error(get_name(), caps1.to_string(), name, caps2.to_string(),
                         ret);
  }
}

void viflow::Pad::link(Pad &pad) { return link(pad.get_core()); }

void viflow::Pad::unlink() {

  auto &peer_pad = get_peer_pad();

  bool ret = false;
  if (GST_PAD_IS_SRC(core.get())) {
    ret = gst_pad_unlink(core.get(), peer_pad.get_core());
  } else {
    ret = gst_pad_unlink(peer_pad.get_core(), core.get());
  }
  if (!ret) {
    throw Pad_Unlink_Error(get_name(), peer_pad.get_name());
  }
}

static GstPadProbeReturn callback_adapter(GstPad *pad, GstPadProbeInfo *info,
                                          void *user_data) {
  /*
     Cast the probe, but don't convert the pad. The process of converting the
     pad to a Pad handle is expensive because it involves retrieveing the pad's
     parent and querying the Element registry. Often, the pad received is not
     touched, so the conversion is left up to the user.
     */
  auto probe = static_cast<viflow::Pad_Probe *>(user_data);
  /*
     Process the info.
     */
  auto ret = probe->process(pad, info);
  /*
     Automatically remove the probe registered through viflow.
     */
  if (ret == GST_PAD_PROBE_REMOVE) {
    viflow::Pad::convert(pad).remove_probe();
  }
  return ret;
}

bool viflow::Pad::has_probe() const noexcept { return probe_id != 0; }

unsigned long viflow::Pad::add_probe(std::unique_ptr<Pad_Probe> &&probe) {
  if (probe_id) {
    throw Exception{std::string{"Tried to add a second probe to pad '"} +
                    get_name() + "'"};
  }
  pad_probe = std::move(probe);
  probe_id = gst_pad_add_probe(core.get(), pad_probe->get_type(),
                               callback_adapter, pad_probe.get(), nullptr);
  return probe_id;
}

void viflow::Pad::remove_probe() {
  if (!probe_id) {
    throw Exception{
        std::string{"Tried to remove not existent probe from pad '"} +
        get_name() + "'"};
  }
  gst_pad_remove_probe(core.get(), probe_id);
  pad_probe.reset(nullptr);
  probe_id = 0;
}

bool viflow::Pad::send_event(GstEvent *event) noexcept {
  return gst_pad_send_event(get_core(), event);
}

bool viflow::Pad::send_event(Event &&event) noexcept {
  return gst_pad_send_event(get_core(), event.release());
}

bool viflow::Pad::push_event(GstEvent *event) noexcept {
  return gst_pad_push_event(get_core(), event);
}

bool viflow::Pad::push_event(Event &&event) noexcept {
  return gst_pad_push_event(get_core(), event.release());
}

GstFlowReturn viflow::Pad::push_buffer(Buffer &&buf) noexcept {
  return gst_pad_push(core.get(), buf.release());
}
