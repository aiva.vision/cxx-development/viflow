#include "viflow/tag_list.hxx"

viflow::Tag_List viflow::Tag_List::ref(GstTagList *t) {
  return Tag_List(gst_tag_list_ref(t));
}

viflow::Tag_List viflow::Tag_List::ref(const Tag_List &t) {
  return ref(t.get_core());
}

viflow::Tag_List::Tag_List(GstTagList *taglist) : core(taglist) {
  if (!core) {
    throw Exception("Can not create Tag_List from nullptr");
  }
}

bool viflow::Tag_List::is_empty() { return gst_tag_list_is_empty(core.get()); }
