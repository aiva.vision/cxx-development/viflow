#include "viflow/uri_decode_bin.hxx"

viflow::URI_Decode_Bin::URI_Decode_Bin(const std::string &name)
    : Decode_Bin{gst_element_factory_make("uridecodebin", name.c_str())} {}

static void source_setup_handler(GstElement *bin, GstElement *source,
                                 viflow::Source_Setup_Callback *callback) {

  auto uri_decode_bin = viflow::Element::convert(bin);
  auto source_bin = viflow::Element::convert(source);
  /*
     Process the new pad.
     */
  callback->process(uri_decode_bin, source_bin);
}

void viflow::URI_Decode_Bin::connect_callback(
    std::unique_ptr<Source_Setup_Callback> &&callback) {

  connect_callback(URI_Decode_Bin_Signals::source_setup, source_setup_handler,
                   std::move(callback));
}
