#include "viflow/decode_bin.hxx"

viflow::Decode_Bin::Decode_Bin(const std::string &name)
    : Child_Proxy{gst_element_factory_make("decodebin", name.c_str())} {}

static void drained_handler(GstElement *elem,
                            viflow::Drained_Callback *callback) {

  auto element = viflow::Element::convert(elem);
  /*
     Process the signal.
     */
  callback->process(element);
}

void viflow::Decode_Bin::connect_callback(
    std::unique_ptr<Drained_Callback> &&callback) {

  connect_callback(Decode_Bin_Signals::drained, drained_handler,
                   std::move(callback));
}

static void unknown_type_handler(GstElement *bin, GstPad *pad, GstCaps *caps,
                                 viflow::Unknown_Type_Callback *callback) {

  auto decode_bin = viflow::Element::convert(bin);
  /*
     Process the new pad.
     */
  callback->process(decode_bin, pad, caps);
}

void viflow::Decode_Bin::connect_callback(
    std::unique_ptr<Unknown_Type_Callback> &&callback) {

  connect_callback(Decode_Bin_Signals::unknown_type, unknown_type_handler,
                   std::move(callback));
}
