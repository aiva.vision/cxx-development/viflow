#include "viflow/message.hxx"
#include "viflow/element.hxx"
#include "viflow/error.hxx"

viflow::Message viflow::Message::ref(GstMessage *m) {
  return Message{gst_message_ref(m)};
}

viflow::Message viflow::Message::ref(const Message &m) {
  return ref(m.get_core());
}

viflow::Message viflow::Message::from_struct(const GstStructure *s) {

  GstMessage *m = nullptr;
  gst_structure_get(s, "message", GST_TYPE_MESSAGE, &m, nullptr);

  return Message{m};
}

viflow::Message::Message(GstMessage *message) : core{message} {}

bool viflow::Message::empty() { return core.get() == nullptr; }

std::string viflow::Message::get_type_name() const noexcept {
  return gst_message_type_get_name(core->type);
}

void viflow::Message::assert_type(GstMessageType expected) const {

  if (core->type != expected) {
    throw Wrong_Message_Type(expected, core->type);
  }
}

std::string viflow::Message::get_source_name() const {
  return GST_MESSAGE_SRC_NAME(core.get());
}

viflow::Shared_Element<> viflow::Message::get_source() const {

  if (auto src = GST_ELEMENT(GST_MESSAGE_SRC(core.get()))) {
    return Element::convert(src);
  }
  throw Exception{"Message has no source: " + get_type_name()};
}

viflow::Message::Context viflow::Message::parse_info() const {

  assert_type(GST_MESSAGE_INFO);

  Context context;
  gst_message_parse_info(core.get(), context.error.get_core_address(),
                         context.debug.get_address_of());

  return context;
}

viflow::Message::Context viflow::Message::parse_warning() const {

  assert_type(GST_MESSAGE_WARNING);

  Context context;
  gst_message_parse_warning(core.get(), context.error.get_core_address(),
                            context.debug.get_address_of());

  return context;
}

viflow::Message::Context viflow::Message::parse_error() const {

  assert_type(GST_MESSAGE_ERROR);

  Context context;
  gst_message_parse_error(core.get(), context.error.get_core_address(),
                          context.debug.get_address_of());

  return context;
}

viflow::Message::State_Change viflow::Message::parse_state_changed() const {

  assert_type(GST_MESSAGE_STATE_CHANGED);

  State_Change context;
  gst_message_parse_state_changed(core.get(), &context.old_state,
                                  &context.new_state, &context.pending_state);

  return context;
}

int viflow::Message::parse_buffering() const {

  assert_type(GST_MESSAGE_BUFFERING);

  int percent = 0;
  gst_message_parse_buffering(core.get(), &percent);
  return percent;
}

bool viflow::Message::has_name(const std::string &name) const {
  return gst_message_has_name(core.get(), name.c_str());
}

const GstStructure *viflow::Message::get_structure() const {
  return gst_message_get_structure(core.get());
}
