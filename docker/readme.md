# ViFlow base images

Use `build_images.sh` to build docker images.

```zsh
./build_images.sh
Usage: build_images.sh [:hv:g:u:]

    -h      print this
    -v      viflow_version
    -g      gcc_version
    -u      ubuntu_version, codename or release
```
