#!/bin/bash

optstring=":hv:g:u:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      viflow_version\n"
  printf "    -g      gcc_version\n"
  printf "    -u      ubuntu_version, codename or release\n"
}

if [[ $# -eq 0 ]]; then
  usage
  exit 0
fi

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      viflow_version=${OPTARG}
      ;;
    g)
      gcc_version=${OPTARG}
      ;;
    u)
      ubuntu_version=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

for variable_name in "viflow_version" "gcc_version" "ubuntu_version"
do
  if [ -z ${!variable_name} ]; then
    echo "$(basename $0): Setting ${variable_name} is mandatory."
    exit 1
  fi
done

declare -a target_stages
target_stages=("runtime" "development")

for target_stage in ${target_stages[@]}
do
  gcc_label=""
  if [[ "${target_stage}" == "development" ]]; then
    gcc_label=-gcc${gcc_version}
  fi
  tag="${target_stage}-${viflow_version}${gcc_label}-${ubuntu_version}"

  echo "Building image with tag ${tag} ..."

  docker buildx build \
    --push \
    --platform linux/amd64,linux/arm64 \
    --cpuset-cpus $(nproc) \
    --build-arg viflow_version=${viflow_version} \
    --build-arg gcc_version=${gcc_version} \
    --build-arg ubuntu_version=${ubuntu_version} \
    --target ${target_stage} \
    -t registry.gitlab.com/aiva.vision/cxx-development/viflow:${tag} \
    -f docker/Dockerfile .

done
