# ViFlow

> spelled vee eye flow

**ViFlow** is a C++ set of adapters for [GStreamer](https://gstreamer.freedesktop.org/). At least for the foreseeable future, it does not aim to be complete.

The main goals are:

- reducing boilerplate
- abstracting cleanup away
- simplifying pipelines creation

This repository reimplements the GStreamer basic tutorials as [samples](samples). By comparing these samples to the sources found in the tutorials it becomes clear how much easier it is for newcomers to get started with pipeline creation.

Visit the Docker [guide](docker) for building Docker images.

The documentation can be generated [here](doc).

### Requirements

- [GStreamer](https://gstreamer.freedesktop.org/documentation/installing/on-linux.html?gi-language=c)
- [Doctest](https://github.com/onqtam/doctest)
- [Trompeloeil](https://github.com/rollbear/trompeloeil)

To install some of the prerequisites, execute the following command:

```zsh
sudo apt-get install \
    libgstreamer-plugins-base1.0-dev \
    libgstreamer-plugins-good1.0-dev \
    libgstrtspserver-1.0-dev \
    libgstreamer1.0-dev
```

### Install

1. Clone the repository.

   ```zsh
   git clone git@gitlab.com:aiva.vision/cxx-development/viflow.git
   cd viflow
   ```

2. Build the project and install:

   ```zsh
   cmake -S . -B debug \
     -D CMAKE_BUILD_TYPE=Debug \
     -D build_samples=ON \
     -D build_tests=ON \
     -D CMAKE_EXPORT_COMPILE_COMMANDS=ON

   cmake --build debug -j

   cmake --install debug
   ```
