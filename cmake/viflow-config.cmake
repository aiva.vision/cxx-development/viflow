include(CMakeFindDependencyMacro)

find_dependency(spdlog REQUIRED)
find_dependency(libconfig REQUIRED)

include(${CMAKE_CURRENT_LIST_DIR}/viflow_export.cmake)
